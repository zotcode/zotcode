﻿using System;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web.Http.Results;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Should;
using ZotCode.Api.Controllers;
using ZotCode.Api.Models;
using ZotCode.Api.Providers;
using ZotCode.Domain.Model;
using ZotCode.Domain.Model.Identity;

namespace ZotCode.Api.Tests.Controllers
{
    [TestClass]
    public class AccountControllerTest
    {
        [TestMethod]
        public async Task UserInfo()
        {
            UserInfoViewModel.CreateMaps();
            var user = new User()
            {
                Id = 1,
                UserName = "user1",
                DateOfBirth = DateTime.Today,
                FirstName = "Aaaa",
                LastName = "Bbbb",
                PostalAddress = new Address
                {
                    City = "Sydney",
                    Country = new Country
                    {
                        Name = "Australia"
                    },
                    PostCode = 2000,
                    State = "NSW",
                    StreetName = "George",
                    StreetNumber = 1,
                    UnitNumber = null,
                }
            };

            var store = new Mock<IUserStore<User, int>>(MockBehavior.Strict);
            store.As<IUserPasswordStore<User, int>>()
                .Setup(m => m.FindByIdAsync(1))
                .ReturnsAsync(user);

            var userManager = new ApplicationUserManager(store.Object);
            var claim = new Claim("test", "1");
            var mockIdentity = Mock.Of<ClaimsIdentity>(m => m.FindFirst(It.IsAny<string>()) == claim);
            var controller = new AccountController(userManager, null)
            {
                User = Mock.Of<IPrincipal>(m => m.Identity == mockIdentity)
            };

            var expected = new UserInfoViewModel()
            {
                DateOfBirth = DateTime.Today,
                FirstName = "Aaaa",
                LastName = "Bbbb",
                PostalAddress = new AddressViewModel
                {
                    City = "Sydney",
                    Country = "Australia",
                    PostCode = 2000,
                    State = "NSW",
                    StreetName = "George",
                    StreetNumber = 1,
                    UnitNumber = null
                },
                UserName = "user1"
            };

            var result = await controller.GetUserInfo();
            result.ShouldNotBeNull();
            result.ShouldBeType<OkNegotiatedContentResult<UserInfoViewModel>>();
            var actual = (result as OkNegotiatedContentResult<UserInfoViewModel>).Content;
            actual.UserName.ShouldEqual(expected.UserName);
            actual.FirstName.ShouldEqual(expected.FirstName);
            actual.LastName.ShouldEqual(expected.LastName);
            actual.DateOfBirth.ShouldEqual(expected.DateOfBirth);

            var expectedPostal = expected.PostalAddress;
            var actualPostal = actual.PostalAddress;
            expectedPostal.State.ShouldEqual(actualPostal.State);
            expectedPostal.City.ShouldEqual(actualPostal.City);
            expectedPostal.Country.ShouldEqual(actualPostal.Country);
            expectedPostal.PostCode.ShouldEqual(actualPostal.PostCode);
            expectedPostal.StreetName.ShouldEqual(actualPostal.StreetName);
            expectedPostal.StreetNumber.ShouldEqual(actualPostal.StreetNumber);
            expectedPostal.UnitNumber.ShouldEqual(actualPostal.UnitNumber);
        }

        [TestMethod]
        public async Task UserInfoNoAddress()
        {
            UserInfoViewModel.CreateMaps();
            var user = new User()
            {
                Id = 1,
                UserName = "user1",
                DateOfBirth = DateTime.Today,
                FirstName = "Aaaa",
                LastName = "Bbbb"
            };

            var store = new Mock<IUserStore<User, int>>(MockBehavior.Strict);
            store.As<IUserPasswordStore<User, int>>()
                .Setup(m => m.FindByIdAsync(1))
                .ReturnsAsync(user);

            var userManager = new ApplicationUserManager(store.Object);
            var claim = new Claim("test", "1");
            var mockIdentity = Mock.Of<ClaimsIdentity>(m => m.FindFirst(It.IsAny<string>()) == claim);
            var controller = new AccountController(userManager, null)
            {
                User = Mock.Of<IPrincipal>(m => m.Identity == mockIdentity)
            };

            var expected = new UserInfoViewModel()
            {
                DateOfBirth = DateTime.Today,
                FirstName = "Aaaa",
                LastName = "Bbbb",
                UserName = "user1",
                PostalAddress = null,
            };

            var result = await controller.GetUserInfo();
            result.ShouldNotBeNull();
            result.ShouldBeType<OkNegotiatedContentResult<UserInfoViewModel>>();
            var actual = (result as OkNegotiatedContentResult<UserInfoViewModel>).Content;
            actual.UserName.ShouldEqual(expected.UserName);
            actual.FirstName.ShouldEqual(expected.FirstName);
            actual.LastName.ShouldEqual(expected.LastName);
            actual.DateOfBirth.ShouldEqual(expected.DateOfBirth);
            actual.PostalAddress.ShouldBeNull();
        }
    }
}
