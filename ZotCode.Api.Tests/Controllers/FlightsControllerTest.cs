﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Results;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Should;
using ZotCode.Api.Controllers;
using ZotCode.Api.Models;
using ZotCode.Domain.DataManager;
using ZotCode.Domain.Model;
using ZotCode.Domain.Model.System;

namespace ZotCode.Api.Tests.Controllers
{
    [TestClass]
    public class FlightsControllerTest
    {
        public Mock<IDbSet<T>> SetupMock<T>(IQueryable<T> queryable) where T: class
        {
            var mockSet = new Mock<IDbSet<T>>();

            mockSet.Setup(m => m.Provider).Returns(queryable.Provider);
            mockSet.Setup(m => m.Expression).Returns(queryable.Expression);
            mockSet.Setup(m => m.ElementType).Returns(queryable.ElementType);
            mockSet.Setup(m => m.GetEnumerator()).Returns(queryable.GetEnumerator());

            return mockSet;
        }

        [TestMethod]
        public void GetFlightsForUser()
        {
            FlightViewModel.CreateMaps();

            var dbMock = new Mock<FlightPubContext>();

            var flights = new List<Flight>
            {
                new Flight
                {
                    Id = 1,
                    AirlineId = 1,
                    ArrivalDate = DateTime.Now.AddDays(1),
                    ArrivingAtId = 1,
                    DepartingFromId = 2,
                    DepartureDate = DateTime.Now.AddHours(1),
                    Duration = 1000,
                    PlaneId = 1,
                    Code = "ABC",
                    ArrivingAt = new AirPort
                    {
                        Name = "Paris"
                    },
                    DepartingFrom = new AirPort
                    {
                        Name = "Sydney"
                    }
                }
            };
            var mockFlights = SetupMock(flights.AsQueryable());
            dbMock.Setup(m => m.Flights)
                .Returns(mockFlights.Object);

            var invoices = new List<Invoice>
            {
                new Invoice
                {
                    Id = 1,
                    InvoiceDate = DateTime.Now.AddDays(-10),
                    UserId = 1
                }
            };
            var mockInvoices = SetupMock(invoices.AsQueryable());
            dbMock.Setup(m => m.Invoices)
                .Returns(mockInvoices.Object);

            var tickets = new List<Ticket>
            {
                new Ticket
                {
                    Id = 1,
                    InvoiceId = 1,
                    AllocatedSeatId = 1,
                    AllocatedSeat = new Availability
                    {
                        Id = 1,
                        FlightId = 1
                    }
                }
            };
            var mockTickets = SetupMock(tickets.AsQueryable());
            dbMock.Setup(m => m.Tickets)
                .Returns(mockTickets.Object);
            invoices[0].Tickets = tickets;
            tickets[0].AllocatedSeat.Flight = flights[0];

            var claim = new Claim("test", "1");
            var mockIdentity = Mock.Of<ClaimsIdentity>(m => m.FindFirst(It.IsAny<string>()) == claim);

            var controller = new FlightsController
            {
                DbContext = dbMock.Object,
                User = Mock.Of<IPrincipal>(m => m.Identity == mockIdentity)
            };

            var expected = new FlightViewModel
            {
                Id = 1,
                ArrivalDate = flights[0].ArrivalDate,
                Code = "ABC",
                DepartureDate = flights[0].DepartureDate,
                ArrivalLocation = "Paris",
                DepartureLocation = "Sydney",
                ArriveStopOverDate = null,
                DepartStopOverDate = null,
                StopOverLocation = null
            };

            var result = controller.Get();
            result.ShouldNotBeNull();
            result.ShouldBeType<OkNegotiatedContentResult<IEnumerable<FlightViewModel>>>();

            var actual = (result as OkNegotiatedContentResult<IEnumerable<FlightViewModel>>).Content.First();
            actual.Id.ShouldEqual(expected.Id);
            actual.ArrivalDate.ShouldEqual(expected.ArrivalDate);
            actual.DepartureDate.ShouldEqual(expected.DepartureDate);
            actual.ArrivalLocation.ShouldEqual(expected.ArrivalLocation);
            actual.DepartureLocation.ShouldEqual(expected.DepartureLocation);
            actual.ArriveStopOverDate.ShouldEqual(expected.ArriveStopOverDate);
            actual.DepartStopOverDate.ShouldEqual(expected.DepartStopOverDate);
            actual.StopOverLocation.ShouldEqual(expected.StopOverLocation);
        }
    }
}
