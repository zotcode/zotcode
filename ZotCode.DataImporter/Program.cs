﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CsvHelper;
using ZotCode.DataImporter.Mapping;
using ZotCode.Domain.DataManager;
using ZotCode.Domain.Model;
using ZotCode.Domain.Model.System;
using Country = ZotCode.DataImporter.Mapping.Country;
using Distance = ZotCode.DataImporter.Mapping.Distance;
using Flight = ZotCode.DataImporter.Mapping.Flight;
using PlaneType = ZotCode.DataImporter.Mapping.PlaneType;
using TicketType = ZotCode.DataImporter.Mapping.TicketType;

namespace ZotCode.DataImporter
{
    class Program
    {
        private static FlightPubContext _DbContext = new FlightPubContext();

        static void Main(string[] args)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var availabilities = LoadCsvData<Mapping.Availability>("Data/AvailabilityData.csv");
            Console.WriteLine("{0} availability rows loaded", availabilities.Count);

            var flights = LoadCsvData<Flight>("Data/FlightData.csv");
            Console.WriteLine("{0} flights loaded", flights.Count);

            var prices = LoadCsvData<PriceData>("Data/PriceData.csv");
            Console.WriteLine("{0} prices loaded", prices.Count);

            var airlines = LoadCsvData<Mapping.Airline>("Data/Airlines.csv");
            Console.WriteLine("{0} airlines loaded", airlines.Count);

            var countries = LoadCsvData<Country>("Data/Countries.csv");
            Console.WriteLine("{0} countries loaded", countries.Count);

            var destinations = LoadCsvData<Destination>("Data/Destinations.csv");
            Console.WriteLine("{0} destinations loaded", destinations.Count);

            var distances = LoadCsvData<Distance>("Data/Distances.csv");
            Console.WriteLine("{0} distances loaded", distances.Count);

            var planeTypes = LoadCsvData<PlaneType>("Data/PlaneTypes.csv");
            Console.WriteLine("{0} planes loaded", planeTypes.Count);

            var ticketClasses = LoadCsvData<TicketClass>("Data/TicketClasses.csv");
            Console.WriteLine("{0} ticket classes loaded", ticketClasses.Count);

            var ticketTypes = LoadCsvData<TicketType>("Data/TicketTypes.csv");
            Console.WriteLine("{0} ticket types loaded", ticketTypes.Count);

            //map csv source classes to ef
            AutoMapping.CreateMaps();
            AutoMapping.DbContext = _DbContext;

            ImportCountries(countries);
            ImportAirports(destinations);
            ImportPlaneTypes(planeTypes);
            ImportAirlines(airlines);
            ImportFlights(flights);
            ImportTicketTypes(ticketTypes);
            ImportDistances(distances);
            ImportPrices(prices);
            ImportAvailabilities(availabilities);

            stopwatch.Stop();
            Console.WriteLine("Import completed in {0}", stopwatch.Elapsed);
            Console.WriteLine("Press enter to exit...");
            Console.ReadLine();
        }

        static void ImportAvailabilities(IList<Mapping.Availability> availabilities)
        {
            if (!_DbContext.Availabilities.Any())
            {
                Console.WriteLine("Importing availabilities into database...");
                int count = availabilities.Count;
                int total = 0;
                do
                {
                    var batch = availabilities.Take(20000).ToList();
                    int loaded = batch.Count;
                    availabilities = availabilities.Skip(loaded).ToList();

                    var efAvailabilities = batch.Select(Mapper.Map<Domain.Model.System.Availability>);
                    _DbContext.Availabilities.AddRange(efAvailabilities);
                    _DbContext.SaveChanges();

                    total += loaded;
                    Console.WriteLine("{0} availabilities imported...", total);
                } while (total < count);
                Console.WriteLine("Availabilities have been imported.");
            }
        }

        static void ImportPrices(IEnumerable<PriceData> prices)
        {
            if (!_DbContext.Prices.Any())
            {
                Console.WriteLine("Importing prices into database...");
                var efPrices = prices.Select(Mapper.Map<Price>);
                _DbContext.Prices.AddRange(efPrices);
                _DbContext.SaveChanges();
                Console.WriteLine("Prices have been imported.");
            }
        }

        static void ImportDistances(IEnumerable<Distance> distances)
        {
            if (!_DbContext.Distances.Any())
            {
                var efDistances = distances.Select(Mapper.Map<Domain.Model.System.Distance>);
                _DbContext.Distances.AddRange(efDistances);
                _DbContext.SaveChanges();
            }
        }

        static void ImportTicketTypes(IEnumerable<TicketType> ticketTypes)
        {
            if (!_DbContext.TicketTypes.Any())
            {
                var efTicketTypes = ticketTypes.Select(Mapper.Map<Domain.Model.System.TicketType>);
                _DbContext.TicketTypes.AddRange(efTicketTypes);
                _DbContext.SaveChanges();
            }
        }

        static void ImportCountries(List<Country> countries)
        {
            if (!_DbContext.Countries.Any())
            {
                var efMothers = countries .Where(c => String.IsNullOrEmpty(c.MotherCountryCode3))
                    .Select(Mapper.Map<Domain.Model.Country>);

                //save mother countries first
                //so we can join to child
                _DbContext.Countries.AddRange(efMothers);
                _DbContext.SaveChanges();

                //load child countries
                var efChildren = countries.Where(c => !String.IsNullOrEmpty(c.MotherCountryCode3))
                    .Select(Mapper.Map<Domain.Model.Country>);

                _DbContext.Countries.AddRange(efChildren);
                _DbContext.SaveChanges();
            }
        }

        static void ImportAirports(List<Destination> destinations)
        {
            if (!_DbContext.AirPorts.Any())
            {
                var efAirports = destinations.Select(Mapper.Map<AirPort>).ToList();
                _DbContext.AirPorts.AddRange(efAirports);
                _DbContext.SaveChanges();
            }
        }

        static void ImportPlaneTypes(IEnumerable<PlaneType> planeTypes)
        {
            if (!_DbContext.Planes.Any())
            {
                var efPlanes = planeTypes.Select(Mapper.Map<Plane>);
                _DbContext.Planes.AddRange(efPlanes);
                _DbContext.SaveChanges();
            }
        }

        static void ImportAirlines(IEnumerable<Mapping.Airline> airlines)
        {
            if (!_DbContext.Airlines.Any())
            {
                var efAirlines = airlines.Select(Mapper.Map<Domain.Model.Airline>);
                _DbContext.Airlines.AddRange(efAirlines);
                _DbContext.SaveChanges();
            }
        }

        static void ImportFlights(IEnumerable<Mapping.Flight> flights)
        {
            if (!_DbContext.Flights.Any())
            {
                Console.WriteLine("Importing flights into database...");
                var efFlights = flights.Select(Mapper.Map<Domain.Model.Flight>);
                _DbContext.Flights.AddRange(efFlights);
                _DbContext.SaveChanges();
                Console.WriteLine("Flights have been imported.");
            }
        }

        static List<T> LoadCsvData<T>(string csvPath)
        {
            var list = new List<T>();
            using (var reader = new StreamReader(File.OpenRead(csvPath)))
            {
                using (var csv = new CsvReader(reader))
                {
                    csv.Configuration.IsHeaderCaseSensitive = false;
                    while (csv.Read())
                    {
                        try
                        {
                            var row = csv.GetRecord<T>();
                            list.Add(row);
                        }
                        catch
                        {
                            Console.WriteLine("Error reading row {0}", csv.Row);
                            break;
                        }
                    }
                }
            }

            return list;
        }
    }
}
