﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZotCode.DataImporter.Mapping
{
    public class PriceData
    {
        //AirlineCode,FlightNumber,ClassCode,TicketCode,StartDate,EndDate,Price,PriceLeg1,PriceLeg2

        public string AirlineCode { get; set; }
        public string FlightNumber { get; set; }
        public string ClassCode { get; set; }
        public string TicketCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double Price { get; set; }
        public double? PriceLeg1 { get; set; }
        public double? PriceLeg2 { get; set; }
    }
}
