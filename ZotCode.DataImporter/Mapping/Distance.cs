﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZotCode.DataImporter.Mapping
{
    public class Distance
    {
        //DestinationCode1,DestinationCode2,DistancesInKms

        public string DestinationCode1 { get; set; }
        public string DestinationCode2 { get; set; }
        public int DistancesInKms { get; set; }
    }
}
