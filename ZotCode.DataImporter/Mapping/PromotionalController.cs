﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/*
 * This script generates promotions based on the search input by the user or 
 * the search results returned from the system, depending on preferences/usage
 */

namespace ZotCode.Web.Controllers
{
    public class PromotionalController
    {
        public virtual ActionResult Index()
        {
            return View();
        }
    }
}