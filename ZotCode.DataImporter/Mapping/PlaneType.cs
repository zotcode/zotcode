﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZotCode.DataImporter.Mapping
{
    public class PlaneType
    {
        //PlaneCode,Details,NumFirstClass,NumBusiness,NumPremiumEconomy,Economy
        public string PlaneCode { get; set; }
        public string Details { get; set; }
        public int NumFirstClass { get; set; }
        public int NumBusiness { get; set; }
        public int NumPremiumEconomy { get; set; }
        public int Economy { get; set; }
    }
}
