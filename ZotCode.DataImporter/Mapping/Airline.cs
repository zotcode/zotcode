﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZotCode.DataImporter.Mapping
{
    public class Airline
    {
        public string AirlineCode { get; set; }
        public string AirlineName { get; set; }
        public string CountryCode3 { get; set; }
    }
}
