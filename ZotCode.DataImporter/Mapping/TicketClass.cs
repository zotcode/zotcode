﻿namespace ZotCode.DataImporter.Mapping
{
    public class TicketClass
    {
        //ClassCode,Details

        public string ClassCode { get; set; }
        public string Details { get; set; }
    }
}
