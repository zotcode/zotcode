﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZotCode.DataImporter.Mapping
{
    public class TicketType
    {
        //TicketCode,Name,Transferrable,Refundable,Exchangeable,FrequentFlyerPoints
        public string TicketCode { get; set; }
        public string Name { get; set; }
        public bool Transferrable { get; set; }
        public bool Refundable { get; set; }
        public bool Exchangeable { get; set; }
        public bool FrequentFlyerPoints { get; set; }
    }
}
