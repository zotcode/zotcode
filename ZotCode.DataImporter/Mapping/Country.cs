﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZotCode.DataImporter.Mapping
{
    public class Country
    {
        //countryCode2, countryCode3, countryName, alternateName1, alternateName2, motherCountryCode3, motherCountryComment

        public string CountryCode2 { get; set; }
        public string CountryCode3 { get; set; }
        public string CountryName { get; set; }
        public string AlternateName1 { get; set; }
        public string AlternateName2 { get; set; }
        public string MotherCountryCode3 { get; set; }
        public string MotherCountryComment { get; set; }
    }
}
