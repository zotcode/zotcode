﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZotCode.DataImporter.Mapping
{
    public class Flight
    {
        //AirlineCode,FlightNumber,DepartureCode,StopOverCode,DestinationCode,DepartureTime,ArrivalTimeStopOver,
        //DepartureTimeStopOver,ArrivalTime,PlaneCode,Duration,DurationSecondLeg

        public string AirlineCode { get; set; }
        public string FlightNumber { get; set; }
        public string DepartureCode { get; set; }
        public string StopOverCode { get; set; }
        public string DestinationCode { get; set; }
        public DateTime DepartureTime { get; set; }
        public DateTime? ArrivalTimeStopOver { get; set; }
        public DateTime? DepartureTimeStopOver { get; set; }
        public DateTime ArrivalTime { get; set; }
        public string PlaneCode { get; set; }
        public int Duration { get; set; }
        public int? DurationSecondLeg { get; set; }
    }
}
