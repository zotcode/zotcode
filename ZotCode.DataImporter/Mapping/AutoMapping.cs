﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ZotCode.Domain.DataManager;
using ZotCode.Domain.Model;
using ZotCode.Domain.Model.System;

namespace ZotCode.DataImporter.Mapping
{
    public static class AutoMapping
    {
        public static FlightPubContext DbContext { get; set; }

        public static void CreateMaps()
        {
            Mapper.CreateMap<Country, Domain.Model.Country>()
                .ForMember(d => d.CountryId, o => o.MapFrom(s => s.CountryCode2))
                .ForMember(d => d.Code, o => o.MapFrom(s => s.CountryCode3))
                .ForMember(d => d.Name, o => o.MapFrom(s => s.CountryName))
                .ForMember(d => d.LongName, o => o.MapFrom(s => s.AlternateName1))
                .ForMember(d => d.AlternateLongName, o => o.MapFrom(s => s.AlternateName2))
                .ForMember(d => d.MotherCountryId, o => o.ResolveUsing<MotherCountryResolver>());

            Mapper.CreateMap<Destination, AirPort>()
                .ForMember(d => d.Code, o => o.MapFrom(s => s.DestinationCode))
                .ForMember(d => d.Name, o => o.MapFrom(s => s.Airport))
                .ForMember(d => d.CountryId, o => o.ResolveUsing<AirportCountryResolver>());

            Mapper.CreateMap<PlaneType, Plane>()
                .ForMember(d => d.PlaneType, o => o.MapFrom(s => s.PlaneCode));

            Mapper.CreateMap<Airline, Domain.Model.Airline>()
                .ForMember(d => d.Name, o => o.MapFrom(s => s.AirlineName))
                .ForMember(d => d.Code, o => o.MapFrom(s => s.AirlineCode))
                .ForMember(d => d.CountryId, o => o.ResolveUsing<AirlineCountryResolver>());

            Mapper.CreateMap<Flight, Domain.Model.Flight>()
                .ForMember(d => d.ArrivalDate, o => o.MapFrom(s => s.ArrivalTime))
                .ForMember(d => d.DepartureDate, o => o.MapFrom(s => s.DepartureTime))
                .ForMember(d => d.Code, o => o.MapFrom(s => s.FlightNumber))
                .ForMember(d => d.PlaneId, o => o.ResolveUsing<FlightPlaneResolver>())
                .ForMember(d => d.AirlineId, o => o.ResolveUsing<FlightAirlineResolver>())
                .ForMember(d => d.ArrivingAtId, o => o.ResolveUsing<FlightArrivingAtResolver>())
                .ForMember(d => d.StopOverId, o => o.ResolveUsing<FlightStopOverResolver>())
                .ForMember(d => d.DepartingFromId, o => o.ResolveUsing<FlightDepartingFromResolver>());

            Mapper.CreateMap<TicketType, Domain.Model.System.TicketType>()
                .ForMember(d => d.Code, o => o.MapFrom(s => s.TicketCode));

            Mapper.CreateMap<Distance, Domain.Model.System.Distance>()
                .ForMember(d => d.AirPortAId, o => o.ResolveUsing<DistanceAirportAResolver>())
                .ForMember(d => d.AirPortBId, o => o.ResolveUsing<DistanceAirportBResolver>())
                .ForMember(d => d.DistanceInKms, o => o.MapFrom(s => s.DistancesInKms));

            Mapper.CreateMap<PriceData, Price>()
                .ForMember(d => d.AirlineId, o => o.ResolveUsing<PriceAirlineResolver>())
                .ForMember(d => d.FlightCode, o => o.MapFrom(s => s.FlightNumber))
                .ForMember(d => d.TicketTypeId, o => o.ResolveUsing<PriceTicketTypeResolver>())
                .ForMember(d => d.SeatClass, o => o.ResolveUsing<PriceSeatClassResolver>());

            Mapper.CreateMap<Availability, Domain.Model.System.Availability>()
                .ForMember(d => d.Class, o => o.ResolveUsing<AvailabilityClassResolver>())
                .ForMember(d => d.TicketTypeId, o => o.ResolveUsing<AvailabilityTicketTypeResolver>())
                .ForMember(d => d.FlightId, o => o.ResolveUsing<AvailabilityFlightResolver>())
                .ForMember(d => d.AvailableLeg1, o => o.MapFrom(s => s.NumberAvailableSeatsLeg1))
                .ForMember(d => d.AvailableLeg2, o => o.MapFrom(s => s.NumberAvailableSeatsLeg2));
        }
    }

    public class AvailabilityFlightResolver : ValueResolver<Availability, int>
    {
        class FlightData
        {
            public int Id { get; set; }
            public string FlightCode { get; set; }
            public DateTime DepartureTime { get; set; }
        }

        public AvailabilityFlightResolver()
        {
            if (_Data == null)
            {
                _Data =
                    AutoMapping.DbContext.Flights.Select(
                        f => new FlightData {Id = f.Id, FlightCode = f.Code, DepartureTime = f.DepartureDate}).ToList();
            }
        }

        private static IEnumerable<FlightData> _Data;

        protected override int ResolveCore(Availability source)
        {
            return
                _Data.Single(f => f.DepartureTime.Equals(source.DepartureTime) && f.FlightCode == source.FlightNumber)
                    .Id;
        }
    }

    public class AvailabilityClassResolver : ValueResolver<Availability, SeatClass>
    {
        protected override SeatClass ResolveCore(Availability source)
        {
            switch (source.ClassCode)
            {
                case "BUS":
                    return SeatClass.Business;
                case "ECO":
                    return SeatClass.Economy;
                case "FIR":
                    return SeatClass.FirstClass;
                case "PME":
                    return SeatClass.PremiumEconomy;
                default:
                    throw new Exception("Invalid Ticket Type");
            }
        }
    } 

    public class PriceSeatClassResolver : ValueResolver<PriceData, SeatClass>
    {
        protected override SeatClass ResolveCore(PriceData source)
        {
            switch (source.ClassCode)
            {
                case "BUS": 
                    return SeatClass.Business;
                case "ECO":
                    return SeatClass.Economy;
                case "FIR":
                    return SeatClass.FirstClass;
                case "PME":
                    return SeatClass.PremiumEconomy;
                default: 
                    throw new Exception("Invalid Ticket Type");
            }
        }
    } 

    public class PriceTicketTypeResolver : ValueResolver<PriceData, int>
    {
        private static Dictionary<string, int> _data; 
        public PriceTicketTypeResolver()
        {
            if (_data == null)
            {
                _data = AutoMapping.DbContext.TicketTypes.ToDictionary(t => t.Code, t => t.Id);
            }
        }

        protected override int ResolveCore(PriceData source)
        {
            return _data[source.TicketCode];
        }
    }

    public class AvailabilityTicketTypeResolver : ValueResolver<Availability, int>
    {
        private static Dictionary<string, int> _data; 
        public AvailabilityTicketTypeResolver()
        {
            if (_data == null)
            {
                _data = AutoMapping.DbContext.TicketTypes.ToDictionary(t => t.Code, t => t.Id);
            }
        }

        protected override int ResolveCore(Availability source)
        {
            return _data[source.TicketCode];
        }
    }

    public class PriceFlightResolver : ValueResolver<PriceData, int>
    {
        protected override int ResolveCore(PriceData source)
        {
            return AutoMapping.DbContext.Flights.First(f => f.Code == source.FlightNumber).Id;
        }
    }

    public class PriceAirlineResolver : ValueResolver<PriceData, int>
    {
        private static Dictionary<string, int> _data;

        public PriceAirlineResolver()
        {
            if (_data == null)
            {
                _data = AutoMapping.DbContext.Airlines.ToDictionary(a => a.Code, a => a.Id);
            }
        }

        protected override int ResolveCore(PriceData source)
        {
            return _data[source.AirlineCode];
        }
    }

    public class DistanceAirportAResolver : ValueResolver<Distance, int>
    {
        protected override int ResolveCore(Distance source)
        {
            return AutoMapping.DbContext.AirPorts.Single(a => a.Code == source.DestinationCode1).Id;
        }
    }

    public class DistanceAirportBResolver : ValueResolver<Distance, int>
    {
        protected override int ResolveCore(Distance source)
        {
            return AutoMapping.DbContext.AirPorts.Single(a => a.Code == source.DestinationCode2).Id;
        }
    }

    public class FlightPlaneResolver : ValueResolver<Flight, int>
    {
        private static Dictionary<string, int> _data; 
        public FlightPlaneResolver()
        {
            if (_data == null)
            {
                _data = AutoMapping.DbContext.Planes.Distinct().ToDictionary(k => k.PlaneType, v => v.Id);
            }
        }

        protected override int ResolveCore(Flight source)
        {
            return _data[source.PlaneCode];
        }
    }

    public class FlightArrivingAtResolver : ValueResolver<Flight, int>
    {
        private static Dictionary<string, int> _data; 
        public FlightArrivingAtResolver()
        {
            if (_data == null)
            {
                _data = AutoMapping.DbContext.AirPorts.Distinct().ToDictionary(k => k.Code, v => v.Id);
            }
        }

        protected override int ResolveCore(Flight source)
        {
            return _data[source.DestinationCode];
        }
    }

    public class FlightStopOverResolver : ValueResolver<Flight, int?>
    {
        private static Dictionary<string, int> _data; 
        public FlightStopOverResolver()
        {
            if (_data == null)
            {
                _data = AutoMapping.DbContext.AirPorts.Distinct().ToDictionary(k => k.Code, v => v.Id);
            }
        }

        protected override int? ResolveCore(Flight source)
        {
            if (String.IsNullOrEmpty(source.StopOverCode))
                return null;

            return _data[source.StopOverCode];
        }
    }

    public class FlightDepartingFromResolver : ValueResolver<Flight, int>
    {
        private static Dictionary<string, int> _data; 
        public FlightDepartingFromResolver()
        {
            if (_data == null)
            {
                _data = AutoMapping.DbContext.AirPorts.Distinct().ToDictionary(k => k.Code, v => v.Id);
            }
        }

        protected override int ResolveCore(Flight source)
        {
            return _data[source.DepartureCode];
        }
    }

    public class FlightAirlineResolver : ValueResolver<Flight, int>
    {
        private static Dictionary<string, int> _data; 
        public FlightAirlineResolver()
        {
            if (_data == null)
            {
                _data = AutoMapping.DbContext.Airlines.Distinct().ToDictionary(k => k.Code, v => v.Id);
            }
        }

        protected override int ResolveCore(Flight source)
        {
            return _data[source.AirlineCode];
        }
    }

    public class AirlineCountryResolver : ValueResolver<Airline, int>
    {
        protected override int ResolveCore(Airline source)
        {
            return AutoMapping.DbContext.Countries.Single(c => c.Code.Equals(source.CountryCode3)).Id;
        }
    }

    public class MotherCountryResolver : ValueResolver<Country, int?>
    {
        protected override int? ResolveCore(Country source)
        {
            if (String.IsNullOrEmpty(source.MotherCountryCode3))
                return null;

            var country = AutoMapping.DbContext.Countries.SingleOrDefault(c => c.Code.Equals(source.MotherCountryCode3));
            if (country != null)
            {
                return country.Id;
            }
            return null;
        }
    }

    public class AirportCountryResolver : ValueResolver<Destination, int>
    {
        protected override int ResolveCore(Destination source)
        {
            var country = AutoMapping.DbContext.Countries.Single(c => c.Code == source.CountryCode3);
            return country.Id;
        }
    }
}
