﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZotCode.DataImporter.Mapping
{
    public class Destination
    {
        //DestinationCode, Airport, CountryCode3

        public string DestinationCode { get; set; }
        public string Airport { get; set; }
        public string CountryCode3 { get; set; }
    }
}
