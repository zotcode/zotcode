﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZotCode.DataImporter.Mapping
{
    public class Availability
    {
        public string AirlineCode { get; set; }
        public string FlightNumber { get; set; }
        public DateTime DepartureTime { get; set; }
        public string ClassCode { get; set; }
        public string TicketCode { get; set; }
        public int NumberAvailableSeatsLeg1 { get; set; }
        public int? NumberAvailableSeatsLeg2 { get; set; }
    }
}
