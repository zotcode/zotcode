﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using Microsoft.AspNet.Identity;
using ZotCode.Domain.DataManager;
using Microsoft.AspNet.Identity.Owin;
using ZotCode.Api.Models;
using ZotCode.Domain.Model;

namespace ZotCode.Api.Controllers
{
    [RoutePrefix("api/Flights")]
    [Authorize]
    public class FlightsController : ApiController
    {
        private FlightPubContext _dbContext;
        public FlightPubContext DbContext
        {
            get
            {
                return _dbContext ?? Request.GetOwinContext().Get<FlightPubContext>();
            }
            set { _dbContext = value; }
        }

        public FlightsController() { }

        public FlightsController(FlightPubContext dbContext)
        {
            DbContext = dbContext;
        }

        /// <summary>
        /// GET All flights for user
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult Get()
        {
            var userId = User.Identity.GetUserId<int>();

            var flights = Flight.FlightExtensions.FlightsForUser(DbContext,userId)
                .OrderByDescending(f => f.DepartureDate)
                .ToList();

            var model = Mapper.Map<IEnumerable<FlightViewModel>>(flights);

            return Ok(model);
        }
    }
}