﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using ZotCode.Domain.Model;

namespace ZotCode.Api.Models
{
    // Models returned by AccountController actions.

    public class ExternalLoginViewModel
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string State { get; set; }
    }

    public class ManageInfoViewModel
    {
        public string LocalLoginProvider { get; set; }

        public string Email { get; set; }

        public IEnumerable<UserLoginInfoViewModel> Logins { get; set; }

        public IEnumerable<ExternalLoginViewModel> ExternalLoginProviders { get; set; }
    }

    public class UserInfoViewModel
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }
        public AddressViewModel PostalAddress { get; set; }

        public static void CreateMaps()
        {
            Mapper.CreateMap<User, UserInfoViewModel>();
            AddressViewModel.CreateMaps();
        }
    }

    public class AddressViewModel
    {
        public int? UnitNumber { get; set; }
        public int? StreetNumber { get; set; }
        public string StreetName { get; set; }
        public string City { get; set; }
        public int? PostCode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

        public static void CreateMaps()
        {
            Mapper.CreateMap<Address, AddressViewModel>()
                .ForMember(d => d.PostCode, o => o.Condition(s => s.PostCode > 0))
                .ForMember(d => d.UnitNumber, o => o.Condition(s => s.UnitNumber > 0))
                .ForMember(d => d.StreetNumber, o => o.Condition(s => s.StreetNumber > 0))
                .ForMember(d => d.Country, o => o.Condition(s => s.Country != null))
                .ForMember(d => d.Country, o => o.MapFrom(s => s.Country.Name));
        }
    }

    public class UserLoginInfoViewModel
    {
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }
    }
}
