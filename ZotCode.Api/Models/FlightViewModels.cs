﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using ZotCode.Domain.Model;

namespace ZotCode.Api.Models
{
    public class FlightViewModel
    {
        public int Id { get; set; }
        public string Code { get; set; }

        public DateTime DepartureDate { get; set; }
        public string DepartureLocation { get; set; }
        public DateTime ArrivalDate { get; set; }
        public string ArrivalLocation { get; set; }
        public DateTime? ArriveStopOverDate { get; set; }
        public string StopOverLocation { get; set; }
        public DateTime? DepartStopOverDate { get; set; }

        public static void CreateMaps()
        {
            Mapper.CreateMap<Flight, FlightViewModel>()
                .ForMember(o => o.ArrivalLocation, o => o.MapFrom(s => s.ArrivingAt.Name))
                .ForMember(o => o.DepartureLocation, o => o.MapFrom(s => s.DepartingFrom.Name))
                .ForMember(o => o.ArriveStopOverDate, o => o.MapFrom(s => s.ArrivalTimeStopOver))
                .ForMember(o => o.DepartStopOverDate, o => o.MapFrom(s => s.DepartureTimeStopOver));
        }
    }
}