﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using ZotCode.Api.Models;

namespace ZotCode.Api
{
    public partial class Startup
    {
        public void ConfigureAutoMapper()
        {
            FlightViewModel.CreateMaps();
            UserInfoViewModel.CreateMaps();
        }
    }
}