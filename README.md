ZotCode - Flight Booking System
===============================

# Developers #

* Joshua Crompton - C3165877
* Leigh Cooper - C3088848
* Nathan Davidson - C3187164
* Simon Hartcher - C3185790
* Tyler Haigh - C3182929
* Christian Lassen - C3121707

# Abstract #

This project aims to implement a fully functioning flight booking website as part of SENG3150 - Software Project at the University of Newcastle (Callaghan), 2015.

FlightPub is an Australian start-up company that specialises in direct ticket sales for airlines. FlightPub does not operate with retail outlets and their vision is to conduct their business via an e-commerce store; they have approached ZotCode to design and implement the solution. Preliminary research has indicated that existing e-commerce flight booking sites contain a significant portion of the required functionality for FlightPub’s website. In addition to this, FlightPub have requested that additional functionality be included in their system to provide a superior product, service, and improve their market share. ZotCode’s goal and project purpose will be to establish FlightPub’s online presence.

# To Run #

1. Clone our project `git clone git@bitbucket.org:zotcode/zotcode.git`
2. Open the ZotCode.sln Visual Studio Solution file
3. `Build` the project to restore NuGet Packages
4. Initialise the Database by running `Update-Database -StartUpProjectName ZotCode.Web` (targeting ZotCode.Domain) from the NuGet Package Manager Console. Or run the ZotCode.Web Application (Note: You may need to navigate to the Log In page for this to take effect)
5. Once the database has been initialised in `ZotCode.Web/App_Data/aspnet-ZotCode.mdf`:
    1. Open the NuGet Package Manager Console in Visual Studio `Tools->NuGet Package Manager->Package Manager Console`
    2. Run `Update-Database` to update to the latest database schema (make sure to target ZotCode.Domain)
    3. Run the ZotCode.DataImporter console application to populate the database

# Client Side Libraries #

We have used [Bower](http://bower.io/) and [Gulp](http://gulpjs.com/) for this project. Refer to our [Wiki guide](https://bitbucket.org/zotcode/zotcode/wiki/Tutorials/Getting%20VS2013%20Up%20and%20Running) for details on how to install these tools and download the client side dependencies