﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using ZotCode.Domain.Model;
using ZotCode.Domain.Model.Identity;
using ZotCode.Domain.Model.System;

namespace ZotCode.Domain.DataManager
{
    // This will represent all the operations that will be done against a real database.
    public interface IFlightPubContext : IDisposable
    {
        IQueryable<T> Query<T>() where T : class;
    }
}
