﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZotCode.Domain.Model;
using ZotCode.Domain.Model.Identity;
using ZotCode.Domain.Model.System;

namespace ZotCode.Domain.DataManager
{
    /// <summary>
    /// Provides the context for the Entity Framework to work with a MS SQL Database.
    /// <param>In this application, the context will be confined to FlightPub's data.</param>
    /// </summary>
    public class FlightPubContext : IdentityDbContext<User, Role, int, UserLogin, UserRole, UserClaim>, IFlightPubContext
    {

        public virtual IDbSet<Address> Addresses { get; set; }
        public virtual IDbSet<AirPort> AirPorts { get; set; }
        public virtual IDbSet<Country> Countries { get; set; }
        public virtual IDbSet<Flight> Flights { get; set; }
        public virtual IDbSet<Invoice> Invoices { get; set; }
        public virtual IDbSet<Passenger> Passengers { get; set; }
        public virtual IDbSet<Plane> Planes { get; set; }
        public virtual IDbSet<Availability> Availabilities { get; set; }
        public virtual IDbSet<Ticket> Tickets { get; set; }
        public virtual IDbSet<Airline> Airlines { get; set; }
        public virtual IDbSet<Model.System.TicketType> TicketTypes { get; set; }
        public virtual IDbSet<Distance> Distances { get; set; }
        public virtual IDbSet<Price> Prices { get; set; }

        public FlightPubContext() : base("FlightPubContext") { }

        public static FlightPubContext Create()
        {
            return new FlightPubContext();
        }

        IQueryable<T> IFlightPubContext.Query<T>()
        {
            return Set<T>();
        }
    }
}
