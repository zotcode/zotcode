﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZotCode.Domain.Model
{
    public class Invoice
    {
        public int Id { get; set; }
        public DateTime InvoiceDate { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public List<Ticket> Tickets { get; set; }

    }
}
