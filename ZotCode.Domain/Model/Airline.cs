﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZotCode.Domain.Model
{
    public class Airline
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public string Code { get; set; }

        public virtual ICollection<Plane> Planes { get; set; }

        public int CountryId { get; set; }
        public virtual Country Country { get; set; }
    }
}
