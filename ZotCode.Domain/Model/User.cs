﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ZotCode.Domain.Model.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace ZotCode.Domain.Model
{
    public class User : IdentityUser<int, UserLogin, UserRole, UserClaim>
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public DateTime DateOfBirth { get; set; }

        public int? PostalAddressId { get; set; }
        public virtual Address PostalAddress { get; set; }

        public int? BillingAddressId { get; set; }
        public virtual Address BillingAddress { get; set; }

        public virtual ICollection<Invoice> Invoices { get; set; }

        [NotMapped]
        public string FullName
        {
            get
            {
                return string.Format("{0} {1}", FirstName, LastName);
            }
        }

    }
}
