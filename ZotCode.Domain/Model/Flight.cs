﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZotCode.Domain.DataManager;
using ZotCode.Domain.Model.System;

namespace ZotCode.Domain.Model
{
    public class Flight
    {
        //AirlineCode,FlightNumber,DepartureCode,StopOverCode,DestinationCode,DepartureTime,
        //ArrivalTimeStopOver,DepartureTimeStopOver,ArrivalTime,PlaneCode,Duration,DurationSecondLeg

        public int Id { get; set; }
        [Index(IsUnique = false), StringLength(6)]
        public string Code { get; set; }

        public int AirlineId { get; set; }
        public virtual Airline Airline { get; set; }

        public int PlaneId { get; set; }
        public virtual Plane Plane { get; set; }

        public DateTime DepartureDate { get; set; }
        public int DepartingFromId { get; set; }
        public virtual AirPort DepartingFrom { get; set; }

        public DateTime? ArrivalTimeStopOver { get; set; }
        public int? StopOverId { get; set; }
        public virtual AirPort StopOver { get; set; }
        public DateTime? DepartureTimeStopOver { get; set; }

        public DateTime ArrivalDate { get; set; }
        public int ArrivingAtId { get; set; }
        public virtual AirPort ArrivingAt { get; set; }

        public int Duration { get; set; }
        public int? DurationSecondLeg { get; set; }

        public virtual ICollection<Availability> FlightAvailabilities { get; set; }
    }

    public static class FlightExtensions
    {
        public static IQueryable<Flight> FlightsForUser(this FlightPubContext db, int userId)
        {
            return db.Invoices
                .Where(i => i.UserId == userId)
                .SelectMany(i => i.Tickets)
                .Select(i => i.AllocatedSeat.Flight)
                .Distinct();
        }
    }
}
