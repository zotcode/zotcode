﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZotCode.Domain.Model
{
    public class AirPort
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

        public int CountryId { get; set; }
        public virtual Country Country { get; set; }
    }
}
