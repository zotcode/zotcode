﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZotCode.Domain.Model.System;

namespace ZotCode.Domain.Model
{
    public class Plane
    {
        public int Id { get; set; }
        public string PlaneType { get; set; }

        public virtual ICollection<Airline> Airlines { get; set; } 
    }
}
