﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZotCode.Domain.Model.System;

namespace ZotCode.Domain.Model
{
    public class Ticket
    {
        public int Id { get; set; }
        public string TicketCode { get; set; }
        public string TicketName { get; set; }
        public double TicketPrice { get; set; }
        
        public int AllocatedSeatId { get; set; }
        public virtual Availability AllocatedSeat { get; set; }

        public bool IsRefundable { get; set; }
        public bool IsTransferable { get; set; }
        public bool IsExchangeable { get; set; }

        public int FrequentFlyerPoints { get; set; }

        public int InvoiceId { get; set; }
        public virtual Invoice Invoice { get; set; }

        public int PassengerId { get; set; }
        public virtual Passenger Passegner { get; set; }
    }
}
