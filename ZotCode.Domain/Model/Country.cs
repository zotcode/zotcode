﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZotCode.Domain.Model
{
    public class Country
    {
        public int Id { get; set; } //Used for fast look-ups on MotherCountry - Tyler Haigh (12 April 2015)
        
        public string CountryId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string LongName { get; set; }
        public string AlternateLongName { get; set; }

        public int? MotherCountryId { get; set; }
        public virtual Country MotherCountry { get; set; }
        public string MotherCountryComments { get; set; }

        public List<AirPort> AirPorts { get; set; }
    }
}
