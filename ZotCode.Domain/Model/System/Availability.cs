﻿namespace ZotCode.Domain.Model.System
{
    public class Availability
    {
        //AirlineCode,FlightNumber,DepartureTime,ClassCode,TicketCode,NumberAvailableSeatsLeg1,NumberAvailableSeatsLeg2

        public int Id { get; set; }

        public int FlightId { get; set; }
        public virtual Flight Flight { get; set; }

        public SeatClass Class { get; set; }

        public int TicketTypeId { get; set; }
        public virtual TicketType TicketType { get; set; }

        public int AvailableLeg1 { get; set; }
        public int? AvailableLeg2 { get; set; }
    }

    public enum SeatClass 
    {
        Business,
        Economy,
        FirstClass,
        PremiumEconomy
    }
}
