﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZotCode.Domain.Model.System
{
    public class TicketType
    {
        //TicketCode,Name,Transferrable,Refundable,Exchangeable,FrequentFlyerPoints
        [Key]
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool Transferrable { get; set; }
        public bool Refundable { get; set; }
        public bool Exchangable { get; set; }
        public bool FrequentFlyerPoints { get; set; }
    }
}
