﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZotCode.Domain.Model.System
{
    public class Distance
    {
        public int Id { get; set; }

        public int AirPortAId { get; set; }
        public virtual AirPort AirPortA { get; set; }

        public int AirPortBId { get; set; }
        public virtual AirPort AirPortB { get; set; }

        public int DistanceInKms { get; set; }
    }
}
