﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZotCode.Domain.Model.System
{
    public class Price
    {
        public int Id { get; set; }

        public int AirlineId { get; set; }
        public virtual Airline Airline { get; set; }

        [Index(IsUnique = false), StringLength(6)]
        public string FlightCode { get; set; }

        public SeatClass SeatClass { get; set; }

        public int TicketTypeId { get; set; }
        public virtual TicketType TicketType { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public double Value { get; set; }
        public double? PriceLeg1 { get; set; }
        public double? PriceLeg2 { get; set; }
    }
}
