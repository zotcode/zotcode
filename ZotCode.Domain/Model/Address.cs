﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZotCode.Domain.Model
{
    public class Address
    {

        public int Id { get; set; }

        public int? UnitNumber { get; set; }
        public int StreetNumber { get; set; }
        public string StreetName { get; set; }

        /// <summary>
        /// This can be the Suburb (e.g. Euroka) or City (e.g. Sydney). Pick the one with the highest granularity.
        /// </summary>
        public string City { get; set; }
        public ushort PostCode { get; set; }

        public string State { get; set; }

        public int CountryId { get; set; }
        public virtual Country Country { get; set; }

        public AddressType AddressType { get; set; }

        [NotMapped]
        public string FullAddress
        {
            get
            {
                return UnitNumber != null ?
                    string.Format("{0}/{1} {2}\n{3} {4}\n{5} {6}",
                   UnitNumber, StreetNumber, StreetName, City, PostCode, State, Country.Name)
                    : string.Format("{0} {1}\n{2} {3}\n{4} {5}",
                   StreetNumber, StreetName, City, PostCode, State, Country.Name);
            }
        }

    }

    public enum AddressType
    {
        Postal,
        Billing
    }
}
