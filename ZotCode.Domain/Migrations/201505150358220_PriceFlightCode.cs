namespace ZotCode.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PriceFlightCode : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Prices", "FlightId", "dbo.Flights");
            DropIndex("dbo.Prices", new[] { "FlightId" });
            AddColumn("dbo.Prices", "FlightCode", c => c.String(maxLength: 6));
            CreateIndex("dbo.Prices", "FlightCode");
            DropColumn("dbo.Prices", "FlightId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Prices", "FlightId", c => c.Int(nullable: false));
            DropIndex("dbo.Prices", new[] { "FlightCode" });
            DropColumn("dbo.Prices", "FlightCode");
            CreateIndex("dbo.Prices", "FlightId");
            AddForeignKey("dbo.Prices", "FlightId", "dbo.Flights", "Id", cascadeDelete: false);
        }
    }
}
