namespace ZotCode.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PlaneRemoveAvailabilities : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Availabilities", "Plane_Id", "dbo.Planes");
            Sql("ALTER TABLE Availabilities DROP CONSTRAINT [FK_dbo.Seats_dbo.Planes_PlaneId]");
            DropIndex("dbo.Availabilities", new[] { "Plane_Id" });
            DropColumn("dbo.Availabilities", "Plane_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Availabilities", "Plane_Id", c => c.Int());
            CreateIndex("dbo.Availabilities", "Plane_Id");
            AddForeignKey("dbo.Availabilities", "Plane_Id", "dbo.Planes", "Id");
        }
    }
}
