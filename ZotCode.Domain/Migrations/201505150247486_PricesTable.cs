namespace ZotCode.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PricesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Prices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AirlineId = c.Int(nullable: false),
                        FlightId = c.Int(nullable: false),
                        SeatClass = c.Int(nullable: false),
                        TicketTypeId = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Value = c.Double(nullable: false),
                        PriceLeg1 = c.Double(),
                        PriceLeg2 = c.Double(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Airlines", t => t.AirlineId, cascadeDelete: true)
                .ForeignKey("dbo.Flights", t => t.FlightId, cascadeDelete: false)
                .ForeignKey("dbo.TicketTypes", t => t.TicketTypeId, cascadeDelete: true)
                .Index(t => t.AirlineId)
                .Index(t => t.FlightId)
                .Index(t => t.TicketTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Prices", "TicketTypeId", "dbo.TicketTypes");
            DropForeignKey("dbo.Prices", "FlightId", "dbo.Flights");
            DropForeignKey("dbo.Prices", "AirlineId", "dbo.Airlines");
            DropIndex("dbo.Prices", new[] { "TicketTypeId" });
            DropIndex("dbo.Prices", new[] { "FlightId" });
            DropIndex("dbo.Prices", new[] { "AirlineId" });
            DropTable("dbo.Prices");
        }
    }
}
