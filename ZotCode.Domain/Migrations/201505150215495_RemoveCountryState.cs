namespace ZotCode.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveCountryState : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CountryStates", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.Addresses", "StateId", "dbo.CountryStates");
            DropIndex("dbo.Addresses", new[] { "StateId" });
            DropIndex("dbo.CountryStates", new[] { "CountryId" });
            AddColumn("dbo.Addresses", "State", c => c.String());
            DropColumn("dbo.Addresses", "StateId");
            DropTable("dbo.CountryStates");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.CountryStates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Code = c.String(),
                        CountryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Addresses", "StateId", c => c.Int(nullable: false));
            DropColumn("dbo.Addresses", "State");
            CreateIndex("dbo.CountryStates", "CountryId");
            CreateIndex("dbo.Addresses", "StateId");
            AddForeignKey("dbo.Addresses", "StateId", "dbo.CountryStates", "Id", cascadeDelete: true);
            AddForeignKey("dbo.CountryStates", "CountryId", "dbo.Countries", "Id", cascadeDelete: true);
        }
    }
}
