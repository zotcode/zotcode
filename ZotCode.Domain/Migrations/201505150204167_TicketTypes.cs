namespace ZotCode.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TicketTypes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TicketTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        Transferrable = c.Boolean(nullable: false),
                        Refundable = c.Boolean(nullable: false),
                        Exchangable = c.Boolean(nullable: false),
                        FrequentFlyerPoints = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TicketTypes");
        }
    }
}
