namespace ZotCode.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EnableNullAddress : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Users", "BillingAddressId", "dbo.Addresses");
            DropForeignKey("dbo.Users", "PostalAddressId", "dbo.Addresses");
            DropIndex("dbo.Users", new[] { "PostalAddressId" });
            DropIndex("dbo.Users", new[] { "BillingAddressId" });
            AlterColumn("dbo.Users", "PostalAddressId", c => c.Int());
            AlterColumn("dbo.Users", "BillingAddressId", c => c.Int());
            CreateIndex("dbo.Users", "PostalAddressId");
            CreateIndex("dbo.Users", "BillingAddressId");
            AddForeignKey("dbo.Users", "BillingAddressId", "dbo.Addresses", "Id");
            AddForeignKey("dbo.Users", "PostalAddressId", "dbo.Addresses", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "PostalAddressId", "dbo.Addresses");
            DropForeignKey("dbo.Users", "BillingAddressId", "dbo.Addresses");
            DropIndex("dbo.Users", new[] { "BillingAddressId" });
            DropIndex("dbo.Users", new[] { "PostalAddressId" });
            AlterColumn("dbo.Users", "BillingAddressId", c => c.Int(nullable: false));
            AlterColumn("dbo.Users", "PostalAddressId", c => c.Int(nullable: false));
            CreateIndex("dbo.Users", "BillingAddressId");
            CreateIndex("dbo.Users", "PostalAddressId");
            AddForeignKey("dbo.Users", "PostalAddressId", "dbo.Addresses", "Id", cascadeDelete: false);
            AddForeignKey("dbo.Users", "BillingAddressId", "dbo.Addresses", "Id", cascadeDelete: false);
        }
    }
}
