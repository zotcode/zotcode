namespace ZotCode.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DatabaseSchema : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AirPorts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Code = c.String(),
                        CountryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.CountryId, cascadeDelete: false)
                .Index(t => t.CountryId);
            
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InvoiceDate = c.DateTime(nullable: false),
                        UserId = c.Int(nullable: false),
                        Passenger_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Passengers", t => t.Passenger_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.Passenger_Id);
            
            CreateTable(
                "dbo.Tickets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TicketCode = c.String(),
                        TicketName = c.String(),
                        TicketPrice = c.Double(nullable: false),
                        AllocatedSeatId = c.Int(nullable: false),
                        IsRefundable = c.Boolean(nullable: false),
                        IsTransferable = c.Boolean(nullable: false),
                        IsExchangeable = c.Boolean(nullable: false),
                        FrequentFlyerPoints = c.Int(nullable: false),
                        InvoiceId = c.Int(nullable: false),
                        PassengerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Seats", t => t.AllocatedSeatId, cascadeDelete: false)
                .ForeignKey("dbo.Invoices", t => t.InvoiceId, cascadeDelete: false)
                .ForeignKey("dbo.Passengers", t => t.PassengerId, cascadeDelete: false)
                .Index(t => t.AllocatedSeatId)
                .Index(t => t.InvoiceId)
                .Index(t => t.PassengerId);
            
            CreateTable(
                "dbo.Seats",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PlaneId = c.Int(nullable: false),
                        FlightId = c.Int(nullable: false),
                        IsBooked = c.Boolean(),
                        Row = c.Int(nullable: false),
                        Column = c.Int(nullable: false),
                        Class = c.Int(nullable: false),
                        SeatPriceId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Flights", t => t.FlightId, cascadeDelete: false)
                .ForeignKey("dbo.Planes", t => t.PlaneId, cascadeDelete: false)
                .ForeignKey("dbo.SeatPrices", t => t.SeatPriceId, cascadeDelete: false)
                .Index(t => t.PlaneId)
                .Index(t => t.FlightId)
                .Index(t => t.SeatPriceId);
            
            CreateTable(
                "dbo.Flights",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PlaneId = c.Int(nullable: false),
                        DepartureDate = c.DateTime(nullable: false),
                        DepartingFromId = c.Int(nullable: false),
                        ArrivalDate = c.DateTime(nullable: false),
                        ArrivingAtId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AirPorts", t => t.ArrivingAtId, cascadeDelete: false)
                .ForeignKey("dbo.AirPorts", t => t.DepartingFromId, cascadeDelete: false)
                .ForeignKey("dbo.Planes", t => t.PlaneId, cascadeDelete: false)
                .Index(t => t.PlaneId)
                .Index(t => t.DepartingFromId)
                .Index(t => t.ArrivingAtId);
            
            CreateTable(
                "dbo.Planes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AirLine = c.String(),
                        PlaneType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SeatPrices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Value = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Passengers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        EmailAddress = c.String(),
                        PostalAddress = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Addresses", "AddressType", c => c.Int(nullable: false));
            DropColumn("dbo.CountryStates", "PostCode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CountryStates", "PostCode", c => c.Int(nullable: false));
            DropForeignKey("dbo.Invoices", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Tickets", "PassengerId", "dbo.Passengers");
            DropForeignKey("dbo.Invoices", "Passenger_Id", "dbo.Passengers");
            DropForeignKey("dbo.Tickets", "InvoiceId", "dbo.Invoices");
            DropForeignKey("dbo.Tickets", "AllocatedSeatId", "dbo.Seats");
            DropForeignKey("dbo.Seats", "SeatPriceId", "dbo.SeatPrices");
            DropForeignKey("dbo.Flights", "PlaneId", "dbo.Planes");
            DropForeignKey("dbo.Seats", "PlaneId", "dbo.Planes");
            DropForeignKey("dbo.Flights", "DepartingFromId", "dbo.AirPorts");
            DropForeignKey("dbo.Seats", "FlightId", "dbo.Flights");
            DropForeignKey("dbo.Flights", "ArrivingAtId", "dbo.AirPorts");
            DropForeignKey("dbo.AirPorts", "CountryId", "dbo.Countries");
            DropIndex("dbo.Flights", new[] { "ArrivingAtId" });
            DropIndex("dbo.Flights", new[] { "DepartingFromId" });
            DropIndex("dbo.Flights", new[] { "PlaneId" });
            DropIndex("dbo.Seats", new[] { "SeatPriceId" });
            DropIndex("dbo.Seats", new[] { "FlightId" });
            DropIndex("dbo.Seats", new[] { "PlaneId" });
            DropIndex("dbo.Tickets", new[] { "PassengerId" });
            DropIndex("dbo.Tickets", new[] { "InvoiceId" });
            DropIndex("dbo.Tickets", new[] { "AllocatedSeatId" });
            DropIndex("dbo.Invoices", new[] { "Passenger_Id" });
            DropIndex("dbo.Invoices", new[] { "UserId" });
            DropIndex("dbo.AirPorts", new[] { "CountryId" });
            DropColumn("dbo.Addresses", "AddressType");
            DropTable("dbo.Passengers");
            DropTable("dbo.SeatPrices");
            DropTable("dbo.Seats");
            DropTable("dbo.Planes");
            DropTable("dbo.Flights");
            DropTable("dbo.Tickets");
            DropTable("dbo.Invoices");
            DropTable("dbo.AirPorts");
        }
    }
}
