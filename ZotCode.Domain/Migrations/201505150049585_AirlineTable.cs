namespace ZotCode.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AirlineTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Airlines",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        CountryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.CountryId, cascadeDelete: true)
                .Index(t => t.CountryId);
            
            CreateTable(
                "dbo.PlaneAirlines",
                c => new
                    {
                        Plane_Id = c.Int(nullable: false),
                        Airline_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Plane_Id, t.Airline_Id })
                .ForeignKey("dbo.Planes", t => t.Plane_Id, cascadeDelete: true)
                .ForeignKey("dbo.Airlines", t => t.Airline_Id, cascadeDelete: true)
                .Index(t => t.Plane_Id)
                .Index(t => t.Airline_Id);
            
            AddColumn("dbo.Flights", "AirlineId", c => c.Int(nullable: false));
            CreateIndex("dbo.Flights", "AirlineId");
            AddForeignKey("dbo.Flights", "AirlineId", "dbo.Airlines", "Id", cascadeDelete: true);
            DropColumn("dbo.Planes", "AirLine");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Planes", "AirLine", c => c.String());
            DropForeignKey("dbo.Flights", "AirlineId", "dbo.Airlines");
            DropForeignKey("dbo.PlaneAirlines", "Airline_Id", "dbo.Airlines");
            DropForeignKey("dbo.PlaneAirlines", "Plane_Id", "dbo.Planes");
            DropForeignKey("dbo.Airlines", "CountryId", "dbo.Countries");
            DropIndex("dbo.PlaneAirlines", new[] { "Airline_Id" });
            DropIndex("dbo.PlaneAirlines", new[] { "Plane_Id" });
            DropIndex("dbo.Airlines", new[] { "CountryId" });
            DropIndex("dbo.Flights", new[] { "AirlineId" });
            DropColumn("dbo.Flights", "AirlineId");
            DropTable("dbo.PlaneAirlines");
            DropTable("dbo.Airlines");
        }
    }
}
