namespace ZotCode.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FlightCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Flights", "Code", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Flights", "Code");
        }
    }
}
