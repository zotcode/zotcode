using System.Data.Entity.Validation;
using System.Text;
using System.Threading;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ZotCode.Domain.Model.Identity;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace ZotCode.Domain.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using ZotCode.Domain.DataManager;
    using ZotCode.Domain.Model;

    internal sealed class Configuration : DbMigrationsConfiguration<FlightPubContext>
    {
        private UserManager<User, int> _userManager;
        private FlightPubContext _context;
        private RoleManager<Role, int> _roleManager;

        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        private void Init(FlightPubContext context)
        {
            _context = context;
            _roleManager = new RoleManager<Role, int>(new RoleStore<Role, int, UserRole>(_context));
            _userManager = new UserManager<User, int>(new UserStore<User, Role, int, UserLogin, UserRole, UserClaim>(context));
        }

        protected override void Seed(FlightPubContext context)
        {
            Init(context);

            var users = GetUsers();

            log.Info("Data has been generated. Seeding database...");

            context.Users.AddOrUpdate(users);
            CreateRoles(users);
            CreateAdminPasswords(users);

            SaveChanges(context);

            log.Info("Database Seeding complete...");
        }

        private User[] GetUsers()
        {
            var users = new[]
            {
                new User
                {
                    Id = 1,
                    UserName = "jcrompton",
                    Email = "c3165877@uon.edu.au",
                    FirstName="Joshua",
                    LastName="Crompton",
                    DateOfBirth=DateTime.Parse("1993-01-01"),
                    EmailConfirmed = true,
                    PasswordHash = _userManager.PasswordHasher.HashPassword("12345"),
                    SecurityStamp = Guid.NewGuid().ToString()
                },
                new User
                {
                    Id = 2,
                    UserName = "lcooper",
                    Email = "leigh.cooper@uon.edu.au",
                    FirstName="Leigh",
                    LastName="Cooper",
                    DateOfBirth=DateTime.Parse("1992-02-02"),
                    EmailConfirmed = true,
                    PasswordHash = _userManager.PasswordHasher.HashPassword("12345"),
                    SecurityStamp = Guid.NewGuid().ToString()

                },
                new User
                {
                    Id = 3,
                    UserName = "ndavidson",
                    Email = "nathan.davidson@uon.edu.au",
                    FirstName="Nathan",
                    LastName="Davidson",
                    DateOfBirth=DateTime.Parse("1994-03-03"),
                    EmailConfirmed = true,
                    PasswordHash = _userManager.PasswordHasher.HashPassword("12345"),
                    SecurityStamp = Guid.NewGuid().ToString()

                },
                new User
                {
                    Id = 4,
                    UserName = "shartcher",
                    Email = "c3185790@uon.edu.au",
                    FirstName="Simon",
                    LastName="Hartcher",
                    DateOfBirth=DateTime.Parse("1985-04-04"),
                    EmailConfirmed = true,
                    PasswordHash = _userManager.PasswordHasher.HashPassword("12345"),
                    SecurityStamp = Guid.NewGuid().ToString()

                },
                new User
                {
                    Id = 5,
                    UserName = "thaigh",
                    Email = "c3182929@uon.edu.au",
                    FirstName="Tyler",
                    LastName="Haigh",
                    DateOfBirth=DateTime.Parse("1994-05-05"),
                    EmailConfirmed = true,
                    PasswordHash = _userManager.PasswordHasher.HashPassword("12345"),
                    SecurityStamp = Guid.NewGuid().ToString()

                }
            };

            return users;
        }

        private void SaveChanges(DbContext context)
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                StringBuilder sb = new StringBuilder();

                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new DbEntityValidationException(
                    "Entity Validation Failed - errors follow:\n" +
                    sb.ToString(), ex
                ); // Add the original exception as the innerException
            }
        }

        private void CreateRoles(User[] adminUsers)
        {
            if (!_roleManager.RoleExists("Admin"))
            {
                _roleManager.Create(new Role {Name = "Admin"});
            }

            foreach (var user in adminUsers)
            {
                if (!_userManager.IsInRole(user.Id, "Admin"))
                {
                    _userManager.AddToRole(user.Id, "Admin");
                }
            }
        }

        private void CreateAdminPasswords(User[] adminUsers)
        {
            foreach (var user in adminUsers)
            {
                if (user.PasswordHash == null)
                {
                    user.PasswordHash = _userManager.PasswordHasher.HashPassword("12345");
                }
            }
        }
    }
}
