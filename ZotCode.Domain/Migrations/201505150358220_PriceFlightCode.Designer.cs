// <auto-generated />
namespace ZotCode.Domain.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class PriceFlightCode : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(PriceFlightCode));
        
        string IMigrationMetadata.Id
        {
            get { return "201505150358220_PriceFlightCode"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
