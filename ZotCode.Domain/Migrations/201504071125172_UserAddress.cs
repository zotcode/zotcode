namespace ZotCode.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserAddress : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UnitNumber = c.Int(),
                        StreetNumber = c.Int(nullable: false),
                        StreetName = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Country = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        PostalAddressId = c.Int(nullable: false),
                        BillingAddressId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Addresses", t => t.BillingAddressId, cascadeDelete: false)
                .ForeignKey("dbo.Addresses", t => t.PostalAddressId, cascadeDelete: false)
                .Index(t => t.PostalAddressId)
                .Index(t => t.BillingAddressId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "PostalAddressId", "dbo.Addresses");
            DropForeignKey("dbo.Users", "BillingAddressId", "dbo.Addresses");
            DropIndex("dbo.Users", new[] { "BillingAddressId" });
            DropIndex("dbo.Users", new[] { "PostalAddressId" });
            DropTable("dbo.Users");
            DropTable("dbo.Addresses");
        }
    }
}
