namespace ZotCode.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DistancesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Distances",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AirPortAId = c.Int(nullable: false),
                        AirPortBId = c.Int(nullable: false),
                        DistanceInKms = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AirPorts", t => t.AirPortAId, cascadeDelete: false)
                .ForeignKey("dbo.AirPorts", t => t.AirPortBId, cascadeDelete: false)
                .Index(t => t.AirPortAId)
                .Index(t => t.AirPortBId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Distances", "AirPortBId", "dbo.AirPorts");
            DropForeignKey("dbo.Distances", "AirPortAId", "dbo.AirPorts");
            DropIndex("dbo.Distances", new[] { "AirPortBId" });
            DropIndex("dbo.Distances", new[] { "AirPortAId" });
            DropTable("dbo.Distances");
        }
    }
}
