namespace ZotCode.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FlightsRedundancy : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Flights", "ArrivalTimeStopOver", c => c.DateTime());
            AddColumn("dbo.Flights", "StopOverId", c => c.Int());
            AddColumn("dbo.Flights", "DepartureTimeStopOver", c => c.DateTime());
            AddColumn("dbo.Flights", "Duration", c => c.Int(nullable: false));
            AddColumn("dbo.Flights", "DurationSecondLeg", c => c.Int());
            CreateIndex("dbo.Flights", "StopOverId");
            AddForeignKey("dbo.Flights", "StopOverId", "dbo.AirPorts", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Flights", "StopOverId", "dbo.AirPorts");
            DropIndex("dbo.Flights", new[] { "StopOverId" });
            DropColumn("dbo.Flights", "DurationSecondLeg");
            DropColumn("dbo.Flights", "Duration");
            DropColumn("dbo.Flights", "DepartureTimeStopOver");
            DropColumn("dbo.Flights", "StopOverId");
            DropColumn("dbo.Flights", "ArrivalTimeStopOver");
        }
    }
}
