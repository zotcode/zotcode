namespace ZotCode.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveSeatPrice : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Seats", "SeatPriceId", "dbo.SeatPrices");
            RenameTable(name: "dbo.Seats", newName: "Availabilities");
            DropIndex("dbo.Availabilities", new[] { "SeatPriceId" });
            DropColumn("dbo.Availabilities", "SeatPriceId");
            DropTable("dbo.SeatPrices");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SeatPrices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Value = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Availabilities", "SeatPriceId", c => c.Int(nullable: false));
            CreateIndex("dbo.Availabilities", "SeatPriceId");
            RenameTable(name: "dbo.Availabilities", newName: "Seats");
            AddForeignKey("dbo.Seats", "SeatPriceId", "dbo.SeatPrices", "Id", cascadeDelete: true);
        }
    }
}
