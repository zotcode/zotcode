namespace ZotCode.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Availabilities : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Availabilities", "PlaneId", "dbo.Planes");
            DropIndex("dbo.Availabilities", new[] { "PlaneId" });
            RenameColumn(table: "dbo.Availabilities", name: "PlaneId", newName: "Plane_Id");
            AddColumn("dbo.Availabilities", "TicketTypeId", c => c.Int(nullable: false));
            AddColumn("dbo.Availabilities", "AvailableLeg1", c => c.Int(nullable: false));
            AddColumn("dbo.Availabilities", "AvailableLeg2", c => c.Int());
            AlterColumn("dbo.Availabilities", "Plane_Id", c => c.Int());
            CreateIndex("dbo.Availabilities", "TicketTypeId");
            CreateIndex("dbo.Availabilities", "Plane_Id");
            AddForeignKey("dbo.Availabilities", "TicketTypeId", "dbo.TicketTypes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Availabilities", "Plane_Id", "dbo.Planes", "Id");
            DropColumn("dbo.Availabilities", "IsBooked");
            DropColumn("dbo.Availabilities", "Row");
            DropColumn("dbo.Availabilities", "Column");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Availabilities", "Column", c => c.Int(nullable: false));
            AddColumn("dbo.Availabilities", "Row", c => c.Int(nullable: false));
            AddColumn("dbo.Availabilities", "IsBooked", c => c.Boolean());
            DropForeignKey("dbo.Availabilities", "Plane_Id", "dbo.Planes");
            DropForeignKey("dbo.Availabilities", "TicketTypeId", "dbo.TicketTypes");
            DropIndex("dbo.Availabilities", new[] { "Plane_Id" });
            DropIndex("dbo.Availabilities", new[] { "TicketTypeId" });
            AlterColumn("dbo.Availabilities", "Plane_Id", c => c.Int(nullable: false));
            DropColumn("dbo.Availabilities", "AvailableLeg2");
            DropColumn("dbo.Availabilities", "AvailableLeg1");
            DropColumn("dbo.Availabilities", "TicketTypeId");
            RenameColumn(table: "dbo.Availabilities", name: "Plane_Id", newName: "PlaneId");
            CreateIndex("dbo.Availabilities", "PlaneId");
            AddForeignKey("dbo.Availabilities", "PlaneId", "dbo.Planes", "Id", cascadeDelete: true);
        }
    }
}
