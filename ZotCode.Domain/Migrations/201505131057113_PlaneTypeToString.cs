namespace ZotCode.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PlaneTypeToString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Planes", "PlaneType", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Planes", "PlaneType", c => c.Int(nullable: false));
        }
    }
}
