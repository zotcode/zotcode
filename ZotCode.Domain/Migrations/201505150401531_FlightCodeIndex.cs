namespace ZotCode.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FlightCodeIndex : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Flights", "Code", c => c.String(maxLength: 6));
            CreateIndex("dbo.Flights", "Code");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Flights", new[] { "Code" });
            AlterColumn("dbo.Flights", "Code", c => c.String());
        }
    }
}
