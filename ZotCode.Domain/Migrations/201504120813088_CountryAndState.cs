namespace ZotCode.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CountryAndState : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CountryId = c.String(),
                        Code = c.String(),
                        Name = c.String(),
                        LongName = c.String(),
                        AlternateLongName = c.String(),
                        MotherCountryId = c.Int(),
                        MotherCountryComments = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.MotherCountryId)
                .Index(t => t.MotherCountryId);
            
            CreateTable(
                "dbo.CountryStates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Code = c.String(),
                        PostCode = c.Int(nullable: false),
                        CountryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.CountryId, cascadeDelete: false)
                .Index(t => t.CountryId);
            
            AddColumn("dbo.Addresses", "StateId", c => c.Int(nullable: false));
            AddColumn("dbo.Addresses", "CountryId", c => c.Int(nullable: false));
            CreateIndex("dbo.Addresses", "StateId");
            CreateIndex("dbo.Addresses", "CountryId");
            AddForeignKey("dbo.Addresses", "CountryId", "dbo.Countries", "Id", cascadeDelete: false);
            AddForeignKey("dbo.Addresses", "StateId", "dbo.CountryStates", "Id", cascadeDelete: false);
            DropColumn("dbo.Addresses", "State");
            DropColumn("dbo.Addresses", "Country");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Addresses", "Country", c => c.String());
            AddColumn("dbo.Addresses", "State", c => c.String());
            DropForeignKey("dbo.Addresses", "StateId", "dbo.CountryStates");
            DropForeignKey("dbo.CountryStates", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.Addresses", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.Countries", "MotherCountryId", "dbo.Countries");
            DropIndex("dbo.CountryStates", new[] { "CountryId" });
            DropIndex("dbo.Countries", new[] { "MotherCountryId" });
            DropIndex("dbo.Addresses", new[] { "CountryId" });
            DropIndex("dbo.Addresses", new[] { "StateId" });
            DropColumn("dbo.Addresses", "CountryId");
            DropColumn("dbo.Addresses", "StateId");
            DropTable("dbo.CountryStates");
            DropTable("dbo.Countries");
        }
    }
}
