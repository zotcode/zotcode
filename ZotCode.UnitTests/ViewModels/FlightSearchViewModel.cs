﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZotCode.Domain.Model.System;

namespace ZotCode.UnitTests.ViewModels
{
    /* 
        An extended ViewModel which has a constructor for initalization - purely for better code refraction 
    */
    public class FlightSearchViewModel : Web.Areas.Flight.ViewModels.FlightSearchViewModel
    {
        public FlightSearchViewModel(int adults, int children, int deptId, int destId, DateTime dt, bool oneWay, SeatClass sc, DateTime retDt = new DateTime())
        {
            this.Adults = adults;
            this.Children = children;
            this.DepartureId = deptId;
            this.DestinationId = destId;
            this.DepartureDate = dt;
            this.OneWay = oneWay;
            this.TicketClass = sc;

            // Optional argument, means nothing if OneWay is true.
            this.ReturnDate = retDt;
        }
    }
}
