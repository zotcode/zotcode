﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZotCode.Domain.Model;
using ZotCode.Domain.Model.System;

namespace ZotCode.UnitTests.DataManager
{
    class TestData
    {
        /* Mock test data for flights. */
        public static IQueryable<Flight> Flights
        {
            get
            {
                var flights = new List<Flight>();

                // Flight for Direct Flight tests 1
                var f1 = new Flight
                {
                    Id = 0,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "TF001",                                         // Test Flight 001
                    DepartingFromId = 0,                                    // Sydney
                    ArrivingAtId = 1,                                       // Melbourne
                    DepartureDate = new DateTime(2015, 10, 26, 9, 0, 0),    // 26 October 2015, 9am
                    ArrivalDate = new DateTime(2015, 10, 26, 10, 23, 0),    // 26 October 2015, 10:23am
                    Duration = 83,

                    // No Stopover
                    ArrivalTimeStopOver = null,
                    DepartureTimeStopOver = null,
                    StopOverId = null,
                    DurationSecondLeg = null
                };

                // Flight for Direct Flight tests 2
                var f2 = new Flight
                {
                    Id = 1,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "TF002",                                         // Test Flight 002
                    DepartingFromId = 0,                                    // Sydney
                    ArrivingAtId = 2,                                       // Adelaide
                    DepartureDate = new DateTime(2015, 7, 26, 9, 0, 0),     // 26 July 2015, 9am
                    ArrivalDate = new DateTime(2015, 7, 26, 12, 19, 0),     // 26 July 2015, 12:19pm
                    Duration = 83,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = new DateTime(2015, 7, 26, 10, 23, 0),     // 26 July 2015, 10:23am
                    DepartureTimeStopOver = new DateTime(2015, 7, 26, 11, 0, 0),    // 26 July 2015, 11:00am
                    StopOverId = 1,                                                 // Melbourne

                    DurationSecondLeg = 79
                };

                // Flight 1 for Direct Flight tests 3
                var f3 = new Flight
                {
                    Id = 2,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "TF003",                                         // Test Flight 003
                    DepartingFromId = 0,                                    // Sydney
                    ArrivingAtId = 1,                                       // Adelaide
                    DepartureDate = new DateTime(2015, 6, 26, 9, 0, 0),     // 26 June 2015, 9am
                    ArrivalDate = new DateTime(2015, 6, 26, 12, 19, 0),     // 26 June 2015, 12:19pm
                    Duration = 83,

                    // No stopover
                    ArrivalTimeStopOver = null,
                    DepartureTimeStopOver = null,
                    StopOverId = null,
                    DurationSecondLeg = null
                };

                // Flight 2 for Direct Flight tests 3
                var f4 = new Flight
                {
                    Id = 3,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "TF004",                                         // Test Flight 003
                    DepartingFromId = 0,                                    // Sydney
                    ArrivingAtId = 2,                                       // Adelaide
                    DepartureDate = new DateTime(2015, 6, 26, 9, 0, 0),     // 26 June 2015, 9am
                    ArrivalDate = new DateTime(2015, 6, 26, 12, 19, 0),     // 26 June 2015, 12:19pm
                    Duration = 83,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = new DateTime(2015, 6, 26, 10, 23, 0),     // 26 June 2015, 10:23am
                    DepartureTimeStopOver = new DateTime(2015, 6, 26, 11, 0, 0),    // 26 June 2015, 11:00am
                    StopOverId = 1,                                                 // Melbourne
                    DurationSecondLeg = 79
                };

                // Flight 1 for Two Legged Flight tests 1.
                // This flight continues on from StopOverAt from Flight TF002
                var f5 = new Flight
                {
                    Id = 4,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "TLF01",                                         // Two Legged Flight 01
                    DepartingFromId = 1,                                    // Melbourne
                    ArrivingAtId = 4,                                       // Darwin
                    DepartureDate = new DateTime(2015, 7, 26, 12, 0, 0),    // 26 July 2015, 12:00pm
                    ArrivalDate = new DateTime(2015, 7, 26, 20, 47, 0),     // 26 July 2015, 20:47pm
                    Duration = 234,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = new DateTime(2015, 7, 26, 15, 54, 0),     // 26 July 2015, 15:54pm
                    DepartureTimeStopOver = new DateTime(2015, 7, 26, 17, 0, 0),    // 26 July 2015, 17:00pm
                    StopOverId = 3,                                                 // Perth
                    DurationSecondLeg = 227
                };

                // Flight 1 for TwoLeggedFlightTest 2
                var f6 = new Flight
                {
                    Id = 5,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "TLF02",                                         // Two Legged Flight 02
                    DepartingFromId = 0,                                    // Sydney
                    ArrivingAtId = 2,                                       // Adelaide
                    DepartureDate = new DateTime(2015, 11, 26, 6, 0, 0),    // 26 November 2015, 6:00am
                    ArrivalDate = new DateTime(2015, 11, 26, 9, 15, 0),     // 26 July 2015, 20:47pm
                    Duration = 83,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = new DateTime(2015, 11, 26, 7, 23, 0),     // 26 July 2015, 15:54pm
                    DepartureTimeStopOver = new DateTime(2015, 11, 26, 8, 0, 0),    // 26 July 2015, 17:00pm
                    StopOverId = 1,                                                 // Melbourne

                    DurationSecondLeg = 75
                };

                // Flight 2 for TwoLeggedFlightTest 2
                var f7 = new Flight
                {
                    Id = 6,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "TLF03",                                         // Two Legged Flight 03
                    DepartingFromId = 1,                                    // Malbourne
                    ArrivingAtId = 3,                                       // Adelaide
                    DepartureDate = new DateTime(2015, 11, 26, 6, 0, 0),    // 26 November 2015, 6:00am
                    ArrivalDate = new DateTime(2015, 11, 26, 12, 54, 0),    // 26 July 2015, 20:47pm
                    Duration = 174,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = null,                             // 
                    DepartureTimeStopOver = null,                           // 
                    StopOverId = null,                                      // 
                    DurationSecondLeg = null
                };

                // Flight 1/2 for TwoLeggedFlightTest 2,3,4,5,6
                var f8 = new Flight
                {
                    Id = 7,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "TLF04",                                         // Two Legged Flight 04
                    DepartingFromId = 0,                                    // Sydney
                    ArrivingAtId = 3,                                       // Perth
                    DepartureDate = new DateTime(2015, 12, 5, 6, 0, 0),     // 5 December 2015, 6:00am
                    ArrivalDate = new DateTime(2015, 12, 5, 12, 10, 0),     // 5 December 2015, 12:10pm
                    Duration = 95,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = new DateTime(2015, 12, 5, 7, 35, 0),      // 5 December 2015, 7:35am
                    DepartureTimeStopOver = new DateTime(2015, 12, 5, 8, 0, 0),     // 5 December 2015, 8:00am
                    StopOverId = 1,                                                 // Melbourne
                    DurationSecondLeg = 250
                };

                // Flight 2/2 for TwoLeggedFlightTest 2
                var f9 = new Flight
                {
                    Id = 8,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "TLF05",                                         // Two Legged Flight 04
                    DepartingFromId = 1,                                    // Melbourne
                    ArrivingAtId = 5,                                       // Bali
                    DepartureDate = new DateTime(2015, 12, 5, 9, 0, 0),     // 5 December 2015, 9:00am
                    ArrivalDate = new DateTime(2015, 12, 5, 13, 40, 0),     // 5 December 2015, 13:05pm
                    Duration = 365,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = null,                             // 
                    DepartureTimeStopOver = null,                           // 
                    StopOverId = null,                                      // 
                    DurationSecondLeg = null
                };

                // Flight 1/2 for TwoLeggedFlightTest 3
                var f10 = new Flight
                {
                    Id = 9,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "TLF06",                                         // Two Legged Flight 06
                    DepartingFromId = 1,                                    // Melbourne
                    ArrivingAtId = 6,                                       // Brisbane
                    DepartureDate = new DateTime(2016, 1, 1, 6, 0, 0),      // 1 January 2016, 06:00am
                    ArrivalDate = new DateTime(2016, 1, 1, 10, 24, 0),      // 1 January 2016, 10:24am
                    Duration = 95,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = new DateTime(2016, 1, 1, 8, 35, 0),   // 1 January 2016, 08:35am
                    DepartureTimeStopOver = new DateTime(2016, 1, 1, 9, 0, 0),  // 1 January 2016, 09:00am
                    StopOverId = 0,                                             // Sydney
                    DurationSecondLeg = 84
                };

                // Flight 2/2 for TwoLeggedFlightTest 4
                var f11 = new Flight
                {
                    Id = 10,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "TLF07",                                         // Two Legged Flight 07
                    DepartingFromId = 6,                                    // Brisbane
                    ArrivingAtId = 2,                                       // Adelaide
                    DepartureDate = new DateTime(2016, 1, 1, 7, 9, 0),      // 1 January 2016, 07:09am
                    ArrivalDate = new DateTime(2016, 1, 1, 12, 05, 0),      // 1 January 2016, 12:05am
                    Duration = 84,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = new DateTime(2016, 1, 1, 8, 35, 0),   // 1 January 2016, 08:35am
                    DepartureTimeStopOver = new DateTime(2016, 1, 1, 10, 0, 0), // 1 January 2016, 10:00am
                    StopOverId = 0,                                             // Sydney
                    DurationSecondLeg = 125
                };

                // Flight 2/2 for TwoLeggedFlightTest 5
                var f12 = new Flight
                {
                    Id = 11,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "TLF08",                                         // Two Legged Flight 08
                    DepartingFromId = 6,                                    // Brisbane
                    ArrivingAtId = 5,                                       // Bali
                    DepartureDate = new DateTime(2016, 1, 1, 12, 0, 0),    // 1 January 2016, 12:00pm
                    ArrivalDate = new DateTime(2016, 1, 1, 19, 40, 0),      // 1 January 2016, 19:40pm
                    Duration = 240,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = new DateTime(2016, 1, 1, 16, 0, 0),   // 1 January 2016, 16:00am
                    DepartureTimeStopOver = new DateTime(2016, 1, 1, 17, 0, 0), // 1 January 2016, 17:00pm
                    StopOverId = 4,                                             // Darwin
                    DurationSecondLeg = 160
                };

                // Flight 2/2 for TwoLeggedFlightTest 6
                var f13 = new Flight
                {
                    Id = 12,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "TLF09",                                         // Two Legged Flight 09
                    DepartingFromId = 2,                                    // Adelaide
                    ArrivingAtId = 3,                                       // Perth
                    DepartureDate = new DateTime(2016, 1, 1, 8, 4, 0),      // 1 January 2016, 08:04pm
                    ArrivalDate = new DateTime(2016, 1, 1, 17, 15, 0),      // 1 January 2016, 17:15pm
                    Duration = 140,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = new DateTime(2016, 1, 1, 10, 24, 0),      // 1 January 2016, 10:24am
                    DepartureTimeStopOver = new DateTime(2016, 1, 1, 12, 0, 0),     // 1 January 2016, 12:00pm
                    StopOverId = 6,                                                 // Brisbane
                    DurationSecondLeg = 315
                };

                // Flight for Date Tests 1-6
                var f14 = new Flight
                {
                    Id = 13,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "DT001",                                         // Date Test 001
                    DepartingFromId = 0,                                    // Sydney
                    ArrivingAtId = 1,                                       // Melbourne
                    DepartureDate = new DateTime(2016, 2, 1, 8, 0, 0),      // 1 February 2016, 08:00am
                    ArrivalDate = new DateTime(2016, 2, 1, 9, 35, 0),       // 1 February 2016, 09:35am
                    Duration = 95,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = null,                             // 
                    DepartureTimeStopOver = null,                           // 
                    StopOverId = null,                                      // Brisbane
                    DurationSecondLeg = null
                };

                // Flight for Date Tests 7-9
                var f15 = new Flight
                {
                    Id = 14,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "DT002",                                         // Date Test 002
                    DepartingFromId = 0,                                    // Sydney
                    ArrivingAtId = 3,                                       // Perth
                    DepartureDate = new DateTime(2016, 3, 2, 8, 0, 0),      // 1 March 2016, 08:00am
                    ArrivalDate = new DateTime(2016, 3, 2, 15, 10, 0),      // 1 March 2016, 15:10pm
                    Duration = 95,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = new DateTime(2016, 3, 2, 9, 35, 0),   // 1 March 2016, 09:35am
                    DepartureTimeStopOver = new DateTime(2016, 3, 2, 11, 0, 0), // 1 March 2016, 11:00am
                    StopOverId = 1,                                             // Melbourne
                    DurationSecondLeg = 250
                };

                // Flight for Seat Test 1,2,3,4,5,6
                var f16 = new Flight
                {
                    Id = 15,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "ST001",                                         // Seat Test 001
                    DepartingFromId = 0,                                    // Sydney
                    ArrivingAtId = 3,                                       // Perth
                    DepartureDate = new DateTime(2016, 4, 2, 8, 0, 0),      // 1 March 2016, 08:00am
                    ArrivalDate = new DateTime(2016, 4, 2, 15, 10, 0),      // 1 March 2016, 15:10pm
                    Duration = 95,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = new DateTime(2016, 4, 2, 9, 35, 0),   // 1 March 2016, 09:35am
                    DepartureTimeStopOver = new DateTime(2016, 4, 2, 11, 0, 0), // 1 March 2016, 11:00am
                    StopOverId = 1,                                             // Melbourne
                    DurationSecondLeg = 250
                };

                // Flights (next two) for Seat Test 7 and 8.
                var f17 = new Flight
                {
                    Id = 16,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "ST002",                                         // Seat Test 002
                    DepartingFromId = 0,                                    // Sydney
                    ArrivingAtId = 3,                                       // Perth
                    DepartureDate = new DateTime(2016, 5, 2, 8, 0, 0),      // 2 May 2016, 08:00am
                    ArrivalDate = new DateTime(2016, 5, 2, 9, 35, 0),       // 2 May 2016, 09:35am
                    Duration = 95,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = null,
                    DepartureTimeStopOver = null,
                    StopOverId = null,
                    DurationSecondLeg = null
                };

                var f18 = new Flight
                {
                    Id = 17,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "ST003",                                         // Seat Test 003
                    DepartingFromId = 3,                                    // Sydney
                    ArrivingAtId = 5,                                       // Perth
                    DepartureDate = new DateTime(2016, 5, 2, 11, 0, 0),     // 2 May 2016, 11:00am
                    ArrivalDate = new DateTime(2016, 5, 2, 14, 40, 0),      // 2 May 2016, 14:40pm
                    Duration = 220,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = null,
                    DepartureTimeStopOver = null,
                    StopOverId = null,
                    DurationSecondLeg = null
                };

                // Flights (next three) for Seat Test 9, 10, and 11, as well as the first three legs 
                // of the four legged flight.
                var f19 = new Flight
                {
                    Id = 18,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "ST004",                                         // Seat Test 004
                    DepartingFromId = 0,                                    // Sydney
                    ArrivingAtId = 1,                                       // Melbourne
                    DepartureDate = new DateTime(2016, 6, 2, 8, 0, 0),      // 2 June 2016, 08:00am
                    ArrivalDate = new DateTime(2016, 6, 2, 9, 35, 0),       // 2 June 2016, 09:35am
                    Duration = 95,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = null,
                    DepartureTimeStopOver = null,
                    StopOverId = null,
                    DurationSecondLeg = null
                };

                var f20 = new Flight
                {
                    Id = 19,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "ST005",                                         // Seat Test 005
                    DepartingFromId = 1,                                    // Melbourne
                    ArrivingAtId = 2,                                       // Adelaide
                    DepartureDate = new DateTime(2016, 6, 2, 11, 0, 0),     // 2 June 2016, 11:00am
                    ArrivalDate = new DateTime(2016, 6, 2, 12, 20, 0),      // 2 June 2016, 12:20pm
                    Duration = 80,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = null,
                    DepartureTimeStopOver = null,
                    StopOverId = null,
                    DurationSecondLeg = null
                };

                var f21 = new Flight
                {
                    Id = 20,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "ST006",                                         // Seat Test 006
                    DepartingFromId = 2,                                    // Adelaide
                    ArrivingAtId = 3,                                       // Perth
                    DepartureDate = new DateTime(2016, 6, 2, 14, 0, 0),     // 2 June 2016, 14:00pm
                    ArrivalDate = new DateTime(2016, 6, 2, 17, 20, 0),      // 2 June 2016, 17:20pm
                    Duration = 200,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = null,
                    DepartureTimeStopOver = null,
                    StopOverId = null,
                    DurationSecondLeg = null
                };

                // Fourth leg of the four legged flight.
                var f22 = new Flight
                {
                    Id = 21,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "ST007",                                         // Seat Test 007
                    DepartingFromId = 3,                                    // Perth
                    ArrivingAtId = 4,                                       // Darwin
                    DepartureDate = new DateTime(2016, 6, 2, 19, 0, 0),     // 2 June 2016, 19:00pm
                    ArrivalDate = new DateTime(2016, 6, 2, 22, 35, 0),      // 2 June 2016, 22:25pm
                    Duration = 215,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = null,
                    DepartureTimeStopOver = null,
                    StopOverId = null,
                    DurationSecondLeg = null
                };

                // Next three flights are for Three Legged Flight Test 19 and 20
                var f23 = new Flight
                {
                    Id = 22,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "TLFT01",                                        // Three Legged Flight Test 01
                    DepartingFromId = 0,                                    // Sydney   
                    ArrivingAtId = 2,                                       // Adelaide
                    DepartureDate = new DateTime(2016, 7, 2, 5, 0, 0),      // 2 July 2016, 05:00am
                    ArrivalDate = new DateTime(2016, 7, 2, 8, 20, 0),       // 2 July 2016, 08:20am
                    Duration = 95,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = new DateTime(2016, 7, 2, 6, 35, 0),       // 2 July 2016, 06:35am
                    DepartureTimeStopOver = new DateTime(2016, 7, 2, 7, 0, 0),      // 2 July 2016, 07:00am
                    StopOverId = 1,                                                 // Melbourne
                    DurationSecondLeg = 80
                };

                var f24 = new Flight
                {
                    Id = 23,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "TLFT02",                                        // Three Legged Flight Test 02
                    DepartingFromId = 2,                                    // Adelaide   
                    ArrivingAtId = 4,                                       // Darwin
                    DepartureDate = new DateTime(2016, 7, 2, 10, 0, 0),     // 2 July 2016, 10:00am
                    ArrivalDate = new DateTime(2016, 7, 2, 17, 35, 0),      // 2 July 2016, 17:35pm
                    Duration = 200,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = new DateTime(2016, 7, 2, 13, 20, 0),      // 2 July 2016, 13:20pm
                    DepartureTimeStopOver = new DateTime(2016, 7, 2, 14, 0, 0),     // 2 July 2016, 14:00pm
                    StopOverId = 3,                                                 // Perth
                    DurationSecondLeg = 215
                };

                var f25 = new Flight
                {
                    Id = 24,
                    AirlineId = 0,                                          // Qantas Airways
                    PlaneId = 0,                                            // 747-100
                    Code = "TLFT03",                                        // Three Legged Flight Test 03
                    DepartingFromId = 4,                                    // Darwin   
                    ArrivingAtId = 6,                                       // Brisbane
                    DepartureDate = new DateTime(2016, 7, 2, 19, 0, 0),     // 2 July 2016, 19:00pm
                    ArrivalDate = new DateTime(2016, 7, 3, 4, 10, 0),       // 2 July 2016, 04:10am
                    Duration = 160,

                    // Destination airport is the stopover.
                    ArrivalTimeStopOver = new DateTime(2016, 7, 2, 21, 40, 0),      // 2 July 2016, 21:40pm
                    DepartureTimeStopOver = new DateTime(2016, 7, 2, 22, 0, 0),     // 2 July 2016, 22:00pm
                    StopOverId = 5,                                                 // Bali
                    DurationSecondLeg = 370
                };

                // Add flights to list.
                flights.Add(f1);
                flights.Add(f2);
                flights.Add(f3);
                flights.Add(f4);
                flights.Add(f5);
                flights.Add(f6);
                flights.Add(f7);
                flights.Add(f8);
                flights.Add(f9);
                flights.Add(f10);
                flights.Add(f11);
                flights.Add(f12);
                flights.Add(f13);
                flights.Add(f14);
                flights.Add(f15);
                flights.Add(f16);
                flights.Add(f17);
                flights.Add(f18);
                flights.Add(f19);
                flights.Add(f20);
                flights.Add(f21);
                flights.Add(f22);
                flights.Add(f23);
                flights.Add(f24);
                flights.Add(f25);
                // Return list.
                return flights.AsQueryable();
            }
        }

        /* Mock test data for airports. */
        public static IQueryable<AirPort> AirPorts
        {
            get
            {
                var airports = new List<AirPort>();

                var a1 = new AirPort
                {
                    Id = 0,
                    Name = "Sydney",
                    Code = "SYD",
                    CountryId = 0
                };

                var a2 = new AirPort
                {
                    Id = 1,
                    Name = "Melbourne",
                    Code = "MEL",
                    CountryId = 0
                };

                var a3 = new AirPort()
                {
                    Id = 2,
                    Name = "Adelaide",
                    Code = "ADE",
                    CountryId = 0
                };

                var a4 = new AirPort()
                {
                    Id = 3,
                    Name = "Perth",
                    Code = "PER",
                    CountryId = 0
                };

                var a5 = new AirPort()
                {
                    Id = 4,
                    Name = "Darwin",
                    Code = "DAR",
                    CountryId = 0
                };

                var a6 = new AirPort()
                {
                    Id = 5,
                    Name = "Ngurah Rai",
                    Code = "NGU",
                    CountryId = 1
                };

                var a7 = new AirPort()
                {
                    Id = 6,
                    Name = "Newcastle",
                    Code = "NEW",
                    CountryId = 1
                };

                // Add airports to list.
                airports.Add(a1);
                airports.Add(a2);
                airports.Add(a3);
                airports.Add(a4);
                airports.Add(a5);
                airports.Add(a6);
                airports.Add(a7);
                // Return list.
                return airports.AsQueryable();
            }
        }

        /* Mock test data for countries. */
        public static IQueryable<Country> Countries
        {
            get
            {
                var countries = new List<Country>();

                var c1 = new Country
                {
                    Id = 0,
                    CountryId = "AU",
                    Code = "AUS",
                    Name = "Australia",
                    LongName = "Australia"
                };

                var c2 = new Country
                {
                    Id = 1,
                    CountryId = "ID",
                    Code = "IDO",
                    Name = "Indonesia",
                    LongName = "Indonesia"
                };

                // Add countries to list.
                countries.Add(c1);
                countries.Add(c2);
                // Return list.
                return countries.AsQueryable();
            }
        }

        /* Mock test data for planes. */
        public static IQueryable<Plane> Planes
        {
            get
            {
                var planes = new List<Plane>();

                var p = new Plane
                {
                    Id = 0,
                    PlaneType = "747-100"
                };

                // Add planes to list.
                planes.Add(p);
                // Return list.
                return planes.AsQueryable();
            }
        }

        /* Mock test data for airlines. */
        public static IQueryable<Airline> Airlines
        {
            get
            {
                var airlines = new List<Airline>();

                var a = new Airline
                {
                    Id = 0,
                    Name = "Qantas Airways",
                    CountryId = 0,
                    Code = "QF"
                };

                // Add airlines to list.
                airlines.Add(a);
                // Return list.
                return airlines.AsQueryable();
            }
        }

        /* Mock test data for prices. */
        public static IQueryable<Price> Prices
        {
            get
            {
                var prices = new List<Price>();

                var p1 = new Price
                {
                    Id = 0,
                    AirlineId = 0,
                    SeatClass = SeatClass.Economy,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2015, 10, 26, 9, 0, 0),    // 26 October 2015, 9am
                    EndDate = new DateTime(2015, 10, 30, 9, 0, 0),      // 30 October 2015, 9am
                    Value = 0,
                    PriceLeg1 = 250,
                    PriceLeg2 = 0,
                    FlightCode = "TF001"
                };

                var p2 = new Price
                {
                    Id = 1,
                    AirlineId = 0,
                    SeatClass = SeatClass.Economy,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2015, 7, 26, 9, 0, 0),    // 26 July 2015, 9am
                    EndDate = new DateTime(2015, 7, 30, 9, 0, 0),      // 30 July 2015, 9am
                    Value = 0,
                    PriceLeg1 = 170,
                    PriceLeg2 = 110,
                    FlightCode = "TF002"
                };

                var p3 = new Price
                {
                    Id = 2,
                    AirlineId = 0,
                    SeatClass = SeatClass.Economy,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2015, 6, 26, 9, 0, 0),    // 26 June 2015, 9am
                    EndDate = new DateTime(2015, 6, 30, 9, 0, 0),      // 30 June 2015, 9am
                    Value = 0,
                    PriceLeg1 = 220,
                    PriceLeg2 = 0,
                    FlightCode = "TF003"
                };

                var p4 = new Price
                {
                    Id = 3,
                    AirlineId = 0,
                    SeatClass = SeatClass.Economy,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2015, 6, 26, 9, 0, 0),    // 26 June 2015, 9am
                    EndDate = new DateTime(2015, 6, 30, 9, 0, 0),      // 30 June 2015, 9am
                    Value = 0,
                    PriceLeg1 = 160,
                    PriceLeg2 = 200,
                    FlightCode = "TF004"
                };

                var p5 = new Price
                {
                    Id = 4,
                    AirlineId = 0,
                    SeatClass = SeatClass.Economy,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2015, 7, 1, 9, 0, 0),      // 1 July 2015, 9am
                    EndDate = new DateTime(2015, 8, 30, 9, 0, 0),       // 30 August 2015, 9am
                    Value = 0,
                    PriceLeg1 = 520,
                    PriceLeg2 = 700,
                    FlightCode = "TLF01"
                };

                var p6 = new Price
                {
                    Id = 5,
                    AirlineId = 0,
                    SeatClass = SeatClass.Economy,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2015, 10, 26, 9, 0, 0),    // 26 June 2015, 9am
                    EndDate = new DateTime(2015, 12, 30, 9, 0, 0),      // 30 June 2015, 9am
                    Value = 0,
                    PriceLeg1 = 70,
                    PriceLeg2 = 85,
                    FlightCode = "TLF02"
                };

                var p7 = new Price
                {
                    Id = 6,
                    AirlineId = 0,
                    SeatClass = SeatClass.Economy,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2015, 10, 1, 9, 0, 0),    // 1 July 2015, 9am
                    EndDate = new DateTime(2015, 12, 30, 9, 0, 0),     // 30 August 2015, 9am
                    Value = 0,
                    PriceLeg1 = 450,
                    PriceLeg2 = null,
                    FlightCode = "TLF03"
                };

                var p8 = new Price
                {
                    Id = 7,
                    AirlineId = 0,
                    SeatClass = SeatClass.Economy,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2015, 11, 30, 5, 0, 0),    // 1 July 2015, 9am
                    EndDate = new DateTime(2015, 12, 31, 9, 0, 0),     // 30 August 2015, 9am
                    Value = 0,
                    PriceLeg1 = 70,
                    PriceLeg2 = 310,
                    FlightCode = "TLF04"
                };

                var p9 = new Price
                {
                    Id = 8,
                    AirlineId = 0,
                    SeatClass = SeatClass.Economy,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2015, 11, 30, 5, 0, 0),    // 30 November 2015, 9am
                    EndDate = new DateTime(2015, 12, 31, 9, 0, 0),      // 31 December 2015, 9am
                    Value = 0,
                    PriceLeg1 = 720,
                    PriceLeg2 = null,
                    FlightCode = "TLF05"
                };

                var p10 = new Price
                {
                    Id = 9,
                    AirlineId = 0,
                    SeatClass = SeatClass.Economy,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2015, 11, 30, 5, 0, 0),    // 30 November 2015, 5am
                    EndDate = new DateTime(2016, 2, 25, 9, 0, 0),       // 30 February 2016, 9am
                    Value = 0,
                    PriceLeg1 = 110,
                    PriceLeg2 = 260,
                    FlightCode = "TLF06"
                };

                var p11 = new Price
                {
                    Id = 10,
                    AirlineId = 0,
                    SeatClass = SeatClass.Economy,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2015, 11, 30, 5, 0, 0),    // 30 November 2015, 5am
                    EndDate = new DateTime(2016, 2, 25, 9, 0, 0),       // 30 February 2016, 9am
                    Value = 0,
                    PriceLeg1 = 100,
                    PriceLeg2 = 300,
                    FlightCode = "TLF07"
                };

                var p12 = new Price
                {
                    Id = 11,
                    AirlineId = 0,
                    SeatClass = SeatClass.Economy,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2015, 11, 30, 5, 0, 0),    // 30 November 2015, 5am
                    EndDate = new DateTime(2016, 2, 25, 9, 0, 0),       // 30 February 2016, 9am
                    Value = 0,
                    PriceLeg1 = 450,
                    PriceLeg2 = 410,
                    FlightCode = "TLF08"
                };

                var p13 = new Price
                {
                    Id = 12,
                    AirlineId = 0,
                    SeatClass = SeatClass.Economy,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2015, 11, 30, 5, 0, 0),    // 30 November 2015, 5am
                    EndDate = new DateTime(2016, 2, 25, 9, 0, 0),       // 30 February 2016, 9am
                    Value = 0,
                    PriceLeg1 = 300,
                    PriceLeg2 = 605,
                    FlightCode = "TLF09"
                };

                var p14 = new Price
                {
                    Id = 13,
                    AirlineId = 0,
                    SeatClass = SeatClass.Economy,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2015, 11, 30, 5, 0, 0),    // 30 November 2015, 5am
                    EndDate = new DateTime(2016, 4, 25, 9, 0, 0),       // 25 April 2016, 9am
                    Value = 0,
                    PriceLeg1 = 79,
                    PriceLeg2 = null,
                    FlightCode = "DT001"
                };

                var p15 = new Price
                {
                    Id = 14,
                    AirlineId = 0,
                    SeatClass = SeatClass.Economy,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2016, 1, 1, 5, 0, 0),      // 1 January 2015, 5am
                    EndDate = new DateTime(2016, 4, 25, 9, 0, 0),       // 25 April 2016, 9am
                    Value = 0,
                    PriceLeg1 = 79,
                    PriceLeg2 = 290,
                    FlightCode = "DT002"
                };

                var p16 = new Price
                {
                    Id = 15,
                    AirlineId = 0,
                    SeatClass = SeatClass.Economy,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2016, 1, 1, 5, 0, 0),      // 1 January 2016, 5am
                    EndDate = new DateTime(2016, 4, 25, 9, 0, 0),       // 25 April 2016, 9am
                    Value = 0,
                    PriceLeg1 = 79,
                    PriceLeg2 = 290,
                    FlightCode = "ST001"
                };

                var p17 = new Price
                {
                    Id = 16,
                    AirlineId = 0,
                    SeatClass = SeatClass.Economy,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2016, 1, 1, 5, 0, 0),      // 1 January 2016, 5am
                    EndDate = new DateTime(2016, 6, 25, 9, 0, 0),       // 25 June 2016, 9am
                    Value = 0,
                    PriceLeg1 = 200,
                    PriceLeg2 = null,
                    FlightCode = "ST002"
                };

                var p18 = new Price
                {
                    Id = 17,
                    AirlineId = 0,
                    SeatClass = SeatClass.Economy,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2016, 1, 1, 5, 0, 0),      // 1 January 2016, 5am
                    EndDate = new DateTime(2016, 6, 25, 9, 0, 0),       // 25 June 2016, 9am
                    Value = 0,
                    PriceLeg1 = 650,
                    PriceLeg2 = null,
                    FlightCode = "ST003"
                };

                var p19 = new Price
                {
                    Id = 18,
                    AirlineId = 0,
                    SeatClass = SeatClass.Business,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2016, 1, 1, 5, 0, 0),      // 1 January 2016, 5am
                    EndDate = new DateTime(2016, 7, 25, 9, 0, 0),       // 25 July 2016, 9am
                    Value = 0,
                    PriceLeg1 = 69,
                    PriceLeg2 = null,
                    FlightCode = "ST004"
                };

                var p20 = new Price
                {
                    Id = 19,
                    AirlineId = 0,
                    SeatClass = SeatClass.Business,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2016, 1, 1, 5, 0, 0),      // 1 January 2016, 5am
                    EndDate = new DateTime(2016, 7, 25, 9, 0, 0),       // 25 July 2016, 9am
                    Value = 0,
                    PriceLeg1 = 50,
                    PriceLeg2 = null,
                    FlightCode = "ST005"
                };

                var p21 = new Price
                {
                    Id = 20,
                    AirlineId = 0,
                    SeatClass = SeatClass.Business,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2016, 1, 1, 5, 0, 0),      // 1 January 2016, 5am
                    EndDate = new DateTime(2016, 7, 25, 9, 0, 0),       // 25 July 2016, 9am
                    Value = 0,
                    PriceLeg1 = 800,
                    PriceLeg2 = null,
                    FlightCode = "ST006"
                };

                var p22 = new Price
                {
                    Id = 21,
                    AirlineId = 0,
                    SeatClass = SeatClass.Economy,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2016, 1, 1, 5, 0, 0),      // 1 January 2016, 5am
                    EndDate = new DateTime(2016, 7, 25, 9, 0, 0),       // 25 July 2016, 9am
                    Value = 0,
                    PriceLeg1 = 210,
                    PriceLeg2 = null,
                    FlightCode = "ST006"
                };

                var p23 = new Price
                {
                    Id = 22,
                    AirlineId = 0,
                    SeatClass = SeatClass.FirstClass,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2016, 1, 1, 5, 0, 0),      // 1 January 2016, 5am
                    EndDate = new DateTime(2016, 7, 25, 9, 0, 0),       // 25 July 2016, 9am
                    Value = 0,
                    PriceLeg1 = 1200,
                    PriceLeg2 = null,
                    FlightCode = "ST006"
                };

                var p24 = new Price
                {
                    Id = 23,
                    AirlineId = 0,
                    SeatClass = SeatClass.PremiumEconomy,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2016, 1, 1, 5, 0, 0),      // 1 January 2016, 5am
                    EndDate = new DateTime(2016, 7, 25, 9, 0, 0),       // 25 July 2016, 9am
                    Value = 0,
                    PriceLeg1 = 400,
                    PriceLeg2 = null,
                    FlightCode = "ST006"
                };

                var p25 = new Price
                {
                    Id = 24,
                    AirlineId = 0,
                    SeatClass = SeatClass.Business,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2016, 1, 1, 5, 0, 0),      // 1 January 2016, 5am
                    EndDate = new DateTime(2016, 7, 25, 9, 0, 0),       // 25 July 2016, 9am
                    Value = 0,
                    PriceLeg1 = 450,
                    PriceLeg2 = null,
                    FlightCode = "ST007"
                };

                var p26 = new Price
                {
                    Id = 25,
                    AirlineId = 0,
                    SeatClass = SeatClass.FirstClass,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2016, 2, 1, 5, 0, 0),      // 1 February 2016, 5am
                    EndDate = new DateTime(2016, 9, 25, 9, 0, 0),       // 25 Sept 2016, 9am
                    Value = 0,
                    PriceLeg1 = 600,
                    PriceLeg2 = 690,
                    FlightCode = "TLFT01"
                };

                var p27 = new Price
                {
                    Id = 26,
                    AirlineId = 0,
                    SeatClass = SeatClass.FirstClass,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2016, 2, 1, 5, 0, 0),      // 1 February 2016, 5am
                    EndDate = new DateTime(2016, 9, 25, 9, 0, 0),       // 25 Sept 2016, 9am
                    Value = 0,
                    PriceLeg1 = 850,
                    PriceLeg2 = 900,
                    FlightCode = "TLFT02"
                };

                var p28 = new Price
                {
                    Id = 27,
                    AirlineId = 0,
                    SeatClass = SeatClass.FirstClass,
                    TicketTypeId = 0,
                    StartDate = new DateTime(2016, 2, 1, 5, 0, 0),      // 1 February 2016, 5am
                    EndDate = new DateTime(2016, 9, 25, 9, 0, 0),       // 25 Sept 2016, 9am
                    Value = 0,
                    PriceLeg1 = 1000,
                    PriceLeg2 = 1100,
                    FlightCode = "TLFT03"
                };

                // Add prices to list.
                prices.Add(p1);
                prices.Add(p2);
                prices.Add(p3);
                prices.Add(p4);
                prices.Add(p5);
                prices.Add(p6);
                prices.Add(p7);
                prices.Add(p8);
                prices.Add(p9);
                prices.Add(p10);
                prices.Add(p11);
                prices.Add(p12);
                prices.Add(p13);
                prices.Add(p14);
                prices.Add(p15);
                prices.Add(p16);
                prices.Add(p17);
                prices.Add(p18);
                prices.Add(p19);
                prices.Add(p20);
                prices.Add(p21);
                prices.Add(p22);
                prices.Add(p23);
                prices.Add(p24);
                prices.Add(p25);
                prices.Add(p26);
                prices.Add(p27);
                prices.Add(p28);
                // Return list.
                return prices.AsQueryable();
            }
        }

        /* Mock test data for availabilities. */
        public static IQueryable<Availability> Availabilities
        {
            get
            {
                var availabilities = new List<Availability>();

                // Availability for FlightId 0
                var a1 = new Availability
                {
                    Id = 0,
                    FlightId = 0,
                    Class = SeatClass.Economy,
                    TicketTypeId = 0,
                    AvailableLeg1 = 1,
                    AvailableLeg2 = null
                };

                // Availability for FlightId 1
                var a2 = new Availability
                {
                    Id = 1,
                    FlightId = 1,
                    Class = SeatClass.Economy,
                    TicketTypeId = 0,
                    AvailableLeg1 = 1,
                    AvailableLeg2 = null
                };

                // Availability for FlightId 2
                var a3 = new Availability
                {
                    Id = 2,
                    FlightId = 2,
                    Class = SeatClass.Economy,
                    TicketTypeId = 0,
                    AvailableLeg1 = 1,
                    AvailableLeg2 = null
                };

                // Availability FlightId 3
                var a4 = new Availability
                {
                    Id = 3,
                    FlightId = 3,
                    Class = SeatClass.Economy,
                    TicketTypeId = 0,
                    AvailableLeg1 = 1,
                    AvailableLeg2 = null
                };

                // Availability for FlightId 4
                var a5 = new Availability
                {
                    Id = 4,
                    FlightId = 4,
                    Class = SeatClass.Economy,
                    TicketTypeId = 0,
                    AvailableLeg1 = 1,
                    AvailableLeg2 = 1
                };

                // Availability FlightId 5
                var a6 = new Availability
                {
                    Id = 5,
                    FlightId = 5,
                    Class = SeatClass.Economy,
                    TicketTypeId = 0,
                    AvailableLeg1 = 1,
                    AvailableLeg2 = 1
                };

                // Availability for FlightId 6
                var a7 = new Availability
                {
                    Id = 6,
                    FlightId = 6,
                    Class = SeatClass.Economy,
                    TicketTypeId = 0,
                    AvailableLeg1 = 1,
                    AvailableLeg2 = null
                };

                // Availability FlightId 5
                var a8 = new Availability
                {
                    Id = 7,
                    FlightId = 7,
                    Class = SeatClass.Economy,
                    TicketTypeId = 0,
                    AvailableLeg1 = 1,
                    AvailableLeg2 = 1
                };

                // Availability for FlightId 6
                var a9 = new Availability
                {
                    Id = 8,
                    FlightId = 8,
                    Class = SeatClass.Economy,
                    TicketTypeId = 0,
                    AvailableLeg1 = 1,
                    AvailableLeg2 = null
                };

                var a10 = new Availability
                {
                    Id = 9,
                    FlightId = 9,
                    Class = SeatClass.Economy,
                    TicketTypeId = 0,
                    AvailableLeg1 = 1,
                    AvailableLeg2 = 1
                };

                var a11 = new Availability
                {
                    Id = 10,
                    FlightId = 10,
                    Class = SeatClass.Economy,
                    TicketTypeId = 0,
                    AvailableLeg1 = 1,
                    AvailableLeg2 = 1
                };

                var a12 = new Availability
                {
                    Id = 11,
                    FlightId = 11,
                    Class = SeatClass.Economy,
                    TicketTypeId = 0,
                    AvailableLeg1 = 1,
                    AvailableLeg2 = 1
                };

                var a13 = new Availability
                {
                    Id = 12,
                    FlightId = 12,
                    Class = SeatClass.Economy,
                    TicketTypeId = 0,
                    AvailableLeg1 = 1,
                    AvailableLeg2 = 1
                };

                var a14 = new Availability
                {
                    Id = 13,
                    FlightId = 13,
                    Class = SeatClass.Economy,
                    TicketTypeId = 0,
                    AvailableLeg1 = 1,
                    AvailableLeg2 = null
                };

                var a15 = new Availability
                {
                    Id = 14,
                    FlightId = 14,
                    Class = SeatClass.Economy,
                    TicketTypeId = 0,
                    AvailableLeg1 = 1,
                    AvailableLeg2 = 1
                };

                var a16 = new Availability
                {
                    Id = 15,
                    FlightId = 15,
                    Class = SeatClass.Economy,
                    TicketTypeId = 0,
                    AvailableLeg1 = 0,
                    AvailableLeg2 = 0
                };

                var a17 = new Availability
                {
                    Id = 16,
                    FlightId = 16,
                    Class = SeatClass.Economy,
                    TicketTypeId = 0,
                    AvailableLeg1 = 0,
                    AvailableLeg2 = null
                };

                var a18 = new Availability
                {
                    Id = 17,
                    FlightId = 17,
                    Class = SeatClass.Economy,
                    TicketTypeId = 0,
                    AvailableLeg1 = 0,
                    AvailableLeg2 = null
                };

                var a19 = new Availability
                {
                    Id = 18,
                    FlightId = 18,
                    Class = SeatClass.Business,
                    TicketTypeId = 0,
                    AvailableLeg1 = 0,
                    AvailableLeg2 = null
                };

                var a20 = new Availability
                {
                    Id = 19,
                    FlightId = 19,
                    Class = SeatClass.Business,
                    TicketTypeId = 0,
                    AvailableLeg1 = 0,
                    AvailableLeg2 = null
                };

                var a21 = new Availability
                {
                    Id = 20,
                    FlightId = 20,
                    Class = SeatClass.Business,
                    TicketTypeId = 0,
                    AvailableLeg1 = 0,
                    AvailableLeg2 = null
                };

                var a22 = new Availability
                {
                    Id = 21,
                    FlightId = 20,
                    Class = SeatClass.Economy,
                    TicketTypeId = 0,
                    AvailableLeg1 = 0,
                    AvailableLeg2 = null
                };

                var a23 = new Availability
                {
                    Id = 22,
                    FlightId = 20,
                    Class = SeatClass.FirstClass,
                    TicketTypeId = 0,
                    AvailableLeg1 = 0,
                    AvailableLeg2 = null
                };

                var a24 = new Availability
                {
                    Id = 23,
                    FlightId = 20,
                    Class = SeatClass.PremiumEconomy,
                    TicketTypeId = 0,
                    AvailableLeg1 = 0,
                    AvailableLeg2 = null
                };

                var a25 = new Availability
                {
                    Id = 24,
                    FlightId = 21,
                    Class = SeatClass.Business,
                    TicketTypeId = 0,
                    AvailableLeg1 = 0,
                    AvailableLeg2 = null
                };

                var a26 = new Availability
                {
                    Id = 25,
                    FlightId = 22,
                    Class = SeatClass.FirstClass,
                    TicketTypeId = 0,
                    AvailableLeg1 = 1,
                    AvailableLeg2 = 1
                };

                var a27 = new Availability
                {
                    Id = 26,
                    FlightId = 23,
                    Class = SeatClass.FirstClass,
                    TicketTypeId = 0,
                    AvailableLeg1 = 1,
                    AvailableLeg2 = 1
                };

                var a28 = new Availability
                {
                    Id = 27,
                    FlightId = 24,
                    Class = SeatClass.FirstClass,
                    TicketTypeId = 0,
                    AvailableLeg1 = 1,
                    AvailableLeg2 = 1
                };

                // Add availabilities to list.
                availabilities.Add(a1);
                availabilities.Add(a2);
                availabilities.Add(a3);
                availabilities.Add(a4);
                availabilities.Add(a5);
                availabilities.Add(a6);
                availabilities.Add(a7);
                availabilities.Add(a8);
                availabilities.Add(a9);
                availabilities.Add(a10);
                availabilities.Add(a11);
                availabilities.Add(a12);
                availabilities.Add(a13);
                availabilities.Add(a14);
                availabilities.Add(a15);
                availabilities.Add(a16);
                availabilities.Add(a17);
                availabilities.Add(a18);
                availabilities.Add(a19);
                availabilities.Add(a20);
                availabilities.Add(a21);
                availabilities.Add(a22);
                availabilities.Add(a23);
                availabilities.Add(a24);
                availabilities.Add(a25);
                availabilities.Add(a26);
                availabilities.Add(a27);
                availabilities.Add(a28);
                // Return list.
                return availabilities.AsQueryable();
            }
        }

        // Mock test data for users
        public static IQueryable<User> Users
        {
            get
            {
                var users = new List<User>();

                var u1 = new User
                {
                    FirstName = "Monkey",
                    LastName = "Brains",
                    DateOfBirth = Convert.ToDateTime("1985-04-04"),
                    PostalAddress = new Address
                    {
                        Id = 1,
                        UnitNumber = 2,
                        StreetNumber = 1,
                        StreetName = "Hogan Street",
                        City = "Newcastle",
                        PostCode = 2000,
                        State = "NSW",
                        CountryId = 0, //Australia
                        Country = TestData.Countries.ToList()[0], //Australia
                    },
                    BillingAddress = new Address
                    {
                        Id = 1,
                        UnitNumber = 2,
                        StreetNumber = 1,
                        StreetName = "Hogan Street",
                        City = "Newcastle",
                        PostCode = 2000,
                        State = "NSW",
                        CountryId = 0,
                        Country = TestData.Countries.ToList()[0],
                    }

                };

                users.Add(u1);

                return users.AsQueryable();
            }
        }
    }
}