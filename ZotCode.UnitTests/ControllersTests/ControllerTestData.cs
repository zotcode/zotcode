﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using ZotCode.Domain.Model;

namespace ZotCode.UnitTests.ControllersTests
{
    public class ControllerTestData
    {
        public static IQueryable<User> Users
        {
            get
            {
                var users = new List<User>();

                var u1 = new User
                {
                    FirstName = "Monkey",
                    LastName = "Brains",
                    DateOfBirth = Convert.ToDateTime("1985-04-04"),
                    PostalAddress = new Address
                    {
                        Id = 1,
                        UnitNumber = 2,
                        StreetNumber = 1,
                        StreetName = "Hogan Street",
                        City = "Newcastle",
                        PostCode = 2000,
                        State = "NSW",
                        CountryId = 0, //Australia
                        Country = ZotCode.UnitTests.DataManager.TestData.Countries.ToList()[0], //Australia
                    },
                    BillingAddress = new Address
                    {
                        Id = 1,
                        UnitNumber = 2,
                        StreetNumber = 1,
                        StreetName = "Hogan Street",
                        City = "Newcastle",
                        PostCode = 2000,
                        State = "NSW",
                        CountryId = 0,
                        Country = ZotCode.UnitTests.DataManager.TestData.Countries.ToList()[0],
                    }

                };
                /*
                var u2 = new User
                {
                    FirstName = "",
                    LastName = "",
                    DateOfBirth = Convert.ToDateTime("1985-04-04"),
                    PostalAddress = new Address(),
                    BillingAddress = new Address()

                };

                var u3 = new User
                {
                    FirstName = "",
                    LastName = "",
                    DateOfBirth = Convert.ToDateTime("1985-04-04"),
                    PostalAddress = new Address(),
                    BillingAddress = new Address()

                };

                users.Add(u1);
                users.Add(u2);*/
                users.Add(u1);

                return users.AsQueryable();
            }
        }
    }
}
