﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZotCode.Web.Areas.Admin.Controllers;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using NUnit.Framework;


namespace ZotCode.UnitTests.ControllersTests
{
    class DashboardControllerTest
    {
        private DashboardController dbc;

        [SetUp]
        public void Init()
        {
            dbc = new DashboardController();
        }

        [Test]
        public void TextIndexView()
        {
            var result = dbc.Index();
            Assert.IsInstanceOf<ViewResultBase>(result);
            var viewResult = (ViewResultBase) result;
            var viewName = viewResult.ViewName;
            Assert.AreEqual(viewName, "Index");
        }
    }
}
