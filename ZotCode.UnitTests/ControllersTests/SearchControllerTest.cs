﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using NUnit.Framework;
using ZotCode.Domain.Model.System;
using ZotCode.UnitTests.ViewModels;
using ZotCode.Web.Areas.Flight.Controllers;
using MSAssert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace ZotCode.UnitTests.ControllersTests
{
    class SearchControllerTest
    {

        private SearchController searchController;
        private FlightSearchViewModel fsm;

        [SetUp]
        public void Init()
        {
            searchController = new SearchController();
            //(int adults, int children, int deptId, int destId, DateTime dt, 
            //             bool oneWay, SeatClass sc, DateTime retDt = new DateTime())
            fsm = new FlightSearchViewModel(0, 0, 0, 0, 
                Convert.ToDateTime("2015-04-04"), 
                false, 
                SeatClass.Business,
                Convert.ToDateTime("2015-05-05")
            );

        }

        [Test]
        public void TestIndexViewName()
        {
            var result = searchController.Index() as ViewResult;
            Assert.AreEqual("Index", result.ViewName); 
        }

        [Test]
        public void TestSearchResultRedirection()
        {
            var result = searchController.SearchResult(fsm) as RedirectToRouteResult;
            Assert.That(result.RouteValues["action"], Is.EqualTo("Result"));
        }

        public void TesthResultRedirection()
        {
            var result = searchController.Result(fsm) as RedirectToRouteResult;
            Assert.That(result.RouteValues["action"], Is.EqualTo("Result"));
        }

        public void TestDetailsResultRedirection()
        {
            var result = searchController.Details(0) as RedirectToRouteResult;
            Assert.That(result.RouteValues["action"], Is.EqualTo("Result"));
        }

        [Test]
        public void HttpBadRequet()
        {
            var searchResult = searchController.SearchResult(null);
            var result = searchController.Result(null);
            var oneWayResult = searchController.OneWayResult(null);
            var details = searchController.Details(null);

            // Check SearchResult()
            MSAssert.IsInstanceOfType(searchResult, typeof(HttpStatusCodeResult));
            HttpStatusCodeResult resultType1 = (HttpStatusCodeResult)searchResult;

            // 400 is bad request code
            Assert.AreEqual(resultType1.StatusCode, 400);

            // Check Result()
            MSAssert.IsInstanceOfType(result, typeof(HttpStatusCodeResult));
            HttpStatusCodeResult resultType2 = (HttpStatusCodeResult)result;
            Assert.AreEqual(resultType2.StatusCode, 400);

            // Check OneWayResult()
            MSAssert.IsInstanceOfType(oneWayResult, typeof(HttpStatusCodeResult));
            HttpStatusCodeResult resultType3 = (HttpStatusCodeResult)oneWayResult;
            Assert.AreEqual(resultType3.StatusCode, 400);

            // Check Details()
            MSAssert.IsInstanceOfType(details, typeof(HttpStatusCodeResult));
            HttpStatusCodeResult resultType4 = (HttpStatusCodeResult)details;
            Assert.AreEqual(resultType4.StatusCode, 400);
        }
    }
}