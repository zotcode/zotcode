﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Moq;
using NUnit.Framework;
using ZotCode.Domain.DataManager;
using ZotCode.Domain.Model;
using ZotCode.Web.Controllers;
using ZotCode.Web.ViewModels;

namespace ZotCode.UnitTests.ControllersTests
{
    class InvoiceControllerTest
    {
        private Mock<FlightPubContext> _dbMock;
        private InvoiceDetailsViewModel _ivm;
        private InvoiceController _invoiceController;

        [SetUp]
        public void Init()
        {
            _dbMock = new Mock<FlightPubContext>();
            _ivm = new InvoiceDetailsViewModel()
            {
                User = getUsers(),
                Flights = new Dictionary<int, Flight>()
                {
                    {0, getFlight() }
                },
                Invoice = new Invoice()
                {
                    User = getUsers(),
                    Id = 0,
                    InvoiceDate = DateTime.Today,
                    Tickets = new List<Ticket>(),
                    UserId = 0,
                },
                Tickets = new Dictionary<int, Ticket>(),
            };
            _invoiceController = new InvoiceController()
            {
                DbContext = _dbMock.Object,
            };
        }

        [Test]
        public void TestIndexView()
        {
            //var result = _invoiceController.Index() as ViewResult;
            //Assert.Equals(result.ViewName, "Index");
        }

        public Flight getFlight()
        {
            var f1 = new Flight
            {
                Id = 0,
                AirlineId = 0,                                          // Qantas Airways
                PlaneId = 0,                                            // 747-100
                Code = "TF001",                                         // Test Flight 001
                DepartingFromId = 0,                                    // Sydney
                ArrivingAtId = 1,                                       // Melbourne
                DepartureDate = new DateTime(2015, 10, 26, 9, 0, 0),    // 26 October 2015, 9am
                ArrivalDate = new DateTime(2015, 10, 26, 10, 23, 0),    // 26 October 2015, 10:23am
                Duration = 83,

                // No Stopover
                ArrivalTimeStopOver = null,
                DepartureTimeStopOver = null,
                StopOverId = null,
                DurationSecondLeg = null
            };
            return f1;
        }

        public User getUsers()
        {
            var u1 = new User()
            {
                FirstName = "Aaaaa",
                LastName = "Bbbbb",
                Id = 0,
                DateOfBirth = DateTime.Today,
                UserName = "a@b.com",
                BillingAddress = new Address()
                {
                    Id = 0,
                    CountryId = 0,
                    Country = new Country()
                    {
                        Id = 0,
                        CountryId = "AUS",
                        AirPorts = null,
                        Name = "Australia",
                        LongName = "Australia",
                        AlternateLongName = "Australia",
                        Code = "AUS",
                    },
                    State = "NSW",
                    City = "Newcastle",
                    StreetNumber = 1,
                    UnitNumber = 2,
                    StreetName = "Donald",
                    PostCode = 1000,
                    AddressType = AddressType.Postal,
                },
                PostalAddress = null,
                Email = "a@b.com",
            };

            return u1;
        }
    }
}
