﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Claims;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using NUnit.Framework;
using ZotCode.Domain.Model;
using ZotCode.Web.Areas.Admin.ViewModels;
using ZotCode.Web.Areas.Admin.Controllers;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using Moq;
using Should;
using ZotCode.Domain.DataManager;
using ZotCode.Domain.Model.Identity;
using ZotCode.UnitTests.DataManager;

namespace ZotCode.UnitTests.ControllersTests
{
    [TestFixture]
    public class UserControllerTest
    {
        private UserController _userController;
        private CreateUserViewModel _cvm;
        private Mock<FlightPubContext> dbMock;

        [SetUp]
        public void Init()
        {
            _cvm = new CreateUserViewModel
            {
                LastName = "Brains",
                FirstName = "Monkey",
                DateOfBirth = Convert.ToDateTime("1985-01-01"),
                EmailAddress = "monkeybrains@business.com",
                PlainPassword = "hobbit4life",
            };

            dbMock = new Mock<FlightPubContext>();
        }

        [Test]
        public void TestAdduser()
        {
            var users = new List<User>();

            var mockUsers = SetupMock(users);
            dbMock.Setup(u => u.Users).Returns(mockUsers.Object);

            _userController = new UserController()
            {
                 DbContext= dbMock.Object,
            };

            _userController.Create(_cvm);

            var expected = new User()
            {
                LastName = "Brains",
                FirstName = "Monkey",
                DateOfBirth = Convert.ToDateTime("1985-01-01"),
                UserName = "monkeybrains@business.com",
            }; 

            var actual = users.Single();
            expected.FirstName.ShouldEqual(actual.FirstName);
            expected.LastName.ShouldEqual(actual.LastName);
            expected.DateOfBirth.ShouldEqual(actual.DateOfBirth);
            expected.UserName.ShouldEqual(actual.UserName);
        }

        [Test]
        public void TestGetUserDetails()
        {
            var users = new List<User>();
            var mockUsers = SetupMock(users);
            dbMock.Setup(u => u.Users).Returns(mockUsers.Object);

            _userController = new UserController()
            {
                DbContext = dbMock.Object,
            };

            _userController.Create(_cvm);

            var actual = users.Single();

            //var monkey = _userController.Details(0);

        }

        public Mock<IDbSet<T>> SetupMock<T>(List<T> list) where T : class
        {
            var mockSet = new Mock<IDbSet<T>>();
            var queryable = list.AsQueryable();

            mockSet.Setup(m => m.Provider).Returns(queryable.Provider);
            mockSet.Setup(m => m.Expression).Returns(queryable.Expression);
            mockSet.Setup(m => m.ElementType).Returns(queryable.ElementType);
            mockSet.Setup(m => m.GetEnumerator()).Returns(queryable.GetEnumerator());
            mockSet.Setup(m => m.Add(It.IsAny<T>())).Callback<T>(list.Add);
            
            return mockSet;
        }

        [Test]
        public void HttpBadRequet()
        {
            var deTailResult1 = _userController.Details(null);
           

            // Check
            Assert.IsInstanceOfType(deTailResult1, typeof(HttpStatusCodeResult));
            HttpStatusCodeResult resultType1 = (HttpStatusCodeResult) deTailResult1;

            // 400 is bad request
            Assert.AreEqual(resultType1.StatusCode, 400);

            /*
             * These tests can't be executed due to two identically named methods
             * who both take possible null as parameter.
             * See testing plan for details
             * 
            var deTailResult2 = _userController.SetAddress(null);
            var deTailResult3 = _userController.UpdateAddress(null);

            // Check
            Assert.IsInstanceOfType(deTailResult2, typeof(HttpStatusCodeResult));
            HttpStatusCodeResult resultType2 = (HttpStatusCodeResult)deTailResult2;

            // Check
            Assert.IsInstanceOfType(deTailResult3, typeof(HttpStatusCodeResult));
            HttpStatusCodeResult resultType3 = (HttpStatusCodeResult)deTailResult3;
            
            
            Assert.AreEqual(resultType2.StatusCode, 400);
            Assert.AreEqual(resultType3.StatusCode, 400);
            */
        }

        [Test]
        public void GetIndexViewModel()
        {
            _userController = new UserController()
            {
                DbContext = dbMock.Object,
            };
            var users = new List<User>();
            var mockUsers = SetupMock(users);
            dbMock.Setup(u => u.Users).Returns(mockUsers.Object);
            var result = _userController.Index() as ViewResult;
            Assert.AreEqual("Index", result.ViewName);
        }

        [Test]
        public void TestCreatUserView()
        {
            _userController = new UserController()
            {
                DbContext = dbMock.Object,
            };
            var users = new List<User>();
            var mockUsers = SetupMock(users);
            dbMock.Setup(u => u.Users).Returns(mockUsers.Object);
            var result = _userController.Create(_cvm) as RedirectToRouteResult;
            Assert.AreEqual(result.RouteValues["action"], "Index");
            
        }
    }
}
