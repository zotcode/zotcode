﻿using ZotCode.Web.Controllers;
using System.Web.Mvc;
using NUnit.Framework;

namespace ZotCode.UnitTests
{
    [TestFixture]
    public class HomeControllerTest
    {
        private HomeController controller;

        /// <summary>
        /// Initial controller setup.
        /// </summary>
        [SetUp]
        public void Init()
        {
            controller = new HomeController();
        }

        /// <summary>
        /// Test the action method returning specific Index view.
        /// </summary>
        [Test]
        public void TestIndexView()
        {
            var result = controller.Index() as RedirectToRouteResult;
            Assert.That(result.RouteValues["action"], Is.EqualTo("Index"));
        }

        /// <summary>
        /// Test the action method returning specific About view.
        /// </summary>
        [Test]
        public void TestAboutView()
        {
            var result = controller.About() as ViewResult;
            Assert.AreEqual("About", result.ViewName);
        }

        /// <summary>
        /// Test the action method returning specific Contact view.
        /// </summary>
        [Test]
        public void TestContactView()
        {
            var result = controller.Contact() as ViewResult;
            Assert.AreEqual("Contact", result.ViewName);
        }
    }
}
