﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZotCode.Web.Areas.Admin.Controllers;
using System.Web.Mvc;
using ZotCode.Domain.DataManager;
using Moq;
using NUnit.Framework;
using ZotCode.Web.Controllers;
using ZotCode.Web.ViewModels;

namespace ZotCode.UnitTests.ControllersTests
{
    internal class ManageControllerTest
    {

        private ManageController _manageController;
        private Mock<FlightPubContext> _dbMock;

        [SetUp]
        public void Init()
        {
            _dbMock = new Mock<FlightPubContext>();
            _manageController = new ManageController()
            {
                DbContext = _dbMock.Object
            };
        }
    }
}
