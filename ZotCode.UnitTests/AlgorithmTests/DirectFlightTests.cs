﻿/* 
    Direct Flight tests:

    Description:    Tests whether the algorithm returns the correct results for various direct flights.
    Tests:      1.  Test if algorithm will return the correct number of flights for a given FlightSearchViewModel where
                    ArrivalAtId is the target destination.
                2.  Test if the algorithm will return the correct number of flights for a given FlightSearchViewModel where
                    StopOverAtId is the target destination.
                3.  Test if the algorithm will return the correct number of flights for a given FlightSearchViewModel where
                    there exists flights where ArrivalAtId and StopOverId are the target destination.
                4.  Test that the correct AirportId's are returned for a FlightSearchViewModel where the ArrivalAtId is the 
                    target destination.
                5.  Test that the correct AirportId's are returned for a FlightSearchViewModel where the StopOverAtId is the
                    target destination.
                6.  Test that the correct AirportId's are returned for a FlightSearchViewModel where a combination of flights
                    exist where ArrivalAtId and StopOverId are both the target id.

                1A.  Test correct cost is calculated for flight 1.
                1B.  Test correct flight duration is calculated for flight 1.
*/

using System;
using NUnit.Framework;
using ZotCode.Domain.Model.System;
using ZotCode.UnitTests.DataManager;
using ZotCode.Web.Flight.Models;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using FlightSearchViewModel = ZotCode.UnitTests.ViewModels.FlightSearchViewModel;

namespace ZotCode.UnitTests.AlgorithmTests
{
    [TestFixture]
    public class DirectFlightTests
    {
        private FakeFlightPubContext _dbContext;

        /* 
            Setup the databsae context.
        */
        [SetUp]
        public void Init()
        {
            _dbContext = new FakeFlightPubContext();
            _dbContext.AddSet(TestData.Flights);
            _dbContext.AddSet(TestData.AirPorts);
            _dbContext.AddSet(TestData.Countries);
            _dbContext.AddSet(TestData.Planes);
            _dbContext.AddSet(TestData.Airlines);
            _dbContext.AddSet(TestData.Prices);
            _dbContext.AddSet(TestData.Availabilities);
        }

        /*
            Test:               Sydney to Melbourne direct flight on 26 October 2015.
            Expected result:    1 flight exists.
            Tests Satisfied:    Test 1.
        */
        [Test]
        public void DirectFlightTest1()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2015, 10, 26, 9,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 1,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /*
            Test:               Sydney to Melbourne direct flight on 26 October 2015.
            Expected result:    Cost = 250.00
            Tests Satisfied:    Test 1A.
        */
        [Test]
        public void DirectFlightTest1A()
        {
            /* Test 1A Setup */
            var deptDateTime = new DateTime(2015, 10, 26, 9,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 1,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            foreach (var r in result.DepartureRoutes)
            {
                Assert.AreEqual(250.00, r.GetCostCheapest());
            }
        }

        /*
            Test:               Sydney to Melbourne direct flight on 26 October 2015.
            Expected result:    Duration = 83 minutes / 60 (convert to hours), precision rounded to 2 d.p.
            Tests Satisfied:    Test 1B.
        */
        [Test]
        public void DirectFlightTest1B()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2015, 10, 26, 9,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 1,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            foreach (var r in result.DepartureRoutes)
            {
                double timeExpected =
                    Math.Round((double)83 / 60, 2);
                // Convert minute to hours and round to 2 decimal places.
                Assert.AreEqual(timeExpected, r.Duration());
            }
        }

        /*
            Test:               Sydney to Melbourne direct with Melbourne as stopover enroute Adelaide flight on 26 July 2015.
            Expected result:    1 flight exists.
            Tests Satisfied:    Test 2.
        */

        [Test]
        public void DirectFlightTest2()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2015, 7, 26, 9,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 1,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /*
            Test:               Sydney to Melbourne direct as Melbourne either StopOver or Arrival flight on 26 June 2015.
            Expected result:    2 flights exists.
            Tests Satisfied:    Test 3.
        */

        [Test]
        public void DirectFlightTest3()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2015, 6, 26, 9,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 1,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(2, count);
        }

        /* 
            Test:               Test that the correct airportIds are returned for a direct flight where arrivalId is the target destination.
            Data:               A flight between Sydney to Melbourne with Melbourne being the target destination and arrivalId.
            Expected result:    1 flight, departureFromId = 0, arrivalAtId = 1.
            Tests Satisfied:    Test 4.
        */
        [Test]
        public void DirectFlightTest4()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2015, 10, 26, 9,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 1,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            // There should only be one flightroute, else fail test.
            if (count == 1)
            {
                // This test is for direct flights, therefore there should only be one flightleg in a flightroute - else fail test.
                if (result.DepartureRoutes.Count == 1)
                {
                    var f =
                        result.DepartureRoutes[0].FlightLegs
                            .First.Value;
                    Assert.AreEqual(0, f.DepartingFromId);
                    Assert.AreEqual(1, f.ArrivingAtId);
                }
                else
                {
                    Assert.Fail();
                }
            }
            else
            {
                Assert.Fail();
            }
        }

        /* 
            Test:               Test that the correct airportIds are returned for a direct flight where stopoverId is the target destination.
            Data:               A flight between Sydney to Melbourne with Melbourne being the target destination and stopoverId.
            Expected result:    1 flight, departureFromId = 0, stopoverId = 1.
            Tests Satisfied:    Test 5.
        */
        [Test]
        public void DirectFlightTest5()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2015, 7, 26, 9,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 1,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;

            // Test that all routes found return the correct airportIds.
            foreach (var route in result.DepartureRoutes)
            {
                // This test is for direct flights, therefore there should only be one flightleg in a flightroute - else fail test.
                if (route.FlightLegs.Count == 1)
                {
                    var f = route.FlightLegs.First.Value;
                    Assert.AreEqual(0, f.DepartingFromId);
                    Assert.AreEqual(1, f.StopOverId);
                }
                else 
                    Assert.Fail();
            }
        }

        /* 
            Test:               Test that the correct AirportId's are returned for a FlightSearchViewModel where a combination of flights
                                exist where ArrivalAtId and StopOverId are both the target id.
            Data:               Two flights, one between Sydney and Melbourne direct with no stopover, and a second with a stopover where the stopover is Melbourne - the
                                target destination.
            Expected result:    At least two FlightRoutes are found and foreach DepartureId will be 0, 
                                StopOverId or ArrivalAtId will be 1 for all returned flights.
            Tests Satisfied:    Test 6.
        */
        [Test]
        public void DirectFlightTest6()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2015, 6, 26, 9,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 1,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;

            // Fail if at least two flights aren't found.
            if(result.DepartureRoutes.Count <= 1)
                Assert.Fail();

            var passed = false;

            foreach (var route in result.DepartureRoutes)
            {
                foreach (var flight in route.FlightLegs)
                {
                    if (flight.ArrivingAtId == 1 ||
                        flight.StopOverId == 1)
                        passed = true;

                    Assert.AreEqual(true, passed);

                    passed = false;
                }
            }
        }
    }
}
