﻿/* 
    Descritpion:    For a given flight, test whether the flight is found given various dates.
    Tests:      1.  For an existing flight which has the departure date of yesterday, test whether the algorithm ignores it.
                2.  For an existing flight which has the departure date of exactly the same time as the FlightSearchViewModel
                    departure date, test whether the algorithm ignores it.
                3.  For an existing flight which has the departure date of exactly 1 hour in the future past the date given by
                    the FlightSearchViewModel, test whether the algorithm finds it.
                4.  For an existing flight which has the departure date of 1 day in the future, test whether the algorithm finds it.
                4A. For an existing flight which has the departure date of 23 hours 59 minutes and 59 seconds in the future,
                    test whether the algorithm finds it.
                4B. For an existing flight which has the departure date of 24 hours 0 minutes, and 1 second in the future
                    test whether the algorithm finds it.
                5.  For an existing flight which has the departure date of 2 days in the future, test whether the algorithm finds it.
                6.  For an existing flight which has the departure date of 47 hours in the future, test whether the algorithm finds it.
                7.  For an existing flight which has the stopover date of exactly the same time as the FlightSearchViewModel departure date.
                8.  For an existing flight which has the stopover date of 1 second earlier than the FlightSearchViewModel departure date.
                9.  For an existing flight which has the stopover date of 24 hours and 1 second later than the FlightSearchViewModel departure date.
*/

using System;
using NUnit.Framework;
using ZotCode.Domain.Model.System;
using ZotCode.UnitTests.DataManager;
using ZotCode.Web.Flight.Models;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using FlightSearchViewModel = ZotCode.UnitTests.ViewModels.FlightSearchViewModel;

namespace ZotCode.UnitTests.AlgorithmTests
{
    [TestFixture]
    public class DateTests
    {

        private FakeFlightPubContext _dbContext;

        /* 
            Setup the databsae context.
        */
        [SetUp]
        public void Init()
        {
            _dbContext = new FakeFlightPubContext();
            _dbContext.AddSet(TestData.Flights);
            _dbContext.AddSet(TestData.AirPorts);
            _dbContext.AddSet(TestData.Countries);
            _dbContext.AddSet(TestData.Planes);
            _dbContext.AddSet(TestData.Airlines);
            _dbContext.AddSet(TestData.Prices);
            _dbContext.AddSet(TestData.Availabilities);
        }

        /* 
            Test:               For an existing flight which has the departure date of yesterday, test whether the algorithm ignores it.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 1.
        */
        [Test]
        public void DateTest1()
        {
            var deptDateTime = new DateTime(2016, 2, 2, 8, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 1, deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For an existing flight which has the departure date of exactly the same time as the FlightSearchViewModel
                                departure date, test whether the algorithm ignores it.
            Expected result:    1 FlightRoutes returned.
            Tests Satisfied:    Test 2.
        */
        [Test]
        public void DateTest2()
        {
            var deptDateTime = new DateTime(2016, 2, 1, 8, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 1, deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /* 
            Test:               For an existing flight which has the departure date of exactly 1 hour in the future past the date given by
                                the FlightSearchViewModel, test whether the algorithm finds it.
            Expected result:    1 FlightRoutes returned.
            Tests Satisfied:    Test 3.
        */
        [Test]
        public void DateTest3()
        {
            var deptDateTime = new DateTime(2016, 2, 1, 7, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 1, deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /* 
            Test:               For an existing flight which has the departure date of 1 day in the future, test whether the algorithm finds it.
            Expected result:    1 FlightRoutes returned.
            Tests Satisfied:    Test 4.
        */
        [Test]
        public void DateTest4()
        {
            var deptDateTime = new DateTime(2016, 1, 31, 8, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 1, deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /* 
            Test:               For an existing flight which has the departure date of 23 hours 59 minutes and 59 seconds in the future,
                                test whether the algorithm finds it.
            Expected result:    1 FlightRoutes returned.
            Tests Satisfied:    Test 4A.
        */
        [Test]
        public void DateTest4A()
        {
            var deptDateTime = new DateTime(2016, 1, 31, 8, 0, 01);
            var model = new FlightSearchViewModel(1, 0, 0, 1, deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /* 
            Test:               For an existing flight which has the departure date of 24 hours 0 minutes, and 1 second in the future
                                test whether the algorithm finds it.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 4B.
        */
        [Test]
        public void DateTest4B()
        {
            var deptDateTime = new DateTime(2016, 1, 31, 7, 59, 59);
            var model = new FlightSearchViewModel(1, 0, 0, 1, deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For an existing flight which has the departure date of 2 days in the future, test whether the algorithm finds it.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 5.
        */
        [Test]
        public void DateTest5()
        {
            var deptDateTime = new DateTime(2016, 1, 30, 8, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 1, deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For an existing flight which has the departure date of 47 hours in the future, test whether the algorithm finds it.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 6.
        */
        [Test]
        public void DateTest6()
        {
            var deptDateTime = new DateTime(2016, 1, 30, 9, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 1, deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For an existing flight which has the stopover date of exactly the same time as the FlightSearchViewModel departure date.
            Expected result:    1 FlightRoutes returned.
            Tests Satisfied:    Test 7.
        */
        [Test]
        public void DateTest7()
        {
            var deptDateTime = new DateTime(2016, 3, 2, 11, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 1, 3, deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /* 
            Test:               For an existing flight which has the stopover date of 1 millisecond earlier than the FlightSearchViewModel departure date.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 8.
        */
        [Test]
        public void DateTest8()
        {
            var deptDateTime = new DateTime(2016, 3, 2, 11, 0, 1);
            var model = new FlightSearchViewModel(1, 0, 1, 3, deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For an existing flight which has the stopover date of 24 hours and 1 millisecond later than the FlightSearchViewModel departure date.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 9.
        */
        [Test]
        public void DateTest9()
        {
            var deptDateTime = new DateTime(2016, 3, 1, 10, 59, 59);
            var model = new FlightSearchViewModel(1, 0, 1, 3, deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }
    }
}