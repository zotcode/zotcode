﻿/* 
    Description:    For a given FlightSearchViewModel, and target Airports which are two legs away from departure Airports, test
                    whether the algorithm finds the FlightRoute.
    Tests:      1.  Flight 1 departs from DepartingFromId and arrives at StopOverId
                    Flight 2 departs from DepartingFromId and arrives at StopOverId [Destination]
                2.  Flight 1 departs from DepartingFromId and arrives at StopOverId
                    Flight 2 departs from DepartingFromId and arrives at ArrivingAtId [Destination]
                3.  Flight 1 departs from DepartingFromId and arrives at StopOverId
                    Flight 2 departs from StopOverId and arrives at ArrivingAtId [Destination]
                4.  Flight 1 departs from DepartingFromId and arrives at ArrivingAtId
                    Flight 2 departs from DepartingFromId and arrives at StopOverId [Destination]
                5.  Flight 1 departs from DepartingFromId and arrives at ArrivingAtId
                    Flight 2 departs from DepartingFromId and arrives at ArrivingAtId [Destination]
                6.  Flight 1 departs from DepartingFromId and arrives at ArrivingAtId
                    Flight 2 departs from StopOverId and arrives at ArrivingAtId [Destination]
                7.  Flight 1 departs from StopOverId and arrives at ArrivingAtId
                    Flight 2 departs from DepartingFromId and arrives at StopOverId [Destination]
                8.  Flight 1 departs from StopOverId and arrives at ArrivingAtId 
                    Flight 2 departs from StopOverId and arrives at ArrivingAtId [Destination]
                9.  Flight 1 departs from StopOverId and arrives at ArrivingAtId
                    Flight 2 departs from DepartingFromId and arrives at ArrivingAtId [Destination]

                A.  Test correct cost is calculated for the above flights.
                B.  Test correct flight duration is calculated for the above flights.
*/

using System;
using System.Linq;
using NUnit.Framework;
using ZotCode.Domain.Model.System;
using ZotCode.UnitTests.DataManager;
using ZotCode.Web.Flight.Models;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using FlightSearchViewModel = ZotCode.UnitTests.ViewModels.FlightSearchViewModel;

namespace ZotCode.UnitTests.AlgorithmTests
{
    [TestFixture]
    public class TwoLeggedFlightTests
    {

        private FakeFlightPubContext _dbContext;

        /* 
            Setup the databsae context.
        */
        [SetUp]
        public void Init()
        {
            _dbContext = new FakeFlightPubContext();
            _dbContext.AddSet(TestData.Flights);
            _dbContext.AddSet(TestData.AirPorts);
            _dbContext.AddSet(TestData.Countries);
            _dbContext.AddSet(TestData.Planes);
            _dbContext.AddSet(TestData.Airlines);
            _dbContext.AddSet(TestData.Prices);
            _dbContext.AddSet(TestData.Availabilities);
        }

        /*
            Test:               Flight 1 departs from DepartingFromId and arrives at StopOverId
                                Flight 2 departs from DepartingFromId and arrives at StopOverId [Destination]
            Expected result:    1 flight exists.
            Tests Satisfied:    Test 1.
        */
        [Test]
        public void TwoLeggedFlightTest1()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2015, 7, 26, 9,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 4,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /*
            Test:               Test correct cost is calculated for Test 1.
            Expected result:    690.00
            Tests Satisfied:    Test 1A.
        */
        [Test]
        public void TwoLeggedFlightTest1A()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2015, 7, 26, 9,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 4,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;

            if(result.DepartureRoutes.Count <= 0)
                Assert.Fail();

            foreach (var route in result.DepartureRoutes)
                Assert.AreEqual(690.00, route.GetCostCheapest());

        }

        /*
            Test:               Test correct flight duration is calculated for Test 1.
            Expected result:    6.9
            Tests Satisfied:    Test 1A.
        */
        [Test]
        public void TwoLeggedFlightTest1B()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2015, 7, 26, 9,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 4,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;

            if (result.DepartureRoutes.Count <= 0)
                Assert.Fail();

            foreach (var route in result.DepartureRoutes)
                Assert.AreEqual(6.9, route.Duration());
        }

        /*
            Test:               Flight 1 departs from DepartingFromId and arrives at StopOverId
                                Flight 2 departs from DepartingFromId and arrives at ArrivingAtId [Destination]
            Expected result:    1 FlightRoute
            Tests Satisfied:    Test 2.
        */
        [Test]
        public void TwoLeggedFlightTest2()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2015, 12, 5, 6,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 5,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /*
            Test:               Test correct cost is calculated for Test 2.
            Expected result:    790.00
            Tests Satisfied:    Test 2A.
        */
        [Test]
        public void TwoLeggedFlightTest2A()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2015, 12, 5, 6,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 5,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;

            if (result.DepartureRoutes.Count <= 0)
                Assert.Fail();

            foreach (var route in result.DepartureRoutes)
                Assert.AreEqual(790.00, route.GetCostCheapest());
        }

        /*
            Test:               Test correct flight duration is calculated for Test 2.
            Expected result:    7.67
            Tests Satisfied:    Test 2B.
        */
        [Test]
        public void TwoLeggedFlightTest2B()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2015, 12, 5, 6,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 5,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;

            if (result.DepartureRoutes.Count <= 0)
                Assert.Fail();

            foreach (var route in result.DepartureRoutes)
                Assert.AreEqual(7.67, route.Duration());
        }

        /*
            Test:               Flight 1 departs from DepartingFromId and arrives at StopOverId
                                Flight 2 departs from StopOverId and arrives at ArrivingAtId [Destination]
            Expected result:    1 FlightRoute
            Tests Satisfied:    Test 3.
        */
        [Test]
        public void TwoLeggedFlightTest3()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2016, 1, 1, 6,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 1, 2,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /*
            Test:               Test correct cost is calculated for Test 3.
            Expected result:    410.00
            Tests Satisfied:    Test 3A.
        */
        [Test]
        public void TwoLeggedFlightTest3A()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2016, 1, 1, 6,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 1, 2,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;

            if (result.DepartureRoutes.Count <= 0)
                Assert.Fail();

            foreach (var route in result.DepartureRoutes)
                Assert.AreEqual(410.00, route.GetCostCheapest());
        }

        /*
            Test:               Test correct flight duration is calculated for Test 3.
            Expected result:    6.08
            Tests Satisfied:    Test 3B.
        */
        [Test]
        public void TwoLeggedFlightTest3B()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2016, 1, 1, 6,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 1, 2,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;

            if (result.DepartureRoutes.Count <= 0)
                Assert.Fail();

            foreach (var route in result.DepartureRoutes)
                Assert.AreEqual(6.08, route.Duration());
        }

        /*
            Test:               Flight 1 departs from DepartingFromId and arrives at ArrivingAtId
                                Flight 2 departs from DepartingFromId and arrives at StopOverId [Destination]
            Expected result:    1 FlightRoute
            Tests Satisfied:    Test 4.
        */
        [Test]
        public void TwoLeggedFlightTest4()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2016, 1, 1, 6,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 1, 4,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /*
            Test:               Test correct cost is calculated for Test 4.
            Expected result:    920.00
            Tests Satisfied:    Test 4A.
        */
        [Test]
        public void TwoLeggedFlightTest4A()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2016, 1, 1, 6,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 1, 4,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;

            if (result.DepartureRoutes.Count <= 0)
                Assert.Fail();

            foreach (var route in result.DepartureRoutes)
                Assert.AreEqual(920.00, route.GetCostCheapest());
        }

        /*
            Test:               Test correct flight duration is calculated for Test 4.
            Expected result:    10.00
            Tests Satisfied:    Test 4B.
        */
        [Test]
        public void TwoLeggedFlightTest4B()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2016, 1, 1, 6,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 1, 4,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;

            if (result.DepartureRoutes.Count <= 0)
                Assert.Fail();

            foreach (var route in result.DepartureRoutes)
                Assert.AreEqual(10.00, route.Duration());
        }

        /*
            Test:               Flight 1 departs from DepartingFromId and arrives at ArrivingAtId
                                Flight 2 departs from DepartingFromId and arrives at ArrivingAtId [Destination]
            Expected result:    1 FlightRoute
            Tests Satisfied:    Test 5.
        */
        [Test]
        public void TwoLeggedFlightTest5()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2016, 1, 1, 5,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 1, 4,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /*
            Test:               Test correct cost is calculated for Test 5.
            Expected result:    1330.00
            Tests Satisfied:    Test 5.
        */
        [Test]
        public void TwoLeggedFlightTest5A()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2016, 1, 1, 5,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 1, 4,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;

            if (result.DepartureRoutes.Count <= 0)
                Assert.Fail();

            foreach (var route in result.DepartureRoutes)
                Assert.AreEqual(1330.00, route.GetCostCheapest());
        }

        /*
            Test:               Test correct flight duration is calculated for Test 5.
            Expected result:    13.67
            Tests Satisfied:    Test 5B.
        */
        [Test]
        public void TwoLeggedFlightTest5B()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2016, 1, 1, 5,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 1, 4,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;

            if (result.DepartureRoutes.Count <= 0)
                Assert.Fail();

            foreach (var route in result.DepartureRoutes)
                Assert.AreEqual(13.67, route.Duration());
        }

        /*
            Test:               Flight 1 departs from DepartingFromId and arrives at ArrivingAtId
                                Flight 2 departs from StopOverId and arrives at ArrivingAtId [Destination]
            Expected result:    1 FlightRoute
            Tests Satisfied:    Test 6.
        */
        [Test]
        public void TwoLeggedFlightTest6()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2016, 1, 1, 5,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 1, 3,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /*
            Test:               Test correct cost is calculated for Test 6.
            Expected result:    
            Tests Satisfied:    Test 6A.
        */
        [Test]
        public void TwoLeggedFlightTest6A()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2016, 1, 1, 5,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 1, 3,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            if (count == 1)
                Assert.AreEqual(975.00, result.DepartureRoutes.FirstOrDefault().GetCostCheapest());
        }

        /*
            Test:               Test correct flight duration is calculated for Test 6.
            Expected result:    11.25
            Tests Satisfied:    Test 6B.
        */
        [Test]
        public void TwoLeggedFlightTest6B()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2016, 1, 1, 5,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 1, 3,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;

            if (result.DepartureRoutes.Count <= 0)
                Assert.Fail();

            foreach (var route in result.DepartureRoutes)
                Assert.AreEqual(11.25, route.Duration());
        }

        /*
            Test:               Flight 1 departs from StopOverId and arrives at ArrivingAtId
                                Flight 2 departs from DepartingFromId and arrives at StopOverId [Destination]
            Expected result:    1 FlightRoute
            Tests Satisfied:    Test 7.
        */
        [Test]
        public void TwoLeggedFlightTest7()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2016, 1, 1, 5,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 4,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /*
            Test:               Flight 1 departs from StopOverId and arrives at ArrivingAtId 
                                Flight 2 departs from StopOverId and arrives at ArrivingAtId [Destination]
            Expected result:    1 FlightRoute
            Tests Satisfied:    Test 8.
        */
        [Test]
        public void TwoLeggedFlightTest8()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2016, 1, 1, 5,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 3,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /*
            Test:               Flight 1 departs from StopOverId and arrives at ArrivingAtId
                                Flight 2 departs from DepartingFromId and arrives at ArrivingAtId [Destination]
            Expected result:    1 FlightRoute
            Tests Satisfied:    Test 9.
        */
        [Test]
        public void TwoLeggedFlightTest9()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2016, 1, 1, 5,
                0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 5,
                deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }
    }
}
