﻿/* 
    Description:    For a given FlightSearchViewModel, and target Airports which are three legs away from departure Airports, test
                    whether the algorithm finds the FlightRoute.
    Tests:      1.  Flight 1 departs from DepartingFromId and arrives at StopOverId
                    Flight 2 departs from DepartingFromId and arrives at StopOverId
                    Flight 3 departs from DepartingFromId and arrives at StopOverId [Destination]
                2.  Flight 1 departs from DepartingFromId and arrives at StopOverId
                    Flight 2 departs from DepartingFromId and arrives at StopOverId
                    Flight 3 departs from DepartingFromId and arrives at ArrivingAtId [Destination]
                3.  Flight 1 departs from DepartingFromId and arrives at StopOverId
                    Flight 2 departs from DepartingFromId and arrives at StopOverId
                    Flight 3 departs from StopOverId and arrives at ArrivingAtId [Destination]
                4.  Flight 1 departs from DepartingFromId and arrives at StopOverId
                    Flight 2 departs from StopOverId and arrives at ArrivingAtId
                    Flight 3 departs from DepartingFromId and arrives at StopOverId [Destination]
                5.  Flight 1 departs from DepartingFromId and arrives at StopOverId
                    Flight 2 departs from StopOverId and arrives at ArrivingAtId
                    Flight 3 departs from DepartingFromId and arrives at ArrivingAtId [Destination]
                6.  Flight 1 departs from DepartingFromId and arrives at StopOverId
                    Flight 2 departs from StopOverId and arrives at ArrivingAtId
                    Flight 3 departs from StopOverId and arrives at ArrivingAtId [Destination]
                7.  Flight 1 departs from DepartingFromId and arrives at ArrivingAtId
                    Flight 2 departs from DepartingFromId and arrives at StopOverId
                    Flight 3 departs from DepartingFromId and arrives at StopOverId [Destination]
                8.  Flight 1 departs from DepartingFromId and arrives at ArrivingAtId
                    Flight 2 departs from DepartingFromId and arrives at StopOverId
                    Flight 3 departs from DepartingFromId and arrives at ArrivingAtId [Destination]
                9.  Flight 1 departs from DepartingFromId and arrives at ArrivingAtId
                    Flight 2 departs from DepartingFromId and arrives at StopOverId
                    Flight 3 departs from StopOverId and arrives at ArrivingAtId [Destination]
                10. Flight 1 departs from DepartingFromId and arrives at ArrivingAtId
                    Flight 2 departs from StopOverId and arrives at ArrivingAtId
                    Flight 3 departs from DepartingFromId and arrives at StopOverId [Destination]
                11. Flight 1 departs from DepartingFromId and arrives at ArrivingAtId
                    Flight 2 departs from StopOverId and arrives at ArrivingAtId
                    Flight 3 departs from DepartingFromId and arrives at ArrivingAtId [Destination]
                12. Flight 1 departs from DepartingFromId and arrives at ArrivingAtId
                    Flight 2 departs from StopOverId and arrives at ArrivingAtId
                    Flight 3 departs from StopOverId and arrives at ArrivingAtId [Destination]
                13. Flight 1 departs from StopOverId and arrives at ArrivingAtId
                    Flight 2 departs from DepartingFromId and arrives at StopOverId
                    Flight 3 departs from DepartingFromId and arrives at StopOverId [Destination]
                14. Flight 1 departs from StopOverId and arrives at ArrivingAtId
                    Flight 2 departs from DepartingFromId and arrives at StopOverId
                    Flight 3 departs from StopOverId and arrives at ArrivingAtId [Destination]
                16. Flight 1 departs from StopOverId and arrives at ArrivingAtId
                    Flight 2 departs from StopOverId and arrives at ArrivingAtId
                    Flight 3 departs from DepartingFromId and arrives at StopOverId [Destination]
                17. Flight 1 departs from StopOverId and arrives at ArrivingAtId
                    Flight 2 departs from StopOverId and arrives at ArrivingAtId
                    Flight 3 departs from DepartingFromId and arrives at ArrivingAtId [Destination]
                18. Flight 1 departs from StopOverId and arrives at ArrivingAtId
                    Flight 2 departs from StopOverId and arrives at ArrivingAtId
                    Flight 3 departs from StopOverId and arrives at ArrivingAtId [Destination]
                19. Flight 1 departs from DepartingFromId and arrives at ArrivingAtId
                    Flight 2 departs from DepartingFromId and arrives at ArrivingAtId
                    Flight 3 departs from DepartingFromId and arrives at StopOverId [Destination]
                20. Flight 1 departs from DepartingFromId and arrives at ArrivingAtId
                    Flight 2 departs from DepartingFromId and arrives at ArrivingAtId
                    Flight 3 departs from DepartingFromId and arrives at ArrivingAtId [Destination]

                a.  Test correct cost is calculated for the above flights.
                b.  Test correct flight duration is calculated for the above flights.
*/

using System;
using System.Linq;
using NUnit.Framework;
using ZotCode.Domain.Model.System;
using ZotCode.UnitTests.DataManager;
using ZotCode.Web.Areas.Flight.ViewModels;
using ZotCode.Web.Flight.Models;
using ZotCode.UnitTests.ViewModels;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using FlightSearchViewModel = ZotCode.UnitTests.ViewModels.FlightSearchViewModel;

namespace ZotCode.UnitTests.AlgorithmTests
{
    [TestFixture]
    public class ThreeLeggedFlightTests
    {

        private FakeFlightPubContext _dbContext;

        /* 
            Setup the databsae context.
        */
        [SetUp]
        public void Init()
        {
            _dbContext = new FakeFlightPubContext();
            _dbContext.AddSet(TestData.Flights);
            _dbContext.AddSet(TestData.AirPorts);
            _dbContext.AddSet(TestData.Countries);
            _dbContext.AddSet(TestData.Planes);
            _dbContext.AddSet(TestData.Airlines);
            _dbContext.AddSet(TestData.Prices);
            _dbContext.AddSet(TestData.Availabilities);
        }

        /* 
            Test:               Flight 1 departs from DepartingFromId and arrives at StopOverId
                                Flight 2 departs from DepartingFromId and arrives at StopOverId
                                Flight 3 departs from DepartingFromId and arrives at StopOverId [Destination]
            Expected result:    1 flight exists.
            Tests Satisfied:    Test 1.
        */
        [Test]
        public void ThreeLeggedFlights1()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2016, 7, 2, 5, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 5,
                deptDateTime, true, SeatClass.FirstClass);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /* 
            Test:               Flight 1 departs from DepartingFromId and arrives at ArrivingAtId
                                Flight 2 departs from DepartingFromId and arrives at ArrivingAtId
                                Flight 3 departs from DepartingFromId and arrives at StopOverId [Destination]
            Expected result:    1 flight exists.
            Tests Satisfied:    Test 19.
        */
        [Test]
        public void ThreeLeggedFlights19()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2016, 7, 2, 5, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 5,
                deptDateTime, true, SeatClass.FirstClass);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /* 
            Test:               Check whether the cost is correctly calculated for Test 19.
            Expected result:    4040.00
            Tests Satisfied:    Test 19A.
        */
        [Test]
        public void ThreeLeggedFlights19A()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2016, 7, 2, 5, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 5,
                deptDateTime, true, SeatClass.FirstClass);
            var search = new Search(model, _dbContext);
            var result = search.Result;

            if (result.DepartureRoutes.Count <= 0)
                Assert.Fail();

            foreach (var route in result.DepartureRoutes)
                Assert.AreEqual(4040.00, route.GetCostCheapest());
        }

        /* 
            Test:               Test correct flight duration is calculated for Test 19.
            Expected result:    16.67
            Tests Satisfied:    Test 19B.
        */
        [Test]
        public void ThreeLeggedFlights19B()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2016, 7, 2, 5, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 5,
                deptDateTime, true, SeatClass.FirstClass);
            var search = new Search(model, _dbContext);
            var result = search.Result;

            if (result.DepartureRoutes.Count <= 0)
                Assert.Fail();
            
            foreach (var route in result.DepartureRoutes)
                Assert.AreEqual(16.67, route.Duration());
        }

        /* 
            Test:               Flight 1 departs from DepartingFromId and arrives at ArrivingAtId
                                Flight 2 departs from DepartingFromId and arrives at ArrivingAtId
                                Flight 3 departs from DepartingFromId and arrives at ArrivingAtId [Destination]
            Expected result:    1 flight exists.
            Tests Satisfied:    Test 20.
        */
        [Test]
        public void ThreeLeggedFlights20()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2016, 7, 2, 5, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 6,
                deptDateTime, true, SeatClass.FirstClass);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /* 
            Test:               Check whether the cost is correctly calculated for Test 20.
            Expected result:    5140.00
            Tests Satisfied:    Test 20A.
        */
        [Test]
        public void ThreeLeggedFlights20A()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2016, 7, 2, 5, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 6,
                deptDateTime, true, SeatClass.FirstClass);
            var search = new Search(model, _dbContext);
            var result = search.Result;

            if (result.DepartureRoutes.Count <= 0)
                Assert.Fail();

            foreach (var route in result.DepartureRoutes)
                Assert.AreEqual(5140.00, route.GetCostCheapest());
        }

        /* 
            Test:               Test correct flight duration is calculated for Test 20.
            Expected result:    23.17
            Tests Satisfied:    Test 20B.
        */
        [Test]
        public void ThreeLeggedFlights20B()
        {
            /* Test Setup */
            var deptDateTime = new DateTime(2016, 7, 2, 5, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 6,
                deptDateTime, true, SeatClass.FirstClass);
            var search = new Search(model, _dbContext);
            var result = search.Result;

            if (result.DepartureRoutes.Count <= 0)
                Assert.Fail();

            foreach (var route in result.DepartureRoutes)
                Assert.AreEqual(23.17, route.Duration());
        }
    }
}
