﻿/* 
    Description:    Test that flights are correctly identified if there are enough seats available given the seat requirements from
                    FlightSearchViewModel
    Tests:      1.  For a given flight which has no available seats, will the flight be ignored should 1 Child and 0 Adults be 
                    entered in the FlightSearchViewModel.
                2.  For a given flight which has no available seats, will the flight be ignored should 1 Adult and 0 Children be 
                    entered in the FlightSearchViewModel.
                3.  For a given flight which has no available seats, will the flight be ignored should 1 Adult and 1 Child be 
                    entered in the FlightSearchViewModel.
                4.  For a given flight which has exactly 1 seat, will the flight be returned given 1 Adult and 0 Children.
                5.  For a given flight which has exactly 1 seat, will the flight be returned given 0 Adult and 1 Children.
                6.  For a given flight which has exactly 1 seat, will the flight be returned given 1 Adult and 1 Children.
                7A. Pre-check to ensure two-legged flight that'll be searched for exists.
                7.  For a given two-legged flight where the first leg does not have sufficient seats.
                8.  For a given two-legged flight where the second leg does not have sufficient seats.
                9A. Pre-check to ensure three-legged flight that'll be searched for exists.
                9.  For a given three-legged flight where the first leg does not have sufficient seats.
                10. For a given three-legged flight where the second leg does not have sufficient seats.
                11. For a given three-legged flight where the third leg does not have sufficient seats.
                12A. For a given flight which only has Economy tickets, check whether the flight will be found with a Economy search.
                12B. For a given flight which only has Economy tickets, check whether the flight will be found with a Business search.
                12C. For a given flight which only has Economy tickets, check whether the flight will be found with a First Class search.
                12D. For a given flight which only has Economy tickets, check whether the flight will be found with a Premium Economy search.
                13A. For a given flight which only has Business tickets, check whether the flight will be found with a Economy search.
                13B. For a given flight which only has Business tickets, check whether the flight will be found with a Business search.
                13C. For a given flight which only has Business tickets, check whether the flight will be found with a First Class search.
                13D. For a given flight which only has Business tickets, check whether the flight will be found with a Premium Economy search.
                14A. For a given flight which only has First Class tickets, check whether the flight will be found with a Economy search.
                14B. For a given flight which only has First Class tickets, check whether the flight will be found with a Business search.
                14C. For a given flight which only has First Class tickets, check whether the flight will be found with a First Class search.
                14D. For a given flight which only has First Class tickets, check whether the flight will be found with a Premium Economy search.
                15A. For a given flight which only has Premium Economy tickets, check whether the flight will be found with a Economy search.
                15B. For a given flight which only has Premium Economy tickets, check whether the flight will be found with a Business search.
                15C. For a given flight which only has Premium Economy tickets, check whether the flight will be found with a First Class search.
                15D. For a given flight which only has Premium Economy tickets, check whether the flight will be found with a Premium Economy search.

*/

using System;
using System.Linq;
using NUnit.Framework;
using ZotCode.Domain.Model;
using ZotCode.Domain.Model.System;
using ZotCode.UnitTests.DataManager;
using ZotCode.Web.Flight.Models;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using FlightSearchViewModel = ZotCode.UnitTests.ViewModels.FlightSearchViewModel;

namespace ZotCode.UnitTests.AlgorithmTests
{
    [TestFixture]
    public class SeatTests
    {

        private FakeFlightPubContext _dbContext;

        /* 
            Setup the databsae context.
        */
        [SetUp]
        public void Init()
        {
            _dbContext = new FakeFlightPubContext();
            _dbContext.AddSet(TestData.Flights);
            _dbContext.AddSet(TestData.AirPorts);
            _dbContext.AddSet(TestData.Countries);
            _dbContext.AddSet(TestData.Planes);
            _dbContext.AddSet(TestData.Airlines);
            _dbContext.AddSet(TestData.Prices);
            _dbContext.AddSet(TestData.Availabilities);
        }

        /* 
            Test:               For a given flight which has no available seats, will the flight be ignored should 1 Child and 0 Adults be 
                                entered in the FlightSearchViewModel.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 1.
        */
        [Test]
        public void SeatsTest1()
        {
            var deptDateTime = new DateTime(2016, 4, 2, 6, 0, 0);
            var model = new FlightSearchViewModel(0, 1, 0, 3, deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            // Check that the flight exists.
            var num = _dbContext.Query<Flight>().Count(x => x.Id == 15);
            if(num <= 0)
                Assert.Fail();

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For a given flight which has no available seats, will the flight be ignored should 1 Adult and 0 Children be 
                                entered in the FlightSearchViewModel.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 2.
        */
        [Test]
        public void SeatsTest2()
        {
            var deptDateTime = new DateTime(2016, 4, 2, 6, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 3, deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            // Check that the flight exists.
            var num = _dbContext.Query<Flight>().Count(x => x.Id == 15);
            if (num <= 0)
                Assert.Fail();

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For a given flight which has no available seats, will the flight be ignored should 1 Adult and 1 Child be 
                                entered in the FlightSearchViewModel.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 3.
        */
        [Test]
        public void SeatsTest3()
        {
            var deptDateTime = new DateTime(2016, 4, 2, 6, 0, 0);
            var model = new FlightSearchViewModel(1, 1, 0, 3, deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            // Check that the flight exists.
            var num = _dbContext.Query<Flight>().Count(x => x.Id == 15);
            if (num <= 0)
                Assert.Fail();

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For a given flight which has exactly 1 seat, will the flight be returned given 1 Adult and 0 Children.
            Expected result:    1 FlightRoutes returned.
            Tests Satisfied:    Test 4.
        */
        [Test]
        public void SeatsTest4()
        {
            var deptDateTime = new DateTime(2015, 10, 26, 9, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 1, deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /* 
            Test:               For a given flight which has exactly 1 seat, will the flight be returned given 0 Adult and 1 Children.
            Expected result:    1 FlightRoutes returned.
            Tests Satisfied:    Test 5.
        */
        [Test]
        public void SeatsTest5()
        {
            var deptDateTime = new DateTime(2015, 10, 26, 9, 0, 0);
            var model = new FlightSearchViewModel(0, 1, 0, 1, deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /* 
            Test:               For a given flight which has exactly 1 seat, will the flight be returned given 1 Adult and 1 Children.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 6.
        */
        [Test]
        public void SeatsTest6()
        {
            var deptDateTime = new DateTime(2015, 10, 26, 9, 0, 0);
            var model = new FlightSearchViewModel(1, 1, 0, 1, deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            // Check that the flight exists.
            var num = _dbContext.Query<Flight>().Count(x => x.Id == 0);
            if (num <= 0)
                Assert.Fail();

            Assert.AreEqual(0, count);
        }

        /* 
            Init for SeatsTest7 and SeatsTest8
        */
        public FlightSearchViewModel TwoLeggedSetup(int a, int b)
        {
            // Setup
            var deptDateTime = new DateTime(2016, 5, 2, 8, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 5, deptDateTime, true, SeatClass.Economy);

            var query1 = _dbContext.Query<Availability>().FirstOrDefault(x => x.Id == 16);
            if (query1 != null) query1.AvailableLeg1 = a;
            var query2 = _dbContext.Query<Availability>().FirstOrDefault(x => x.Id == 17);
            if (query2 != null) query2.AvailableLeg1 = b;

            return model;
        }

        /* 
            Test:               Check to ensure FlightRoute is found should there be sufficient seats 
            Expected result:    1 FlightRoutes returned.
        */
        [Test]
        public void SeatTest7A()
        {
            // Setup
            var model = TwoLeggedSetup(1, 1);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /* 
            Test:               For a given two-legged flight where the first leg does not have sufficient seats.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 7.
        */
        [Test]
        public void SeatsTest7()
        {
            // Setup
            var model = TwoLeggedSetup(0, 1);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For a given two-legged flight where the second leg does not have sufficient seats.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 8.
        */
        [Test]
        public void SeatsTest8()
        {
            // Setup
            var model = TwoLeggedSetup(1, 0);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }

        /* 
            Init for SeatsTest9
        */
        public FlightSearchViewModel ThreeLeggedSetup(int a, int b, int c)
        {
            // Setup
            var deptDateTime = new DateTime(2016, 6, 2, 8, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 3, deptDateTime, true, SeatClass.Business);

            var query1 = _dbContext.Query<Availability>().FirstOrDefault(x => x.Id == 18);
            if (query1 != null) query1.AvailableLeg1 = a;
            var query2 = _dbContext.Query<Availability>().FirstOrDefault(x => x.Id == 19);
            if (query2 != null) query2.AvailableLeg1 = b;
            var query3 = _dbContext.Query<Availability>().FirstOrDefault(x => x.Id == 20);
            if (query3 != null) query3.AvailableLeg1 = c;

            return model;
        }

        /* 
            Test:               Check to ensure FlightRoute is found should there be sufficient seats 
            Expected result:    1 FlightRoutes returned.
        */
        [Test]
        public void SeatTest9PreTest()
        {
            // Setup
            var model = ThreeLeggedSetup(1, 1, 1);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /* 
            Test:               For a given three-legged flight where the first leg does not have sufficient seats.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 9.
        */
        [Test]
        public void SeatsTest9()
        {
            // Setup
            var model = ThreeLeggedSetup(0,1,1);

            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For a given three-legged flight where the second leg does not have sufficient seats.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 10.
        */
        [Test]
        public void SeatsTest10()
        {
            // Setup
            var model = ThreeLeggedSetup(1, 0, 1);

            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For a given three-legged flight where the third leg does not have sufficient seats.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 11.
        */
        [Test]
        public void SeatsTest11()
        {
            // Setup
            var model = ThreeLeggedSetup(1, 1, 0);

            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For a given flight which only has Economy tickets, check whether the flight will be found with a Economy search.
            Expected result:    1 FlightRoutes returned.
            Tests Satisfied:    Test 12A.
        */
        [Test]
        public void SeatsTest12A()
        {
            // Setup
            var deptDateTime = new DateTime(2015, 10, 26, 9, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 1, deptDateTime, true, SeatClass.Economy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /* 
            Test:               For a given flight which only has Economy tickets, check whether the flight will be found with a Business search.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 12B.
        */
        [Test]
        public void SeatsTest12B()
        {
            // Setup
            var deptDateTime = new DateTime(2015, 10, 26, 9, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 1, deptDateTime, true, SeatClass.Business);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For a given flight which only has Economy tickets, check whether the flight will be found with a First Class search.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 12C.
        */
        [Test]
        public void SeatsTest12C()
        {
            // Setup
            var deptDateTime = new DateTime(2015, 10, 26, 9, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 1, deptDateTime, true, SeatClass.FirstClass);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For a given flight which only has Economy tickets, check whether the flight will be found with a Premium Economy search.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 12C.
        */
        [Test]
        public void SeatsTest12D()
        {
            // Setup
            var deptDateTime = new DateTime(2015, 10, 26, 9, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 1, deptDateTime, true, SeatClass.PremiumEconomy);
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For a given flight which only has Business tickets, check whether the flight will be found with a Economy search.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 13A.
        */
        [Test]
        public void SeatsTest13A()
        {
            // Setup
            var deptDateTime = new DateTime(2016, 6, 2, 14, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 2, 3, deptDateTime, true, SeatClass.Economy);
            var query1 = _dbContext.Query<Availability>().FirstOrDefault(x => x.Id == 20);
            if (query1 != null) query1.AvailableLeg1 = 1;
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For a given flight which only has Business tickets, check whether the flight will be found with a Business search.
            Expected result:    1 FlightRoutes returned.
            Tests Satisfied:    Test 13B.
        */
        [Test]
        public void SeatsTest13B()
        {
            // Setup
            var deptDateTime = new DateTime(2016, 6, 2, 14, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 2, 3, deptDateTime, true, SeatClass.Business);
            var query1 = _dbContext.Query<Availability>().FirstOrDefault(x => x.Id == 20);
            if (query1 != null) query1.AvailableLeg1 = 1;
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /* 
            Test:               For a given flight which only has Business tickets, check whether the flight will be found with a First Class search.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 13C.
        */
        [Test]
        public void SeatsTest13C()
        {
            // Setup
            var deptDateTime = new DateTime(2016, 6, 2, 14, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 2, 3, deptDateTime, true, SeatClass.FirstClass);
            var query1 = _dbContext.Query<Availability>().FirstOrDefault(x => x.Id == 20);
            if (query1 != null) query1.AvailableLeg1 = 1;
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For a given flight which only has Business tickets, check whether the flight will be found with a Premium Economy search.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 13D.
        */
        [Test]
        public void SeatsTest13D()
        {
            // Setup
            var deptDateTime = new DateTime(2016, 6, 2, 14, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 2, 3, deptDateTime, true, SeatClass.PremiumEconomy);
            var query1 = _dbContext.Query<Availability>().FirstOrDefault(x => x.Id == 20);
            if (query1 != null) query1.AvailableLeg1 = 1;
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For a given flight which only has First Class tickets, check whether the flight will be found with a Economy search.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 14A.
        */
        [Test]
        public void SeatsTest14A()
        {
            // Setup
            var deptDateTime = new DateTime(2016, 6, 2, 14, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 2, 3, deptDateTime, true, SeatClass.Economy);
            var query1 = _dbContext.Query<Availability>().FirstOrDefault(x => x.Id == 22);
            if (query1 != null) query1.AvailableLeg1 = 1;
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For a given flight which only has First Class tickets, check whether the flight will be found with a Business search.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 14B.
        */
        [Test]
        public void SeatsTest14B()
        {
            // Setup
            var deptDateTime = new DateTime(2016, 6, 2, 14, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 2, 3, deptDateTime, true, SeatClass.Business);
            var query1 = _dbContext.Query<Availability>().FirstOrDefault(x => x.Id == 22);
            if (query1 != null) query1.AvailableLeg1 = 1;
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For a given flight which only has First Class tickets, check whether the flight will be found with a First Class search.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 14C.
        */
        [Test]
        public void SeatsTest14C()
        {
            // Setup
            var deptDateTime = new DateTime(2016, 6, 2, 14, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 2, 3, deptDateTime, true, SeatClass.FirstClass);
            var query1 = _dbContext.Query<Availability>().FirstOrDefault(x => x.Id == 22);
            if (query1 != null) query1.AvailableLeg1 = 1;
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }

        /* 
            Test:               For a given flight which only has First Class tickets, check whether the flight will be found with a Premium Economy search.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 14D.
        */
        [Test]
        public void SeatsTest14D()
        {
            // Setup
            var deptDateTime = new DateTime(2016, 6, 2, 14, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 2, 3, deptDateTime, true, SeatClass.PremiumEconomy);
            var query1 = _dbContext.Query<Availability>().FirstOrDefault(x => x.Id == 22);
            if (query1 != null) query1.AvailableLeg1 = 1;
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For a given flight which only has Premium Economy tickets, check whether the flight will be found with a Economy search.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 15A.
        */
        [Test]
        public void SeatsTest15A()
        {
            // Setup
            var deptDateTime = new DateTime(2016, 6, 2, 14, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 2, 3, deptDateTime, true, SeatClass.Economy);
            var query1 = _dbContext.Query<Availability>().FirstOrDefault(x => x.Id == 23);
            if (query1 != null) query1.AvailableLeg1 = 1;
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For a given flight which only has Premium Economy tickets, check whether the flight will be found with a Business search.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 15B.
        */
        [Test]
        public void SeatsTest15B()
        {
            // Setup
            var deptDateTime = new DateTime(2016, 6, 2, 14, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 2, 3, deptDateTime, true, SeatClass.Business);
            var query1 = _dbContext.Query<Availability>().FirstOrDefault(x => x.Id == 23);
            if (query1 != null) query1.AvailableLeg1 = 1;
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For a given flight which only has Premium Economy tickets, check whether the flight will be found with a First Class search.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 15C.
        */
        [Test]
        public void SeatsTest15C()
        {
            // Setup
            var deptDateTime = new DateTime(2016, 6, 2, 14, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 2, 3, deptDateTime, true, SeatClass.FirstClass);
            var query1 = _dbContext.Query<Availability>().FirstOrDefault(x => x.Id == 23);
            if (query1 != null) query1.AvailableLeg1 = 1;
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }

        /* 
            Test:               For a given flight which only has Premium Economy tickets, check whether the flight will be found with a Premium Economy search.
            Expected result:    0 FlightRoutes returned.
            Tests Satisfied:    Test 15D.
        */
        [Test]
        public void SeatsTest15D()
        {
            // Setup
            var deptDateTime = new DateTime(2016, 6, 2, 14, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 2, 3, deptDateTime, true, SeatClass.PremiumEconomy);
            var query1 = _dbContext.Query<Availability>().FirstOrDefault(x => x.Id == 23);
            if (query1 != null) query1.AvailableLeg1 = 1;
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(1, count);
        }
    }
}
