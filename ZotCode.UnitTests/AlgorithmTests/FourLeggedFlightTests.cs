﻿using System;
using System.Linq;
using NUnit.Framework;
using ZotCode.Domain.Model.System;
using ZotCode.UnitTests.DataManager;
using ZotCode.Web.Areas.Flight.ViewModels;
using ZotCode.Web.Flight.Models;
using ZotCode.UnitTests.ViewModels;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using FlightSearchViewModel = ZotCode.UnitTests.ViewModels.FlightSearchViewModel;

namespace ZotCode.UnitTests.AlgorithmTests
{
    [TestFixture]
    public class FourLeggedFlightTests
    {

        private FakeFlightPubContext _dbContext;

        /* 
            Setup the databsae context.
        */
        [SetUp]
        public void Init()
        {
            _dbContext = new FakeFlightPubContext();
            _dbContext.AddSet(TestData.Flights);
            _dbContext.AddSet(TestData.AirPorts);
            _dbContext.AddSet(TestData.Countries);
            _dbContext.AddSet(TestData.Planes);
            _dbContext.AddSet(TestData.Airlines);
            _dbContext.AddSet(TestData.Prices);
            _dbContext.AddSet(TestData.Availabilities);

            var query1 = _dbContext.Query<Availability>().FirstOrDefault(x => x.Id == 18);
            if (query1 != null) query1.AvailableLeg1 = 1;
            var query2 = _dbContext.Query<Availability>().FirstOrDefault(x => x.Id == 19);
            if (query2 != null) query2.AvailableLeg1 = 1;
            var query3 = _dbContext.Query<Availability>().FirstOrDefault(x => x.Id == 20);
            if (query3 != null) query3.AvailableLeg1 = 1;
            var query4 = _dbContext.Query<Availability>().FirstOrDefault(x => x.Id == 24);
            if (query4 != null) query4.AvailableLeg1 = 1;
        }

        /* 
            Description:        Pre-test to ensure that all four Flights are found in combinations: First the first three flights returned as
                                a three legged flight, and then the last three - as a three legged flight.
            Expected Result:    2 FlightRoutes found.
        */
        [Test]
        public void FourLeggedFlightPreTest1()
        {
            // First three legs test.
            var deptDateTime = new DateTime(2016, 6, 2, 8, 0, 0);
            var modelFirst = new FlightSearchViewModel(1, 0, 0, 3, deptDateTime, true, SeatClass.Business);
            var searchFirst = new Search(modelFirst, _dbContext);
            var resultFirst = searchFirst.Result;
            var countFirst = resultFirst.DepartureRoutes.Count;

            Assert.AreEqual(1, countFirst);
        }

        /* 
            Description:        Pre-test to ensure that all four Flights are found in combinations: First the first three flights returned as
                                a three legged flight, and then the last three - as a three legged flight.
            Expected Result:    2 FlightRoutes found.
        */
        [Test]
        public void FourLeggedFlightPreTest2()
        {
            // Second three legs test.
            var deptDateTime = new DateTime(2016, 6, 2, 8, 0, 0);
            var modelSecond = new FlightSearchViewModel(1, 0, 1, 4, deptDateTime, true, SeatClass.Business);
            var searchSecond = new Search(modelSecond, _dbContext);
            var resultSecond = searchSecond.Result;
            var countSecond = resultSecond.DepartureRoutes.Count;

            Assert.AreEqual(1, countSecond);
        }

        /* 
            Description:        The flight search algorithm is limited to a depth of 3. Which means four legged flights should be excluded
                                from the search results. Therefore given a four legged flight which exists. The algorithm should not find it.
            Expected Result:    0 FlightRoutes found.
        */
        [Test]
        public void FourLeggedFlight()
        {
            // Setup View Model
            var deptDateTime = new DateTime(2016, 6, 2, 8, 0, 0);
            var model = new FlightSearchViewModel(1, 0, 0, 4, deptDateTime, true, SeatClass.Business);

            // Test
            var search = new Search(model, _dbContext);
            var result = search.Result;
            var count = result.DepartureRoutes.Count;

            Assert.AreEqual(0, count);
        }
    }
}
