﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using ZotCode.Domain.Model;
using ZotCode.Domain.Model.System;
using ZotCode.UnitTests.DataManager;
using ZotCode.Web.Areas.Admin.ViewModels;
using ZotCode.Web.Areas.Flight.ViewModels;
using ZotCode.Web.Flight.Models;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace ZotCode.UnitTests.ControllerTests
{
    [TestFixture]
    public class UserControllerTest
    {
        private FakeFlightPubContext _dbContext;

        [SetUp]
        public void Init()
        {
            _dbContext = new FakeFlightPubContext();
            _dbContext.AddSet(ControllerTestData.Users);
        }

        /*
         * Add new user tests:
         * 
         * Description: Tests whether or not the controller allows for adding invalid user details
         *
         *      1:  Test if allowed to add user without last name
         *      2:  Test if allowed to add user without first name
         *      3:  Test if allowed to add user without date of birth
         *      4:  Test if allowed to add user with DOB from future
         */

        [Test]
        public void TestAddUser1()
        {
            var user = _dbContext.Query<User>().ToList()[0];
            Assert.AreEqual(user.FirstName, "Monkey");
            //CreateUserViewModel = new CreateUserViewModel() {};
        }

        /*
         * Update user details tests:
         *
         *      1:  Test if allowed to update name to empty name
         *      2:  Test if allowed to update address to
         */
    }
}
