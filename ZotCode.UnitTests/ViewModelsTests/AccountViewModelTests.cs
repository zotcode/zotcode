﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using NUnit.Framework;
using ZotCode.Web.ViewModels;
using ZotCode.Web.Areas.Admin.ViewModels;

namespace ZotCode.UnitTests.ViewModelsTests
{
    class AccountViewModelTests
    {
        private ExternalLoginConfirmationViewModel _externalLoginCVM;
        private ExternalLoginListViewModel _externalLoginVM;
        private SendCodeViewModel _sendCodeVM;
        private VerifyCodeViewModel _verifyCodeVM;
        private ForgotViewModel _forgotVM;
        private LoginViewModel _loginVM;
        private RegisterViewModel _registerVM;
        private ResetPasswordViewModel _resetPasswordVM;
        private ForgotPasswordViewModel _forgotPasswordVM;

        [SetUp]
        public void Init()
        {
            _externalLoginCVM = new ExternalLoginConfirmationViewModel()
            {
                Email = "test@test.com"
            };

            _externalLoginVM = new ExternalLoginListViewModel()
            {
                ReturnUrl = "testURL"
            };

            _sendCodeVM = new SendCodeViewModel()
            {
                ReturnUrl = "testURL",
                Providers = new List<SelectListItem>(),
                RememberMe = true,
                SelectedProvider = "SP"
            };

            _verifyCodeVM = new VerifyCodeViewModel()
            {
                ReturnUrl = "testURL",
                RememberMe = true,
                Code = "testCode",
                Provider = "P",
                RememberBrowser = true
            };

            _forgotVM = new ForgotViewModel()
            {
                Email = "test@test.com"
            };

            _loginVM = new LoginViewModel()
            {
                RememberMe = true,
                Password = "password",
                Username = "username"
            };

            _registerVM = new RegisterViewModel()
            {
                Email = "test@test.com",
                Password = "password",
                FirstName = "Monkey",
                DateOfBirth = DateTime.Today,
                LastName = "Brains",
                ConfirmPassword = "password"
            };

            _resetPasswordVM = new ResetPasswordViewModel()
            {
                Email = "test@test.com",
                Password = "password",
                ConfirmPassword = "password",
                Code = "testCode"
            };

            _forgotPasswordVM = new ForgotPasswordViewModel()
            {
                 Email = "test@test.com"
            };
        }

        // ExternalLoginConfirmationViewModel tests
        [Test]
        public void TestExternalLoginConfirmationViewModel()
        {
            var results = TestHelper.Validate(_externalLoginCVM);
            Assert.AreEqual(0, results.Count);
        }

        [Test]
        public void TestExternalLoginConfirmationViewModelWithNoEmail()
        {
            _externalLoginCVM.Email = null;
            var results = TestHelper.Validate(_externalLoginCVM);
            Assert.AreEqual(1, results.Count);
            Assert.AreEqual("The Username field is required.", results[0].ErrorMessage);
        }

        // ExternalLoginListViewModel
        [Test]
        public void TestExternalLoginListViewModel()
        {
            var results = TestHelper.Validate(_externalLoginVM);
            Assert.AreEqual(0, results.Count);
        }

        [Test]
        public void TestExternalLoginListViewModelWithNoUsername()
        {
            _externalLoginVM.ReturnUrl = null;
            var results = TestHelper.Validate(_externalLoginVM);
            Assert.AreEqual(1, results.Count);
            Assert.AreEqual("The URL field is required.", results[0].ErrorMessage);
        }

        // SendCodeViewModel
        [Test]
        public void TestSendCodeViewModel()
        {
            var results = TestHelper.Validate(_sendCodeVM);
            Assert.AreEqual(0, results.Count);
        }

        [Test]
        public void TestSendCodeViewModelNoUrl()
        {
            _sendCodeVM.ReturnUrl = null;
            var results = TestHelper.Validate(_sendCodeVM);
            Assert.AreEqual(1, results.Count);
        }

        // VerifyCodeViewModel
        [Test]
        public void TestVerifyCodeViewModel()
        {
            var results = TestHelper.Validate(_verifyCodeVM);
            Assert.AreEqual(0, results.Count);
        }

        [Test]
        public void TestVerifyCodeViewModelNoUrl()
        {
            _verifyCodeVM.ReturnUrl = null;
            var results = TestHelper.Validate(_verifyCodeVM);
            Assert.AreEqual(1, results.Count);
        }

        // ForgotViewModel
        [Test]
        public void TestForgotViewModel()
        {
            var results = TestHelper.Validate(_forgotVM);
            Assert.AreEqual(0, results.Count);
        }

        [Test]
        public void TestForgotViewModelNoUsername()
        {
            _forgotVM.Email = null;
            var results = TestHelper.Validate(_forgotVM);
            Assert.AreEqual(1, results.Count);
        }

        // LoginViewModel
        [Test]
        public void TestLoginViewModel()
        {
            var results = TestHelper.Validate(_loginVM);
            Assert.AreEqual(0, results.Count);
        }

        [Test]
        public void TestLoginViewModelNoUsername()
        {
            _loginVM.Username = null;
            var results = TestHelper.Validate(_loginVM);
            Assert.AreEqual(1, results.Count);
            Assert.AreEqual("The Username field is required.", results[0].ErrorMessage);
        }

        // RegisterViewModel
        [Test]
        public void TestRegisterViewModel()
        {
            var results = TestHelper.Validate(_registerVM);
            Assert.AreEqual(0, results.Count);
        }

        [Test]
        public void TestRegisterViewModelShortPw()
        {
            _registerVM.Password = "1";
            _registerVM.ConfirmPassword = "1";
            var results = TestHelper.Validate(_registerVM);
            Assert.AreEqual(1, results.Count);
            Assert.AreEqual("The Password must be at least 6 characters long.", results[0].ErrorMessage);
        }

        [Test]
        public void TestRegisterViewModelNoMatchPw()
        {
            _registerVM.ConfirmPassword = "abc";
            var result = TestHelper.Validate(_registerVM);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The password and confirmation password do not match.", result[0].ErrorMessage);
        }

        // ResetPasswordViewModel
        [Test]
        public void TestResetPasswordViewModel()
        {
            var results = TestHelper.Validate(_resetPasswordVM);
            Assert.AreEqual(0, results.Count);
        }

        [Test]
        public void TesResetPasswordViewModelShortPw()
        {
            _resetPasswordVM.Password = "1";
            _resetPasswordVM.ConfirmPassword = "1";
            var results = TestHelper.Validate(_resetPasswordVM);
            Assert.AreEqual(1, results.Count);
            Assert.AreEqual("The Password must be at least 6 characters long.", results[0].ErrorMessage);
        }

        [Test]
        public void TestResetPasswordViewModelNoMatchPw()
        {
            _resetPasswordVM.ConfirmPassword = "abc";
            var result = TestHelper.Validate(_resetPasswordVM);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The password and confirmation password do not match.", result[0].ErrorMessage);
        }

        // ForgotPasswordViewModel -- possible duplicate of ForgotViewModel?
        [Test]
        public void TestForgotPasswordViewModel()
        {
            var results = TestHelper.Validate(_forgotPasswordVM);
            Assert.AreEqual(0, results.Count);
        }

        [Test]
        public void TestForgotViewPasswordModelNoUsername()
        {
            _forgotPasswordVM.Email = null;
            var results = TestHelper.Validate(_forgotPasswordVM);
            Assert.AreEqual(1, results.Count);
        }

    }
}
