﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using ZotCode.Domain.Model;
using ZotCode.Web.ViewModels;
using ZotCode.Web.Areas.Admin.ViewModels;

namespace ZotCode.UnitTests.ViewModelsTests
{
    class InvoiceViewModelTest
    {
        private InvoiceDetailsViewModel _invoiceDetailsVM;

        [SetUp]
        public void Init()
        {
            _invoiceDetailsVM = new InvoiceDetailsViewModel()
            {
                User = new User(),
                Flights = new Dictionary<int, Flight>(),
                Invoice = new Invoice(),
                Tickets = new Dictionary<int, Ticket>()
            };
        }

        [Test]
        public void TestInvoiceDetailsViewModel()
        {
            var result = TestHelper.Validate(_invoiceDetailsVM);
            Assert.AreEqual(result.Count, 0);
        }

        [Test]
        public void TestInvoiceDetailsViewModelNoUser()
        {
            _invoiceDetailsVM.User = null;
            var result = TestHelper.Validate(_invoiceDetailsVM);
            Assert.AreEqual(result.Count, 1);
        }
    }
}
