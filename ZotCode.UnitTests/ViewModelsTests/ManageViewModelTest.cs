﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using NUnit.Framework;
using ZotCode.Domain.Model;
using ZotCode.Web.ViewModels;

namespace ZotCode.UnitTests.ViewModelsTests
{
    class ManageViewModelTest
    {
        private IndexViewModel _indexVM;
        private FactorViewModel _factorVM;
        private SetPasswordViewModel _setPasswordVM;
        private ChangePasswordViewModel _changePasswordVM;
        private AddPhoneNumberViewModel _addPhoneNumberVM;
        private VerifyPhoneNumberViewModel _verifyPhoneNumberVM;
        private ConfigureTwoFactorViewModel _configureTwoFactorVM;
        private UpdateAddressViewModel _updateAddressVM;
        private UpdateUserDetailsViewModel _updateUserDetailsVM;

        [SetUp]
        public void Init()
        {
            _indexVM = new IndexViewModel()
            {
                PhoneNumber = testData.phone,
                BrowserRemembered = true,
                HasAddress = true,
                HasPassword = true,
                Logins = testData.infos,
                TwoFactor = true
            }; 
            
            _factorVM = new FactorViewModel()
            {
                Purpose = "some pupose"
            };

            _setPasswordVM = new SetPasswordViewModel()
            {
                ConfirmPassword = testData.password,
                NewPassword = testData.password
            };

            _changePasswordVM = new ChangePasswordViewModel()
            {
                OldPassword = testData.password,
                NewPassword = "newPassword",
                ConfirmPassword = "newPassword",
            };

            _addPhoneNumberVM = new AddPhoneNumberViewModel()
            {
                Number = testData.phone
            };

            _verifyPhoneNumberVM = new VerifyPhoneNumberViewModel()
            {
                Code = testData.code,
                PhoneNumber = testData.phone
            };

            _configureTwoFactorVM = new ConfigureTwoFactorViewModel()
            {
                SelectedProvider = testData.selectprov,
                Providers = testData.providers
            };

            _updateAddressVM = new UpdateAddressViewModel()
            {
                Countries = testData.countries,
                Id = 0,
                City = testData.city,
                CountryId = 0,
                PostCode = testData.postcode,
                State = testData.state,
                StreetName = testData.street,
                StreetNumber = 1,
                UnitNumber = 1,
                UserId = 0
            };

            _updateUserDetailsVM = new UpdateUserDetailsViewModel()
            {
                Email = testData.email,
                FirstName = testData.firstName,
                PhoneNumber = testData.phone,
                LastName = testData.lastName
            };
        }

        // IndexViewModel
        [Test]
        public void TestIndexViewModel()
        {
            var results = TestHelper.Validate(_indexVM);
            Assert.AreEqual(0, results.Count);
        }

        // FactorViewModel

        [Test]
        public void TestFactorViewModel()
        {
            var results = TestHelper.Validate(_factorVM);
            Assert.AreEqual(0, results.Count);
        }

        // SetPasswordViewModel
        [Test]
        public void TestSetPasswordViewModel()
        {
            var results = TestHelper.Validate(_setPasswordVM);
            Assert.AreEqual(0, results.Count);
        }

        [Test]
        public void TestSetPasswordViewModelNoMatchPw()
        {
            _setPasswordVM.ConfirmPassword = "123";
            var results = TestHelper.Validate(_setPasswordVM);
            Assert.AreEqual(1, results.Count);

        }

        //ChangePasswordViewModel
        [Test]
        public void TestChangePasswordViewModel()
        {
            var results = TestHelper.Validate(_changePasswordVM);
            Assert.AreEqual(0, results.Count);
        }

        [Test]
        public void TestChangePasswordViewModelNoMatch()
        {
            _changePasswordVM.ConfirmPassword = "123";
            var results = TestHelper.Validate(_changePasswordVM);
            Assert.AreEqual(1, results.Count);
        }

        //AddPhoneNumberViewModel
        [Test]
        public void TestAddPhoneNumberViewModel()
        {
            var results = TestHelper.Validate(_addPhoneNumberVM);
            Assert.AreEqual(0, results.Count);
        }

        //VerifyPhoneNumberViewModel
        [Test]
        public void TestVerifyPhoneNumberViewModel()
        {
            var results = TestHelper.Validate(_verifyPhoneNumberVM);
            Assert.AreEqual(0, results.Count);
        }

        //ConfigureTwoFactorViewModel
        [Test]
        public void TestConfigureTwoFactorViewModel()
        {
            var results = TestHelper.Validate(_configureTwoFactorVM);
            Assert.AreEqual(0, results.Count);
        }

        //UpdateAddressViewModel
        [Test]
        public void TestUpdateAddressViewModel()
        {
            var results = TestHelper.Validate(_updateAddressVM);
            Assert.AreEqual(0, results.Count);
        }

        //UpdateUserDetailsViewModel
        [Test]
        public void TestUpdateUserDetailsViewModel()
        {
            var results = TestHelper.Validate(_updateUserDetailsVM);
            Assert.AreEqual(0, results.Count);
        }

    }

    class testData
    {
        public static string firstName = "Monkey";
        public static string lastName = "Brains";
        public static string email = "test@test.com";
        public static string phone = "123456789";
        public static string street = "Alpha";
        public static string city = "Sydney";
        public static string state = "NSW";
        public static string selectprov = "Provder";
        public static string code = "testcode";
        public static string password = "password";
        public static int streetnum = 1;
        public static int unit = 1;
        public static ushort postcode = 1234;
        public static IList<UserLoginInfo> infos = new List<UserLoginInfo>()
        {
            new UserLoginInfo(String.Empty, String.Empty)
        };
        public static ICollection<Country> countries = new List<Country>()
        {
            DataManager.TestData.Countries.ToList()[0],
            DataManager.TestData.Countries.ToList()[1]
        };
        public static ICollection<SelectListItem> providers = new List<SelectListItem>()
        {
            new SelectListItem()
        };
    }
}
