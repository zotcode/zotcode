﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using NUnit.Framework;
using ZotCode.Web.ViewModels;
using ZotCode.Web.Areas.Admin.ViewModels;
using System.ComponentModel.DataAnnotations;

namespace ZotCode.UnitTests.ViewModelsTests
{
    class UserViewModelTest
    {
        private CreateUserViewModel _createUserViewModel;

        [SetUp]
        public void Init()
        {
            _createUserViewModel = new CreateUserViewModel
            {
                LastName = "Brains",
                FirstName = "Monkey",
                DateOfBirth = DateTime.Today,
                EmailAddress = "monkeybrains@business.com",
                PlainPassword = "hobbit4life",
            };
        }

        [Test]
        public void TestCreateUserViewModel()
        {
            var results = TestHelper.Validate(_createUserViewModel);
            Assert.AreEqual(0, results.Count);
        }

        [Test]
        public void TestWithNoFirstName()
        {
            _createUserViewModel.FirstName = null;
            var results = TestHelper.Validate(_createUserViewModel);
            Assert.AreEqual(1, results.Count);
            Assert.AreEqual("The First Name field is required.", results[0].ErrorMessage);
        }
    }
}
