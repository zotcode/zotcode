﻿using System;
using System.Collections.Generic;



namespace ZotCode.Web.ViewModels
{
    public class InvoiceViewModels
    {
    }
    public class InvoiceDetailsViewModel
    {
        public ZotCode.Domain.Model.User User { get; set; }

        public ZotCode.Domain.Model.Invoice Invoice {get; set;}

        public Dictionary<int, ZotCode.Domain.Model.Ticket> Tickets { get; set; }

        public Dictionary<int, ZotCode.Domain.Model.Flight> Flights { get; set; }


    }
}