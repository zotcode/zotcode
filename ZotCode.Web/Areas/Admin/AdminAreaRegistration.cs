﻿using System.Web.Mvc;

namespace ZotCode.Web.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
       
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        public override string AreaName 
        {
            get  { return "Admin"; }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                new { controller="Dashboard", action = "Index", id = UrlParameter.Optional }
            );

            log.Info("Admin Area successfully registered");
        }
    }
}