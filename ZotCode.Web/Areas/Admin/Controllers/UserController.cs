﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ZotCode.Domain.Model;
using ZotCode.Domain.DataManager;
using ZotCode.Domain.Model.Identity;
using ZotCode.Web.Controllers;
using ZotCode.Web.Areas.Admin.ViewModels;
using ZotCode.Web.ViewModels;

namespace ZotCode.Web.Areas.Admin.Controllers
{
    public partial class UserController : BaseController
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET: /Admin/User/
        public virtual ActionResult Index()
        {
            //This seems to be okay
            var users = DbContext.Users.Include(u => u.BillingAddress).Include(u => u.PostalAddress);
            return View("Index", users.ToList());
        }

        // GET: /Admin/User/Details/5
        public virtual ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // New fake DbContext does not like this
            User user = DbContext.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            return View(user);
        }

        // GET: /Admin/User/Create
        public virtual ActionResult Create()
        {
            return View();
        }

        // POST: /Admin/User/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create(CreateUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                // Commented this out for testing
                    // Convert plain password to hashed password
                    //var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    //var passwordHash = userManager.PasswordHasher.HashPassword(model.PlainPassword);
                var passwordHash = "mockPasswordHash";

                User newUser = new User()
                {
                    UserName = model.EmailAddress,
                    Email = model.EmailAddress,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    DateOfBirth = model.DateOfBirth,
                    PasswordHash = passwordHash
                };

                DbContext.Users.Add(newUser);
                DbContext.SaveChanges();

                log.InfoFormat("Admin has created a new user {0}-{1}", newUser.Id, newUser.UserName);

                return RedirectToAction(MVC.Admin.User.Index());
            }

            // Model was not valid
            return View(model);
        }

        // GET: /Admin/User/SetAddress/5
        public virtual ActionResult SetAddress(int? id)
        {
            // Check if id is null
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = DbContext.Users.SingleOrDefault(u => u.Id == id);
            if (user == null)
            {
                return HttpNotFound();
            }
            
            // Get the countries in the system
            var countries = DbContext.Countries.ToList();

            // Create two empty models for display and data entry
            var viewModel = new List<UpdateAddressViewModel>();
            viewModel.Add(new UpdateAddressViewModel() { Countries = countries, UserId = (int)id });
            viewModel.Add(new UpdateAddressViewModel() { Countries = countries, UserId = (int)id });
            ViewBag.UserName = user.FullName;
            return View(viewModel);
        }

        // POST: /Admin/User/SetAddress
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult SetAddress(List<UpdateAddressViewModel> model)
        {
            if(!ModelState.IsValid)
            {
                // Need to re-get the countries as they are not returned in the model
                var countries = DbContext.Countries.ToList();
                foreach (var m in model)
                {
                    m.Countries = countries;
                }

                return View(model);
            }

            // Create the addresses in the database
            foreach (UpdateAddressViewModel address in model)
            {
                Address newAddress = new Address()
                {
                    UnitNumber = address.UnitNumber,
                    StreetNumber = address.StreetNumber,
                    StreetName = address.StreetName,
                    City = address.City,
                    PostCode = address.PostCode,
                    State = address.State,
                    CountryId = address.CountryId,
                };

                DbContext.Addresses.ToList().Add(newAddress);

                // Save the database now so we can get back the Id of the new Address
                address.Id = newAddress.Id;
            }

            // Add the Address Ids to the user's account
            int userId = model[0].UserId;
            var user = DbContext.Users.SingleOrDefault(u => u.Id == userId);

            if (user != null)
            {
                // Update the user in the database
                user.PostalAddressId = model[0].Id;
                user.BillingAddressId = model[1].Id;

                DbContext.SaveChanges();
                log.InfoFormat("Administrator has added an address to user {0}-{1}", user.Id, user.UserName);
                return RedirectToAction(MVC.Admin.User.Details(user.Id));
            }
            else
            {
                log.ErrorFormat("Unable to add address to user's account. Unable to find user {0}", userId);
                return RedirectToAction(MVC.Admin.User.Index());
            }

        }

        // GET: /Admin/User/UpdateAddress/5
        public virtual ActionResult UpdateAddress(int? id)
        {

            // Check if the ID is null
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // Attempt to get user
            var user = DbContext.Users.SingleOrDefault(u => u.Id == id);
            if (user == null)
            {
                return HttpNotFound();
            }

            var viewModel = new List<UpdateAddressViewModel>();

            // Should only ever return two - Tyler Haigh (17 May 2015)
            var userAddreses = DbContext.Addresses.Where(add =>
                (user.PostalAddressId != null && add.Id == user.PostalAddressId)
                ||
                (user.BillingAddressId != null && add.Id == user.BillingAddressId)
                );

            var countries = DbContext.Countries.ToList();

            foreach (var address in userAddreses)
            {
                UpdateAddressViewModel modelledAddress = new UpdateAddressViewModel()
                {
                    Id = address.Id,
                    UnitNumber = address.UnitNumber ?? null,
                    StreetNumber = address.StreetNumber,
                    StreetName = address.StreetName,
                    City = address.City,
                    PostCode = address.PostCode,
                    State = address.State,
                    CountryId = address.CountryId,
                    Countries = countries,
                    UserId = (int)id
                };

                viewModel.Add(modelledAddress);
            }

            ViewBag.UserName = user.FullName;
            return View(viewModel);
        }

        // POST: /Admin/User/UpdateAddress
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult UpdateAddress(List<UpdateAddressViewModel> model)
        {
            if (!ModelState.IsValid)
            {
                // Need to re-get the countries as they are not returned in the model
                var countries = DbContext.Countries.ToList();
                foreach (var m in model)
                {
                    m.Countries = countries;
                }

                return View(model);
            }

            // Update the addresses
            foreach (UpdateAddressViewModel updatedAddress in model)
            {
                Address original = DbContext.Addresses.SingleOrDefault(add => add.Id == updatedAddress.Id);

                // Safety net. This should never return null
                if (original != null)
                {
                    original.UnitNumber = updatedAddress.UnitNumber;
                    original.StreetNumber = updatedAddress.StreetNumber;
                    original.StreetName = updatedAddress.StreetName;
                    original.City = updatedAddress.City;
                    original.PostCode = updatedAddress.PostCode;
                    original.State = updatedAddress.State;
                    original.CountryId = updatedAddress.CountryId;
                }
                else
                {
                    log.ErrorFormat("Address Id {0} was not found in the database, but should have been", updatedAddress.Id);
                }
            }

            // Save the database
            DbContext.SaveChanges();
            int userId = model[0].UserId;
            log.InfoFormat("Administrator has updated Address for user {0}. Address Id {1} and {2}", userId, model[0].Id, model[1].Id);

            return RedirectToAction(MVC.Admin.User.Details(userId));
        }

        // GET: /Admin/User/Edit/5
        public virtual ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            User user = DbContext.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            return View(user);
        }

        // POST: /Admin/User/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit(User model)
        {
            if (ModelState.IsValid)
            {
                var dbUser = DbContext.Users.SingleOrDefault(u => u.Id == model.Id);
                dbUser.FirstName = model.FirstName;
                dbUser.LastName = model.LastName;
                dbUser.Email = model.Email;
                dbUser.DateOfBirth = model.DateOfBirth;
                
                DbContext.SaveChanges();

                log.InfoFormat("Admin has updated a user's details {0} - {1}", model.Id, model.UserName);

                return RedirectToAction(MVC.Admin.User.Details(model.Id));
            }

            return View(model);
        }

        // GET: /Admin/User/Delete/5
        public virtual ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            User user = DbContext.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            return View(user);
        }

        // POST: /Admin/User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DeleteConfirmed(int id)
        {
            User user = DbContext.Users.Find(id);
            DbContext.Users.Remove(user);
            DbContext.SaveChanges();

            log.InfoFormat("Admin has deleted a user {0} - {1}", user.Id, user.UserName);

            return RedirectToAction(MVC.Admin.User.Index());
        }

    }
}
