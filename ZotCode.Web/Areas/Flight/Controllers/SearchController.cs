﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ZotCode.Domain.DataManager;
using ZotCode.Web.Areas.Flight.ViewModels;
using ZotCode.Web.Controllers;
using ZotCode.Web.Flight.Models;

namespace ZotCode.Web.Areas.Flight.Controllers
{
    public partial class SearchController : BaseController
    {

        // GET: /Flight/Search/
        public virtual ActionResult Index()
        {
            var model = new FlightSearchViewModel {AirportList = DbContext.AirPorts.ToList()};
            return View("Index", model);
        }

        // GET: Flight/Result
        [HttpPost]
        [ActionName("Index")]
        public virtual ActionResult SearchResult(FlightSearchViewModel model)
        {
            if (model == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return RedirectToAction("Result", model);
        }

        [ActionName("Result")]
        public virtual ActionResult Result(FlightSearchViewModel model)
        {
            if (model == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var search = new Search(model, DbContext);

            if (model.OneWay == false)
                return View(search.Result);
            else
                return RedirectToAction("OneWayResult", model);
        }

        [ActionName("OneWayResult")]
        public virtual ActionResult OneWayResult(FlightSearchViewModel model)
        {
            if(model == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var search = new Search(model, DbContext);

            return View(search.Result);
        }

        [ActionName("Details")]
        public virtual ActionResult Details(int? id)
        {
            if(id==null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Domain.Model.Flight flight = DbContext.Flights.Find(id);

            if(flight==null)
                return new HttpNotFoundResult();

            return View(flight);
        }

    }
}