﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZotCode.Domain.DataManager;
using ZotCode.Domain.Model;
using ZotCode.Domain.Model.System;

namespace ZotCode.Web.Flight.Models
{
    public class FlightRoute : IEquatable<FlightRoute>
    {
        // FlightRoute properties.
        public LinkedList<Domain.Model.Flight> FlightLegs { get; private set; }
        public LinkedList<AirPort> VisitedAirports { get; private set; }
        public int DepartureAirportId { get; set; }
        public int DestinationAirportId { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime ArrivalDate { get; set; }
        public int id { get; private set; } // id is set as part of the search algorithm.
        public Dictionary<Domain.Model.Flight, double> Cost { get; set; }
        public AirPort DestAirport { get; set; }

        public FlightRoute(int id, int deptId, int destId)
        {
            this.id = id;
            DepartureAirportId = deptId;
            DestinationAirportId = destId;
            FlightLegs = new LinkedList<Domain.Model.Flight>();
            VisitedAirports = new LinkedList<AirPort>();
            Cost = new Dictionary<Domain.Model.Flight, double>();
        }

        public FlightRoute(int id, FlightRoute routes)
        {
            this.id = id;
            DepartureAirportId = routes.DepartureAirportId;
            DestinationAirportId = routes.DestinationAirportId;
            FlightLegs = new LinkedList<Domain.Model.Flight>();
            VisitedAirports = new LinkedList<AirPort>();
            Cost = new Dictionary<Domain.Model.Flight, double>();
            Copy(routes);
        }

        // Check to see if FlightRoute is a circuit (I.e. visits the same Airport multiple times.
        // Returns true if FlightRoute is a circuit, otherwise false.
        public Boolean IsACircuit()
        {
            // Departure and Destination Airports should only be visited once.
            var departureCount = VisitedAirports.Count(x => x != null && x.Id == DepartureAirportId);
            var destinationCount = VisitedAirports.Count(x => x != null && x.Id == DestinationAirportId);
            if (departureCount > 1)
                return true;
            if (destinationCount > 1)
                return true;

            // All other Airports should only be listed twice. Once for arrival and Once for departure.
            return VisitedAirports.Select(a => VisitedAirports.Count(x => x != null && x.Id == a.Id)).Any(count => count > 2);
        }

        public void Copy(FlightRoute routes)
        {
            foreach (var f in routes.FlightLegs)
            {
                AppendFlight(f);
            }
        }

        public void AppendFlight(Domain.Model.Flight leg)
        {
            if (leg.StopOverId != null)
                if (leg.StopOverId == DestinationAirportId)
                {
                    ArrivalDate = (DateTime)leg.ArrivalTimeStopOver;
                    DestAirport = leg.StopOver;
                }
            if (leg.ArrivingAtId == DestinationAirportId)
            {
                ArrivalDate = leg.ArrivalDate;
                DestAirport = leg.ArrivingAt;
            }

            // Only add Flight if ArrivalAt & DepartingFrom is not in VisitedAirports.
            //if (!VisitedAirports.Contains(leg.ArrivingAt) && !VisitedAirports.Contains(leg.DepartingFrom))
            //{
            // Only add DepartingFrom Airport to VisitedAirports if it is not already in the list.
            if (VisitedAirports.Count == 0)
            {
                DepartureDate = leg.DepartureDate;
                VisitedAirports.AddLast(leg.DepartingFrom);
            }
            if (leg.StopOver != null)
                VisitedAirports.AddLast(leg.StopOver);
            VisitedAirports.AddLast(leg.ArrivingAt);

            FlightLegs.AddLast(leg);
            FlightLegs.Last.Value = leg;
            //}
        }

        public Boolean DoesConnect(AirPort a, AirPort b)
        {
            return DoesConnect(a.Id, b.Id);
        }

        public Boolean DoesConnect(int a, int b)
        {
            var first = FlightLegs.First.Value.DepartingFromId;
            var middle = FlightLegs.Last.Value.StopOverId;
            var last = FlightLegs.Last.Value.ArrivingAtId;
            return (first == a && (middle == b || last == b)) || (middle == a && last == b);
        }

        public String DepartureAirport()
        {
            return FlightLegs.First.Value.DepartingFrom.Name;
        }

        public String DestinationAirport()
        {
            return FlightLegs.Last.Value.ArrivingAt.Name;
        }

        public double Duration()
        {
            DateTime start = FlightLegs.First.Value.DepartureDate;
            DateTime finish = FlightLegs.Last.Value.ArrivalDate;
            TimeSpan duration = finish - start;
            return Math.Round(duration.TotalHours, 2);
        }

        public void CalcCost(SeatClass seatClass, DateTime date, IFlightPubContext dbContext)
        {
            int i = 0;
            foreach (Domain.Model.Flight f in FlightLegs)
            {
                i++;
                double cost = 0;
                Domain.Model.Flight next;

                if (i < FlightLegs.Count - 1)
                    next = FlightLegs.ElementAt(i); // Test to make sure it returns null if index out of bounds
                else
                    next = null;

                // Should always return one object as the query is based on availabilities set by the search algorithm
                // and further limited by the foreach statement
                IQueryable<Price> query = from p in dbContext.Query<Price>()
                                          where p.StartDate <= date
                                          && p.EndDate >= date
                                          && p.SeatClass == seatClass
                                          && p.FlightCode == f.Code
                                          select p;

                var firstOrDefault = query.FirstOrDefault();
                if (firstOrDefault != null)
                {
                    var leg1 = firstOrDefault.PriceLeg1 ?? 0;

                    var leg2 = firstOrDefault.PriceLeg2 ?? 0;

                    if (next != null) // Not last flight in FlightLegs
                    {
                        if (f.StopOver != null)
                        {
                            if (f.StopOver == next.DepartingFrom || f.StopOver == next.StopOver) // either case you'll get off at stopover
                                cost += (double)leg1;
                            else // Not getting off at stopover, therefore pay both legs.
                                cost += (double)leg1 + (double)leg2;
                        }
                        else
                            cost += (double)leg1;
                    }
                    else // last flight in FlightLegs
                    {
                        if (f.StopOver != null && f.StopOver == DestAirport)
                            cost += (double)leg1;
                        else if (f.StopOver != null && f.ArrivingAt == DestAirport)
                            cost += (double)leg1 + (double)leg2;
                        else
                            cost += (double)leg1;
                    }
                }

                Cost.Add(f, cost);
            }
        }

        public double GetCostCheapest()
        {
            var totalCost = FlightLegs.Sum(f => Cost[f]);
            return Math.Round(totalCost, 2);
        }

        public bool Equals(FlightRoute other)
        {
            //Check whether the compared object is null.
            if (Object.ReferenceEquals(other, null)) return false;

            var codeStr = FlightLegs.Aggregate("", (current, flight) => current + flight.Code);
            var otherCodeStr = other.FlightLegs.Aggregate("", (current, flight) => current + flight.Code);

            return (codeStr == otherCodeStr);
        }

        public override int GetHashCode()
        {
            var codeStr = FlightLegs.Aggregate("", (current, flight) => current + flight.Code);
            return codeStr.GetHashCode() ^
                   this.DepartureDate.GetHashCode();
        }
    }
}