﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZotCode.Domain.Model;

namespace ZotCode.Web.Areas.Flight.Models
{
    public class DistinctConnections
    {
        private AirPort DepartureAirport { get; set; }
        private AirPort ArrivalAirport { get; set; }
        private int DistanceKm { get; set; }

        public DistinctConnections(AirPort a, AirPort b)
        {
            DepartureAirport = a;
            ArrivalAirport = b;
        } 
    }
}