﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.UI.WebControls;
using ZotCode.Domain.DataManager;
using ZotCode.Domain.Model.System;
using ZotCode.Web.Areas.Flight.ViewModels;

namespace ZotCode.Web.Flight.Models
{
    public class Search
    {
        private int departureAirPortId;     // Id for the departure airport.
        private int destinationAirPortId;   // Id for the destination airport.
        private readonly IFlightPubContext _dbContext; // Reference to the database context.
        private const int DATE_OFFSET = 1;  // Range of days to search over.
        private const int DEPTH_MAX = 3;    // Depth limit of flight graph search.
        private const int MIN_STOPOVER = 1; // Used to offset arrival time and next flight departure time.
        private SeatClass seatClass;        // SeatClass is an enum from Availabilities model.
        public FlightResultViewModel Result { get; private set; }
        private int adults, children, reqTickets;
        private int id;

        
        /* 
            Default constractor.
        */
        public Search(FlightSearchViewModel model, FlightPubContext dbContext)
        {
            this._dbContext = dbContext;
            SearchIni(model);
        }

        /*
            Constructor which accepts a IFlightPubContext which is an interface that allows the Search algorithm to be passed
            alternate data sources. This has been added for testing.
        */
        public Search(FlightSearchViewModel model, IFlightPubContext dbContext)
        {
            this._dbContext = dbContext;
            SearchIni(model);
        }

        /* 
            Setup and initialize the Search algorithm class.
        */
        private void SearchIni(FlightSearchViewModel model)
        {
            seatClass = model.TicketClass;
            Result = new FlightResultViewModel();
            adults = model.Adults;
            children = model.Children;
            reqTickets = adults + children;
            id = 0;
            Result.DepartureRoutes = GetFlightRoutes(model.DepartureDate, model.DepartureId,
                model.DestinationId);
            Result.ReturnRoutes = GetFlightRoutes(model.ReturnDate, model.DestinationId, model.DepartureId);
        }

        /*
         * Returns a list of flights within parameters.
         */
        private List<Domain.Model.Flight> DirectFlight(int deptId, int destId, DateTime deptDate, int range)
        {
            var flights = new List<Domain.Model.Flight>();
            var tempFlightsList = new List<Domain.Model.Flight>();
            var limitDate = deptDate.AddDays(range);

            tempFlightsList.AddRange(
                    from flight in _dbContext.Query<Domain.Model.Flight>()
                    where (flight.DepartingFromId == deptId
                    && (flight.ArrivingAtId == destId || flight.StopOverId == destId))
                    && flight.DepartureDate >= deptDate
                    && flight.DepartureDate <= limitDate
                    select flight
                );

            tempFlightsList.AddRange(
                    from flight in _dbContext.Query<Domain.Model.Flight>()
                    where flight.StopOverId == deptId && flight.ArrivingAtId == destId
                    && flight.DepartureTimeStopOver >= deptDate
                    && flight.DepartureTimeStopOver <= limitDate
                    select flight
                );

            // Add availabilities to each flight.
            foreach (var f in tempFlightsList)
            {
                if (f.StopOverId != destId)
                {
                    var seats = new List<Availability>();
                    seats.AddRange(
                            from seat in _dbContext.Query<Availability>()
                            where seat.FlightId == f.Id
                            && seat.Class == seatClass          // Restrict availabilities based on the seatclass.
                            && seat.AvailableLeg1 >= reqTickets // Availabilities are only added if there are enough seats available for both legs (if two legged flight).
                            && (seat.AvailableLeg2 >= reqTickets || seat.AvailableLeg2 == null)
                            select seat
                        );
                    f.FlightAvailabilities = seats;
                }
                else
                {
                    var seats = new List<Availability>();
                    seats.AddRange(
                            from seat in _dbContext.Query<Availability>()
                            where seat.FlightId == f.Id
                            && seat.Class == seatClass          // Restrict availabilities based on the seatclass.
                            && seat.AvailableLeg1 >= reqTickets // Availabilities are only added if there are enough seats available for both legs (if two legged flight).
                            select seat
                        );
                    f.FlightAvailabilities = seats;
                }
            }

            // Filter flights which meet required number of seats 'requiredSeats' param.
            // Note: each availabilty has multiple seats.
            flights.AddRange(
                    from f in tempFlightsList
                    where f.FlightAvailabilities.Count >= 1       // Only add to result if there are availabilities.
                    select f
                );

            return flights;
        }

        /*
         * Get a list of FlightRoutes based on current search.
         * Precondition:    Search object has been created.
         * Postcondition:   Returns a List of flights which match search criteria.
         */
        public IList<FlightRoute> GetFlightRoutes(DateTime date, int startId, int finishId)
        {
            // To measure query performance.
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            departureAirPortId = startId;
            destinationAirPortId = finishId;

            var routes = new List<FlightRoute>();
            var depth = 0;
            var connections = GetAdjacentConnections(departureAirPortId, date, DATE_OFFSET);

            // For each Flight in DistinctConnections from initial AirPort (represented by AirPort.Id)
            // create a new FlightRoute and append the Flight to the start of that FlightRoute.
            depth++;
            foreach (var c in connections)
            {
                // Get base cases - direct connections.
                var flights = DirectFlight(departureAirPortId, c.ArriveId, date, DATE_OFFSET);
                foreach (var f in flights)
                {
                    var r = new FlightRoute(id, departureAirPortId, destinationAirPortId);
                    id++;
                    r.AppendFlight(f);

                    if (r.DoesConnect(departureAirPortId, destinationAirPortId) && !r.IsACircuit())
                        routes.Add(r);
                    else
                    {
                        // Recursively add Flights to new FlightRoutes created from the passed in FlightRoute.
                        var stopOverFlag = false;
                        var currentAirportId = f.ArrivingAtId;
                        AddFlightRoutes(currentAirportId, routes, r, depth, stopOverFlag);
                        if (f.StopOverId != null)
                        {
                            stopOverFlag = true;
                            AddFlightRoutes(
                                (int) f.StopOverId, routes,
                                r, depth, stopOverFlag);
                        }
                    }
                }
            }

            // Calculate costs
            foreach (var r in routes)
                r.CalcCost(seatClass, date, _dbContext);

            stopwatch.Stop();
            var performance = stopwatch.ElapsedMilliseconds;

            // Remove duplications
            IEnumerable<FlightRoute> createDistinctList =
                routes;

            createDistinctList = createDistinctList.Distinct();

            return createDistinctList.ToList();
        }

        // Recursively adds Flights to FlightRoutes
        /// <param name="currAirport">The airport node in focus.</param>
        /// <param name="routes">A list of all current FlightRoutes for this search query.</param>
        /// <param name="current">The current FlightRoute that a flight will be added to, used for recursively creating FlightRoutess.</param>
        /// <param name="depth">The current depth of recursion.</param>
        private void AddFlightRoutes(int currAirport, List<FlightRoute> routes, FlightRoute current, int depth, bool sFlag)
        {
            var currDepth = depth;

            // For each Flight in DistinctConnections from current AirPort 'currAirport' (represented by AirPort.Id)
            // create a new FlightRoute and append the Flight to the start of that FlightRoute.
            currDepth++;
            if (currDepth <= DEPTH_MAX)
            {
                var deptDate = new DateTime();
                if (!sFlag)
                    deptDate = current.FlightLegs.Last.Value.ArrivalDate;
                else if (current.FlightLegs.Last.Value
                    .ArrivalTimeStopOver != null)
                    deptDate =
                        (DateTime)current.FlightLegs.Last.Value
                            .ArrivalTimeStopOver;
                var connections = GetAdjacentConnections(currAirport, deptDate, DATE_OFFSET);

                foreach (var c in connections)
                {
                    var flights = DirectFlight(currAirport, c.ArriveId,deptDate.AddHours(MIN_STOPOVER), DATE_OFFSET);

                    foreach (var f in flights)
                    {
                        var r = new FlightRoute(id, current);
                        id++;
                        //if (!r.VisitedAirports.Contains(f.ArrivingAt))
                        r.DepartureAirportId = departureAirPortId;
                        r.DestinationAirportId = destinationAirPortId;
                        r.AppendFlight(f);
                        if (r.DoesConnect(departureAirPortId, destinationAirPortId) && !r.IsACircuit())
                            routes.Add(r);
                        else
                        {
                            // Recursively add Flights to new FlightRoutes created from the passed in FlightRoute.
                            var stopOverFlag = false;
                            var currentAirportId = f.ArrivingAtId;
                            AddFlightRoutes(currentAirportId, routes, r, currDepth, stopOverFlag);
                            if (f.StopOverId != null)
                            {
                                stopOverFlag = true;
                                AddFlightRoutes(
                                    (int) f.StopOverId,
                                    routes, r, currDepth, stopOverFlag);
                            }
                        }
                    }
                }
            }
        }

        // Create a list of adjacent connections to an Airport.
        private List<DistinctConnection> GetAdjacentConnections(int id, DateTime deptDate, int range)
        {
            var deptDateMax = deptDate.AddDays(range);
            var connections = new List<DistinctConnection>();
            // Add stopover airports to DistinctConnection.
            connections.AddRange(
                    (
                        from flight in _dbContext.Query<Domain.Model.Flight>()
                        where flight.DepartingFromId == id
                        && flight.DepartureDate >= deptDate
                        && flight.DepartureDate <= deptDateMax
                        && flight.StopOverId != null
                        select flight.StopOverId
                    )
                    .Distinct()
                    .ToArray()
                    .Select(x => new DistinctConnection(id, (int)x)));
            // Add arrival airports to DistinctConnection.
            connections.AddRange(
                    (
                        from flight in _dbContext.Query<Domain.Model.Flight>()
                        where flight.DepartingFromId == id
                        && flight.DepartureDate >= deptDate
                        && flight.DepartureDate <= deptDateMax
                        select flight.ArrivingAtId
                    )
                    .Distinct()
                    .ToArray()
                    .Select(x => new DistinctConnection(id, x)));

            // Add arrival airports to DistinctConnection.
            connections.AddRange(
                    (
                        from flight in _dbContext.Query<Domain.Model.Flight>()
                        where flight.StopOverId == id
                        && flight.DepartureTimeStopOver >= deptDate
                        && flight.DepartureTimeStopOver <= deptDateMax
                        select flight.ArrivingAtId
                    )
                    .Distinct()
                    .ToArray()
                    .Select(x => new DistinctConnection(id, x)));

            return connections;
        }

        // Support private member class which holds an edge between two airport ids.
        private class DistinctConnection
        {
            public int DepartId { get; private set; }
            public int ArriveId { get; private set; }

            public DistinctConnection(int a, int b)
            {
                DepartId = a;
                ArriveId = b;
            }
        }
    }
}