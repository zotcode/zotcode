﻿using System.Web.Mvc;

namespace ZotCode.Web.Areas.Flight
{
    public class FlightAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Flight";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Flight_default",
                "Flight/{controller}/{action}/{id}",
                new { controller = "Search", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}