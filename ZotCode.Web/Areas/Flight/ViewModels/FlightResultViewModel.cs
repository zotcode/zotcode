﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZotCode.Web.Flight.Models;

namespace ZotCode.Web.Areas.Flight.ViewModels
{
    public class FlightResultViewModel
    {
        public IList<FlightRoute> DepartureRoutes { get; set; } 
        public IList<FlightRoute> ReturnRoutes { get; set; }
        public Domain.Model.Flight FieldNames { get; private set; }

        public FlightResultViewModel()
        {
            FieldNames = new Domain.Model.Flight();
        }
    }
}