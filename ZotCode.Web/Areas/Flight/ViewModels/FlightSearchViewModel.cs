﻿using System;
using System.Collections.Generic;
using ZotCode.Domain.Model;
using ZotCode.Domain.Model.System;
using System.ComponentModel.DataAnnotations;

namespace ZotCode.Web.Areas.Flight.ViewModels
{
    public class FlightSearchViewModel
    {
        public int DepartureId { get; set; }
        public int DestinationId { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public int Adults { get; set; }
        public int Children { get; set; }
        public SeatClass TicketClass { get; set; }
        public bool OneWay { get; set; }

        public IEnumerable<AirPort> AirportList { get; set; }
        public IEnumerable<SeatClass> SeatClasses { get; set; }
    }
}