﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.UI.WebControls;
using ZotCode.Domain.DataManager;
using ZotCode.Domain.Model;
using ZotCode.Domain.Model.System;
using ZotCode.Web.Areas.Flight.ViewModels;

namespace ZotCode.Web.Algorithm
{
    public class Search
    {
        private int departureAirPortId;     // Id for the departure airport.
        private int destinationAirPortId;   // Id for the destination airport.
        private FlightPubContext dbContext; // Reference to the database context.
        private const int DATE_OFFSET = 2;  // Range of days to search over.
        private const int DEPTH_MAX = 3;    // Depth limit of flight graph search.
        private const int MIN_STOPOVER = 1; // Used to offset arrival time and next flight departure time.
        public FlightResultViewModel Result { get; private set; }
        private int adults, children, reqTickets;
        /*
         * 
         */
        public Search(FlightSearchViewModel model, FlightPubContext dbContext)
        {
            this.dbContext = dbContext;
            Result = new FlightResultViewModel();
            adults = model.Adults;
            children = model.Children;
            reqTickets = adults + children;
            Result.DepartureRoutes = GetFlightRoutes(model.DepartureDate, model.DepartureId,
                model.DestinationId);
            Result.ReturnRoutes = GetFlightRoutes(model.ReturnDate, model.DestinationId, model.DepartureId);
        }

        /*
         * Returns a list of flights within parameters.
         */
        private List<Flight> DirectFlight(int deptId, int destId, DateTime deptDate, int range)
        {
            List<Flight> flights, tempFlightsList;
            flights = new List<Flight>();
            tempFlightsList = new List<Flight>();
            DateTime limitDate = deptDate.AddDays(range);

            tempFlightsList.AddRange(
                    from f in dbContext.Flights
                    where f.DepartingFromId == deptId
                    && f.ArrivingAtId == destId
                    && f.DepartureDate >= deptDate
                    && f.DepartureDate <= limitDate
                    select f
                );

            // Add availabilities to each flight.
            foreach(Flight f in tempFlightsList)
            {
                List<Availability> seats = new List<Availability>();
                seats.AddRange(
                        from seat in dbContext.Availabilities
                        where seat.FlightId == f.Id
                        select seat
                    );
                f.AvailableSeats = seats;
            }

            // Filter flights which meet required filters; seat class, seat availability, etc.
            flights.AddRange(
                    from f in tempFlightsList
                    where f.AvailableSeats.Count >= this.reqTickets // reqTickets is instantiated in the constructor.
                    select f
                );
                

            return flights;
        }

        /*
         * Get a list of FlightRoutes based on current search.
         * Precondition:    Search object has been created.
         * Postcondition:   Returns a List of flights which match search criteria.
         */
        public List<FlightRoute> GetFlightRoutes(DateTime date, int startId, int finishId)
        {
            // To measure query performance.
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            departureAirPortId = startId;
            destinationAirPortId = finishId;

            List<FlightRoute> routes = new List<FlightRoute>();
            int depth = 0;
            List<DistinctConnection> connections = GetAdjacentConnections(departureAirPortId, date, DATE_OFFSET);

            // For each Flight in DistinctConnections from initial AirPort (represented by AirPort.Id)
            // create a new FlightRoute and append the Flight to the start of that FlightRoute.
            depth++;
            foreach (var c in connections)
            {
                List<Flight> flights = DirectFlight(departureAirPortId, c.ArriveId, date, DATE_OFFSET);
                foreach (var f in flights)
                {
                    FlightRoute r = new FlightRoute();
                    r.AppendFlight(f);
                    if (r.DoesConnect(departureAirPortId, destinationAirPortId))
                        routes.Add(r);
                    else
                    {
                        // Recursively add Flights to new FlightRoutes created from the passed in FlightRoute.
                        int currentAirportId = f.ArrivingAtId;
                        AddFlightRoutes(currentAirportId, routes, r, depth);
                    }
                }
            }

            // Add availabilities
            AddAvailabilities(routes);

            stopwatch.Stop();
            long performance = stopwatch.ElapsedMilliseconds;
            return routes;
        }

        private void AddAvailabilities(List<FlightRoute> routes)
        {
            
        }


        // Recursively adds Flights to FlightRoutes
        /// <param name="currAirport">The airport node in focus.</param>
        /// <param name="routes">A list of all current FlightRoutes for this search query.</param>
        /// <param name="current">The current FlightRoute that a flight will be added to, used for recursively creating FlightRoutess.</param>
        /// <param name="depth">The current depth of recursion.</param>
        private void AddFlightRoutes(int currAirport, List<FlightRoute> routes, FlightRoute current, int depth)
        {
            int currDepth = depth;

            // For each Flight in DistinctConnections from current AirPort 'currAirport' (represented by AirPort.Id)
            // create a new FlightRoute and append the Flight to the start of that FlightRoute.
            currDepth++;
            if (currDepth <= DEPTH_MAX)
            {
                DateTime deptDate = current.FlightLegs.Last.Value.ArrivalDate;
                List<DistinctConnection> connections = GetAdjacentConnections(currAirport, deptDate, DATE_OFFSET);

                foreach (var c in connections)
                {
                    List<Flight> flights = DirectFlight(currAirport, c.ArriveId,
                        current.FlightLegs.Last.Value.ArrivalDate.AddHours(MIN_STOPOVER), DATE_OFFSET);

                    foreach (var f in flights)
                    {
                        FlightRoute r = new FlightRoute(current);
                        r.AppendFlight(f);
                        if (r.DoesConnect(departureAirPortId, destinationAirPortId))
                            routes.Add(r);
                        else
                        {
                            // Recursively add Flights to new FlightRoutes created from the passed in FlightRoute.
                            int currentAirportId = f.ArrivingAtId;
                            AddFlightRoutes(currentAirportId, routes, r, currDepth);
                        }
                    }
                }
            }
        } 

        /*
         * 
         */
        private List<DistinctConnection> GetAdjacentConnections(int id, DateTime deptDate, int range)
        {
            DateTime deptDateMax = deptDate.AddDays(range);
            List<DistinctConnection> connections = new List<DistinctConnection>();
            connections.AddRange(
                    (
                        from flight in dbContext.Flights
                        where flight.DepartingFromId == id
                        && flight.DepartureDate >= deptDate
                        && flight.DepartureDate <= deptDateMax
                        select flight.ArrivingAtId
                    )
                    .Distinct()
                    .ToArray()
                    .Select(x => new DistinctConnection(id, x)));
            return connections;
        } 

        /*
         * Support private member class which holds an edge between two airport id's.
         */
        private class DistinctConnection 
        {
            public int DepartId { get; private set; }
            public int ArriveId { get; private set; }

            public DistinctConnection(int a, int b)
            {
                DepartId = a;
                ArriveId = b;
            }
        }
    }
}