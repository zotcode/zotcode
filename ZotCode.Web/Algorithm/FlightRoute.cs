﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using ZotCode.Domain.Model;
using ZotCode.Domain.Model.System;

namespace ZotCode.Web.Algorithm
{
    public class FlightRoute
    {
        public LinkedList<Flight> FlightLegs { get; private set; }

        public FlightRoute()
        {
            FlightLegs = new LinkedList<Flight>();
        }

        public FlightRoute(FlightRoute routes)
        {
            FlightLegs = new LinkedList<Flight>();
            Copy(routes);
        }

        public void Copy(FlightRoute routes)
        {
            foreach (var f in routes.FlightLegs)
            {
                AppendFlight(f);
            }
        }

        public void AppendFlight(Flight leg)
        {
            FlightLegs.AddLast(leg);
            FlightLegs.Last.Value = leg;
        }

        public Boolean DoesConnect(AirPort a, AirPort b)
        {
            return DoesConnect(a.Id, b.Id);
        }

        public Boolean DoesConnect(int a, int b)
        {
            var first = FlightLegs.First.Value.DepartingFromId;
            var last = FlightLegs.Last.Value.ArrivingAtId;
            return (first == a && last == b);
        }

        public int Count()
        {
            return FlightLegs.Count;
        }

        public String DepartureAirport()
        {
            return FlightLegs.First.Value.DepartingFrom.Name;
        }

        public String DestinationAirport()
        {
            return FlightLegs.Last.Value.ArrivingAt.Name;
        }

        public double Duration()
        {
            DateTime start = FlightLegs.First.Value.DepartureDate;
            DateTime finish = FlightLegs.Last.Value.ArrivalDate;
            TimeSpan duration = finish - start;
            return Math.Round(duration.TotalHours, 2);
        }
    }
}