﻿using Microsoft.Owin;
using Owin;
using ZotCode.Web;

[assembly: OwinStartup(typeof(Startup))]
[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace ZotCode.Web
{
    public partial class Startup
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            log.Info("Application started successfully");
        }
    }
}
