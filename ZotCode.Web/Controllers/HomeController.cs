﻿using System.Web.Mvc;
using ZotCode.Web.Areas.Admin.Controllers;

namespace ZotCode.Web.Controllers
{
    public partial class HomeController : BaseController
    {
        public virtual ActionResult Index()
        {
            return RedirectToAction(MVC.Flight.Search.Index());
        }

        [ActionName("About")]
        public virtual ActionResult About()
        {
            ViewBag.Title = "About FlightPub";
            ViewBag.SubTitle = "Proud Flight Specialists";
            ViewBag.Message = "Who Are We?";

            return View("About");
        }

        [ActionName("Contact")]
        public virtual ActionResult Contact()
        {
            ViewBag.Title = "Contact";
            ViewBag.SubTitle = "FlightPub\'s Friendly Consultants";
            ViewBag.Message = "Contact Details";

            return View("Contact");
        }
    }
}