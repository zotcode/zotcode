﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZotCode.Domain.DataManager;
using ZotCode.Domain.Model;
using Microsoft.AspNet.Identity.Owin;

namespace ZotCode.Web.Controllers
{
    public abstract partial class BaseController : Controller
    {

        private FlightPubContext _dbContext;
        public FlightPubContext DbContext
        {
            get { return _dbContext ?? FlightPubContext.Create(); }
            set { _dbContext = value; }
        }

    }
}

/*
private FlightPubContext _dbContext;
Replace the get/set code blocks
return _dbContext ?? FlightPubContext.Create();
_dbContext = value
 
 */