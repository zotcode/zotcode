﻿using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ZotCode.Web.ViewModels;
using ZotCode.Domain.Model;
using System.Collections.Generic;
using ZotCode.Domain.DataManager;
using System.Net;
using ZotCode.Domain.Model.Identity;

namespace ZotCode.Web.Controllers
{
    [Authorize]
    public partial class ManageController : BaseController
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ManageController()
        {
        }

        public ManageController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: /Manage/Index
        public virtual async Task<ActionResult> Index(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.SetTwoFactorSuccess ? "Your two-factor authentication provider has been set."
                : message == ManageMessageId.Error ? "An error has occurred."
                : message == ManageMessageId.AddPhoneSuccess ? "Your phone number was added."
                : message == ManageMessageId.RemovePhoneSuccess ? "Your phone number was removed."
                : "";

            var userId = User.Identity.GetUserId<int>();
            var model = new IndexViewModel
            {
                HasPassword = HasPassword(),
                PhoneNumber = await UserManager.GetPhoneNumberAsync(userId),
                TwoFactor = await UserManager.GetTwoFactorEnabledAsync(userId),
                Logins = await UserManager.GetLoginsAsync(userId),
                BrowserRemembered = await AuthenticationManager.TwoFactorBrowserRememberedAsync(userId.ToString()),
                HasAddress = HasAddress()
            };
            return View(model);
        }

        // POST: /Manage/RemoveLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> RemoveLogin(string loginProvider, string providerKey)
        {
            ManageMessageId? message;
            var result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId<int>(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction(MVC.Manage.ManageLogins(message));
        }

        // GET: /Manage/AddPhoneNumber
        public virtual ActionResult AddPhoneNumber()
        {
            return View();
        }

        // POST: /Manage/AddPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> AddPhoneNumber(AddPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            // Generate the token and send it
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId<int>(), model.Number);
            if (UserManager.SmsService != null)
            {
                var message = new IdentityMessage
                {
                    Destination = model.Number,
                    Body = "Your security code is: " + code
                };
                await UserManager.SmsService.SendAsync(message);
            }
            return RedirectToAction(MVC.Manage.VerifyPhoneNumber(model.Number));
        }

        // POST: /Manage/EnableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> EnableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId<int>(), true);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction(MVC.Manage.Index());
        }

        // POST: /Manage/DisableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> DisableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId<int>(), false);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction(MVC.Manage.Index());
        }

        // GET: /Manage/VerifyPhoneNumber
        public virtual async Task<ActionResult> VerifyPhoneNumber(string phoneNumber)
        {
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId<int>(), phoneNumber);
            // Send an SMS through the SMS provider to verify the phone number
            return phoneNumber == null ?
                View(MVC.Shared.Views.Error) : 
                View(new VerifyPhoneNumberViewModel { PhoneNumber = phoneNumber });
        }

        // POST: /Manage/VerifyPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePhoneNumberAsync(User.Identity.GetUserId<int>(), model.PhoneNumber, model.Code);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction(MVC.Manage.Index(ManageMessageId.AddPhoneSuccess));
            }
            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "Failed to verify phone");
            return View(model);
        }

        // GET: /Manage/RemovePhoneNumber
        public virtual async Task<ActionResult> RemovePhoneNumber()
        {
            var result = await UserManager.SetPhoneNumberAsync(User.Identity.GetUserId<int>(), null);
            if (!result.Succeeded)
            {
                return RedirectToAction(MVC.Manage.Index(ManageMessageId.Error));
            }
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction(MVC.Manage.Index(ManageMessageId.RemovePhoneSuccess));
        }

        // GET: /Manage/ChangePassword
        public virtual ActionResult ChangePassword()
        {
            return View();
        }

        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId<int>(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction(MVC.Manage.Index(ManageMessageId.ChangePasswordSuccess));
            }
            AddErrors(result);
            return View(model);
        }

        // GET: /Manage/SetPassword
        public virtual ActionResult SetPassword()
        {
            return View();
        }

        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await UserManager.AddPasswordAsync(User.Identity.GetUserId<int>(), model.NewPassword);
                if (result.Succeeded)
                {
                    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
                    if (user != null)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    }
                    return RedirectToAction(MVC.Manage.Index(ManageMessageId.SetPasswordSuccess));
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Manage/ManageLogins
        public virtual async Task<ActionResult> ManageLogins(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
            if (user == null)
            {
                return View(MVC.Shared.Views.Error);
            }

            var userLogins = await UserManager.GetLoginsAsync(User.Identity.GetUserId<int>());
            var otherLogins = AuthenticationManager.GetExternalAuthenticationTypes()
                .Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider))
                .ToList();
            ViewBag.ShowRemoveButton = user.PasswordHash != null || userLogins.Count > 1;
            
            return View(new ManageLoginsViewModel
            {
                CurrentLogins = userLogins,
                OtherLogins = otherLogins
            });
        }

        // POST: /Manage/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new AccountController.ChallengeResult(provider, Url.Action("LinkLoginCallback", "Manage"), User.Identity.GetUserId());
        }

        // GET: /Manage/LinkLoginCallback
        public virtual async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction(MVC.Manage.ManageLogins(ManageMessageId.Error));
            }
            
            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId<int>(), loginInfo.Login);
            return result.Succeeded ?
                RedirectToAction(MVC.Manage.ManageLogins()) :
                RedirectToAction(MVC.Manage.ManageLogins(ManageMessageId.Error));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        // GET: /Manage/ChangeDetails
        public virtual ActionResult ChangeDetails()
        {
            var model = new UpdateUserDetailsViewModel();
            
            // Get the user's details
            var user = UserManager.FindById(User.Identity.GetUserId<int>());

            if (user != null)
            {
                model.FirstName = user.FirstName;
                model.LastName = user.LastName;
                model.Email = user.Email;
                model.PhoneNumber = user.PhoneNumber;
            }
            else
            {
                log.Error("User attempted to change their details, but was not logged in");
                throw new HttpException((int)HttpStatusCode.Unauthorized, "User was not authenticated");
            }
            
            return View(model);
        }

        // POST: /Manage/ChangeDetails
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult ChangeDetails(UpdateUserDetailsViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var user = UserManager.FindById(User.Identity.GetUserId<int>());
            
            if (user != null)
            {
                // Update the user in the database
                var dbUser = DbContext.Users.SingleOrDefault(u => u.Id == user.Id);
                dbUser.FirstName = model.FirstName;
                dbUser.LastName = model.LastName;
                dbUser.Email = model.Email;
                dbUser.PhoneNumber = model.PhoneNumber;

                DbContext.SaveChanges();
            }
            else
            {
                log.Error("User attempted to save changes to their details, but was not logged in");
                throw new HttpException((int)HttpStatusCode.Unauthorized, "User was not authenticated");
            }

            return RedirectToAction(MVC.Manage.Index());

        }

        // GET: /Manage/ChangeAddress
        [HttpGet]
        public virtual ActionResult ChangeAddress()
        {
            
            var viewModel = new List<UpdateAddressViewModel>();
            
            var user = UserManager.FindById(User.Identity.GetUserId<int>());
            if (user != null)
            {
                // Should only ever return two - Tyler Haigh (17 May 2015)
                var userAddreses = DbContext.Addresses.Where(add =>
                    (user.PostalAddressId != null && add.Id == user.PostalAddressId)
                    ||
                    (user.BillingAddressId != null && add.Id == user.BillingAddressId)
                    );

                var countries = DbContext.Countries.ToList();

                foreach (var address in userAddreses)
                {
                    UpdateAddressViewModel modelledAddress = new UpdateAddressViewModel()
                    {
                        Id = address.Id,
                        UnitNumber = address.UnitNumber ?? null,
                        StreetNumber = address.StreetNumber,
                        StreetName = address.StreetName,
                        City = address.City,
                        PostCode = address.PostCode,
                        State = address.State,
                        CountryId = address.CountryId,
                        Countries = countries
                    };
                    
                    viewModel.Add(modelledAddress);
                }

                return View(viewModel);
            }

            // User was null
            throw new HttpException((int)HttpStatusCode.Unauthorized, "User not authenticated");
        }

        // POST: /Manage/ChangeAddress
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult ChangeAddress(List<UpdateAddressViewModel> model)
        {
            if (!ModelState.IsValid)
            {
                // Need to re-get the countries as they are not returned in the model
                var countries = DbContext.Countries.ToList();
                foreach(var m in model)
                {
                    m.Countries = countries;
                }
                
                return View(model);
            }

            // Update the addresses
            foreach (UpdateAddressViewModel updatedAddress in model)
            {
                Address original = DbContext.Addresses.SingleOrDefault(add => add.Id == updatedAddress.Id);

                // Safety net. This should never return null
                if (original != null)
                {
                    original.UnitNumber = updatedAddress.UnitNumber;
                    original.StreetNumber = updatedAddress.StreetNumber;
                    original.StreetName = updatedAddress.StreetName;
                    original.City = updatedAddress.City;
                    original.PostCode = updatedAddress.PostCode;
                    original.State = updatedAddress.State;
                    original.CountryId = updatedAddress.CountryId;
                }
                else
                {
                    log.ErrorFormat("Address Id {0} was not found in the database, but should have been", updatedAddress.Id);
                }
            }

            // Save the database
            DbContext.SaveChanges();

            return RedirectToAction(MVC.Manage.Index());
        }
        
        // GET: /Manage/SetAddress
        public virtual ActionResult SetAddress()
        {

            // Get the countries in the system
            var countries = DbContext.Countries.ToList();
            
            // Create two empty models for display and data entry
            var viewModel = new List<UpdateAddressViewModel>();
            viewModel.Add(new UpdateAddressViewModel() { Countries = countries });
            viewModel.Add(new UpdateAddressViewModel() { Countries = countries });

            return View(viewModel);
        }

        // POST: /Manage/SetAddress
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult SetAddress(List<UpdateAddressViewModel> model)
        {
            if (!ModelState.IsValid)
            {
                // Need to re-get the countries as they are not returned in the model
                var countries = DbContext.Countries.ToList();
                foreach (var m in model)
                {
                    m.Countries = countries;
                }

                return View(model);
            }

            // Create the addresses in the database
            foreach (UpdateAddressViewModel address in model)
            {
                Address newAddress = new Address()
                {
                    UnitNumber = address.UnitNumber,
                    StreetNumber = address.StreetNumber,
                    StreetName = address.StreetName,
                    City = address.City,
                    PostCode = address.PostCode,
                    State = address.State,
                    CountryId = address.CountryId,
                };

                DbContext.Addresses.Add(newAddress);

                // Save the database now so we can get back the Id of the new Address
                DbContext.SaveChanges();

                address.Id = newAddress.Id;
            }


            // Add the Address Ids to the user's account
            var user = UserManager.FindById(User.Identity.GetUserId<int>());

            if (user != null)
            {
                // Update the session user
                user.PostalAddressId = model[0].Id;
                user.BillingAddressId = model[1].Id;

                // Update the user's details in the database
                var dbUser = DbContext.Users.SingleOrDefault(u => u.Id == user.Id);
                dbUser.PostalAddressId = model[0].Id;
                dbUser.BillingAddressId = model[1].Id;
                DbContext.SaveChanges();
            }
            else
            {
                log.Error("Unable to add address to user's account. Unable to find logged in user");
            }

            return RedirectToAction(MVC.Manage.Index());

        }

#region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId<int>());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private bool HasPhoneNumber()
        {
            var user = UserManager.FindById(User.Identity.GetUserId<int>());
            if (user != null)
            {
                return user.PhoneNumber != null;
            }
            return false;
        }

        private bool HasAddress()
        {
            var user = UserManager.FindById(User.Identity.GetUserId<int>());
            if (user != null)
            {
                return user.PostalAddressId != null && user.BillingAddressId != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

#endregion
    }
}