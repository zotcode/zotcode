﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZotCode.Domain.DataManager;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using ZotCode.Web.ViewModels;
using System.Net;

namespace ZotCode.Web.Controllers
{
    public partial class InvoiceController : BaseController
    {
        /*
        public FlightPubContext DbContext
        {
            get
            {
                return HttpContext.GetOwinContext().Get<FlightPubContext>();    
            }
        } */
        // GET: Invoice
        //Returns all of the onvoices connected with the current users account
        public virtual ActionResult Index()
        {
            var userID = Convert.ToInt32(User.Identity.GetUserId<int>());
            var invoices = DbContext.Invoices.Where(inv => inv.UserId == userID);
            return View(invoices);
        }

        public virtual ActionResult Details(int? id)
        {
            
            //Create ViewModel
            var invoiceModel = new InvoiceDetailsViewModel();
            if (id == null)
            {
                throw new HttpException((int)HttpStatusCode.NotFound, "ID Not Found");

            }        
            //Get the selected invoice            
            invoiceModel.Invoice = DbContext.Invoices.SingleOrDefault(inv => inv.Id == id);
            if(invoiceModel.Invoice == null)
            {
                throw new HttpException((int)HttpStatusCode.NotFound, "Invoice Not Found");
            }
            
            //Get the user Details
            invoiceModel.User = invoiceModel.Invoice.User;

            //Get all the tickets
            var invoiceTickets = DbContext.Tickets.Where(i => i.InvoiceId == invoiceModel.Invoice.Id).Distinct().ToDictionary(i => i.Id, i => i);
            invoiceModel.Tickets = invoiceTickets;
           
            //Get all the flights associated with this invoice
            var invoiceFlights = DbContext.Tickets.Join(DbContext.Invoices, m => m.InvoiceId, m => m.Id,
                (t, i) => new { i.Id, t.AllocatedSeat.Flight }).Distinct().ToDictionary(m => m.Id, m => m.Flight);
           
            //Get all information about each flight
            foreach(var flight in invoiceFlights)
            {
                //Arrival Airport 
                flight.Value.ArrivingAt = DbContext.AirPorts.SingleOrDefault(i => i.Id == flight.Value.ArrivingAtId);
                //Departure Airport
                flight.Value.DepartingFrom = DbContext.AirPorts.SingleOrDefault(i => i.Id == flight.Value.DepartingFromId);
                //Plane
                flight.Value.Plane = DbContext.Planes.SingleOrDefault(i => i.Id == flight.Value.PlaneId);
            }
            invoiceModel.Flights = invoiceFlights;
            return View(invoiceModel);
        }
        
        
    }
}