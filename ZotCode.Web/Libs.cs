﻿namespace Links
{
    public static class Lib
    {
        public static class Scripts
        {
            public static readonly string jquery_js = "~/bower_components/jquery/dist/jquery.js";
            public static readonly string bootstrap_js = "~/bower_components/bootstrap/dist/js/bootstrap.js";
            public static readonly string jqueryui_js = "~/bower_components/jquery-ui/jquery-ui.js";
            public static readonly string dataTables_js = "~/bower_components/datatables/media/js/jquery.dataTables.js";
            public static readonly string jquery_timepicker_js = "~/bower_components/jquery-timepicker-jt/jquery.timepicker.js";
            public static readonly string Chartjs = "~/bower_components/Chart.js/Chart.js";
            public static readonly string jqueryui_TimepickerAddon_js = "~/bower_components/jQuery-Timepicker-Addon/dist/jquery-ui-timepicker-addon.js";
            public static readonly string jqueryui_TimepickerAddon_SliderAccess_js = "~/bower_components/jQuery-Timepicker-Addon/dist/jquery-ui-sliderAccess.js";
            public static readonly string wow_js = "Scripts/wow.js";
        }

        public static class Styles
        {
            public static readonly string bootstrap_css = "~/bower_components/bootstrap/dist/css/bootstrap.css";
            public static readonly string jquery_ui_css = "~/bower_components/jquery-ui/themes/base/jquery-ui.css";
            public static readonly string jquery_TimepickerAddon_css = "~/bower_components/jQuery-Timepicker-Addon/dist/jquery-ui-timepicker-addon.css";
            public static readonly string dataTables_css = "~/bower_components/datatables/media/css/jquery.dataTables.css";
            public static readonly string animate_css = "Content/animate.css";
        }
    }
}