﻿/* Functions for the date picker objects. */
$(document).ready(function () {
    var date = $.datepicker.formatDate('dd-M-yy', new Date());
    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate());
    var date2 = $.datepicker.formatDate('dd-M-yy', tomorrow);

    document.getElementById("range_start").value = date;
    document.getElementById("range_end").value = date2;
});

$(function () {
    $("#range_start").datepicker({
        minDate: 0,
        dateFormat: "dd-M-yy"
    });

});

$("#range_end").datepicker({
    dateFormat: "dd-M-yy",
    minDate: 0,
    onSelect: function (date) {
        var date1 = $('#range_start').datepicker('getDate');
        var date = new Date(Date.parse(date1));
        date.setDate(date.getDate());
        var newDate = date.toDateString();
        newDate = new Date(Date.parse(newDate));
        $('#range_end').datepicker("option", "minDate", newDate);
    }
});

/* Function to update the label of the passengers button. */
function updatePassengers() {
    var numAdults = document.getElementById("adults").value;
    var numChildren = document.getElementById("children").value;

    if (numAdults == 1)
        var str = numAdults + " Adult";
    else
        var str = numAdults + " Adults";

    if (numChildren == 1)
        var str = str + "; " + numChildren + " Child";
    else 
        var str = str + "; " + numChildren + " Children";

    var button = document.getElementById("dLabel").firstChild;
    button.data = str;
}

/* One way checkbox */
$("#oneway").click(function () {
    $("#range_end").attr('disabled', this.checked)
});