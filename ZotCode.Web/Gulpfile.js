﻿/// <vs BeforeBuild='bower' AfterBuild='default' />
var gulp = require("gulp");
var bower = require("gulp-bower");
var install = require("gulp-install");

gulp.task("default", function() {
    gulp.src("package.json")
        .pipe(install());
});

gulp.task("bower", function() {
    return bower()
        .pipe(gulp.dest('bower_components/'));
});